--Obtener todas las especialidades medicas en la base de datos

exec SEL_EspecialidadesMedica --Para buscar las especialiidades no se le da ning�n parametro.

CREATE PROC SEL_EspecialidadesMedica

AS
set nocount on

IF EXISTS (
			SELECT * 
			FROM especialidad
		  )
	SELECT * 
	FROM especialidad
ELSE
	SELECT 0 AS Resultado_consulta