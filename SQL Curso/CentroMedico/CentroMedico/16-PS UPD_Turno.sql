--Actualizar turno de paciente

exec upd_turno 10,2,"El paciente recoge los an�lisis"


CREATE PROC UPD_Turno(
				@idturno turno,
				@estado tinyint,
				@observacion observacion)
				
AS
set nocount on

if exists(
			SELECT * 
			FROM Turno
			WHERE idturno = @idturno
			)
		UPDATE Turno 
		SET estado = @estado,
			observacion = @observacion
		WHERE idturno = @idturno
ELSE
	SELECT 0 AS Resultado_consulta

