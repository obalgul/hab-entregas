--Tablas temporales en memoria
--Estas tablas se destruyen despu�s de ejcutarse y devolver un resultado.
--de esta forma al ejecutarse no ocupan espacio en memoria

DECLARE @mitabla TABLE ( --Se crea con el @ delante del nombre de la tabla
		id int IDENTITY(1,1),
		pais varchar(50)
)

INSERT INTO @mitabla values('Mexico')
INSERT INTO @mitabla values('San Salvador')
INSERT INTO @mitabla values('Honduras')
INSERT INTO @mitabla values('Panama')
INSERT INTO @mitabla values('Guatemala')

select * from @mitabla

--Tablas temporales f�sicas
--Estas tablas s� se mantienen en la base de datos, pero hasta qu ese reinicia el motor de la base de datos
--En ese momento dessparecen.

CREATE TABLE #mitabla( --Se crea con el # delante del nombre de la tabla
		id int IDENTITY(1,1),
		nombre varchar(50),
		apellido varchar(50)
)

INSERT INTO #mitabla values('Manolo', 'Castillo')
INSERT INTO #mitabla values('Salvador', 'Madariaga')
INSERT INTO #mitabla values('Hilda', 'Russo')

select * from #mitabla
DROP Table #mitabla  --As� destruimos una tabla temporal en memoria

--Creaci�n de script en un PS

DECLARE @turnos TABLE (
		id INT IDENTITY,
		idturno turno,
		idpaciente paciente
)
DECLARE @idpaciente paciente
set @idpaciente = 5

INSERT INTO @turnos (idturno, idpaciente) --INSERT INTO SELECT es el m�todo m�s utilizado para insertar registros
SELECT TP.idturno, P.idpaciente
FROM Paciente P
INNER JOIN turnoPaciente TP
ON TP.idpaciente = P.idpaciente

DECLARE @i int, @total int

set @i = 1
set @total = (SELECT count(*) FROM @turnos)

WHILE @i <= @total
BEGIN
	IF(
	SELECT idpaciente
	FROM @turnos 
	WHERE id = @i
	) <> 5
		DELETE FROM @turnos
		WHERE id = @i

	set @i = @i + 1
END

SELECT * 
FROM Paciente P
INNER JOIN @turnos T
ON T.idpaciente = P.idpaciente


