--Operadores aritmetricos
--Suma +
DECLARE @num1 decimal(9,2) = 20
DECLARE @num2 decimal(9,2) = 30
DECLARE @result decimal(9,2)

set @result = @num1 + @num2
PRINT @result

-- suma usandolo como concatenar
DECLARE @num10 varchar(25) = 'Hola mi nombre es'
DECLARE @num11 varchar(25) = 'Perico de los Palotes'
DECLARE @result5 varchar(50)

set @result5 = @num10 + ' ' +  @num11
PRINT @result5

--Resta -
DECLARE @num3 decimal(9,2) = 20
DECLARE @num4 decimal(9,2) = 30
DECLARE @result1 decimal(9,2)

set @result1 = @num3 - @num4
PRINT @result1

--Multiplicaci�n *
DECLARE @num5 decimal(9,2) = 20
DECLARE @num6 decimal(9,2) = 30
DECLARE @result2 decimal(9,2)

set @result2 = @num5 * @num6
PRINT @result2

--Divisi�n /
DECLARE @num7 decimal(9,2) = 20
DECLARE @num8 decimal(9,2) = 30
DECLARE @result3 decimal(9,2)

set @result3 = @num7 / @num8
PRINT @result3

--Modulo � resto %
DECLARE @num9 decimal(9,2) = 20
DECLARE @num0 decimal(9,2) = 30
DECLARE @result4 decimal(9,2)

set @result4 = @num9 % @num0

PRINT @result4

--Operadores l�gicos con n�meros
--Mayor que >
DECLARE @num13 decimal(9,2) = 40
DECLARE @num14 decimal(9,2) = 30

IF @num13 > @num14
	PRINT 'S�'

--Menor que >
DECLARE @num15 decimal(9,2) = 40
DECLARE @num16 decimal(9,2) = 30

IF @num15 > @num16
	PRINT 'No'

--Mayor o igual que >=
DECLARE @num17 decimal(9,2) = 40
DECLARE @num18 decimal(9,2) = 30

IF @num17 >= @num18
	PRINT 'S�'

--Menor o igual que >=
DECLARE @num19 decimal(9,2) = 40
DECLARE @num20 decimal(9,2) = 30

IF @num19 >= @num20
	PRINT 'No'

--Igual =
DECLARE @num21 decimal(9,2) = 40
DECLARE @num22 decimal(9,2) = 40

IF @num21 = @num22
	PRINT 'S�'

--Distinto <>
DECLARE @num23 decimal(9,2) = 40
DECLARE @num24 decimal(9,2) = 30

IF @num23 <> @num24
	PRINT 'S�'
--Operadores l�gicos con textos (Comprueva alfabeticamente)
--Mayor que >
DECLARE @texto1 varchar(25) = 'Hola mi nombre es'
DECLARE @texto2 varchar(25) = 'Perico de los Palotes'

IF @texto2 > @texto1
	PRINT 'S�, es mayor'

--Menor que <
DECLARE @texto3 varchar(25) = 'Hola mi nombre es'
DECLARE @texto4 varchar(25) = 'Perico de los Palotes'

IF @texto3 < @texto4
	PRINT 'S�, es menor'

--Mayor o igual que >=
DECLARE @texto5 varchar(25) = 'Hola mi nombre es'
DECLARE @texto6 varchar(25) = 'Perico de los Palotes'

IF @texto6 >= @texto5
	PRINT 'S�'

--Menor o igual que <=
DECLARE @texto7 varchar(25) = 'Hola mi nombre es'
DECLARE @texto8 varchar(25) = 'Perico de los Palotes'

IF @texto5 <= @texto6
	PRINT 'S�'

--Igual que = (Comprueba que ambos textos sean iguales)
DECLARE @texto9 varchar(25) = 'Hola mi nombre es Jon'
DECLARE @texto10 varchar(25) = 'Perico de los Palotes'

IF @texto9 = @texto10
	PRINT 'S�'

--Distinto <>
DECLARE @texto11 varchar(25) = 'Hola mi nombre es Jon'
DECLARE @texto12 varchar(25) = 'Perico de los Palotes'

IF @texto11 <> @texto12
	PRINT 'S�'