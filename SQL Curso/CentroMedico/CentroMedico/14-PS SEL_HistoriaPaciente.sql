USE [CentroMedico]
GO
/****** Object:  StoredProcedure [dbo].[SEL_HistoriaPaciente]    Script Date: 06/04/2021 12:39:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Obtener una historia cl�nica de un paciente

exec SEL_HistoriaPaciente 3

ALTER PROC [dbo].[SEL_HistoriaPaciente](
					@idpaciente paciente
					)

AS
set nocount on

/*
select * from historia
select * from historiapaciente
*/

IF EXISTS(
			SELECT * 
			FROM Paciente P
			INNER JOIN HistoriaPaciente HP
			ON HP.idpaciente = P.idpaciente
			INNER JOIN Historia H
			ON H.idHistoria = HP.idHistoria
			INNER JOIN MedicoEspecialidad ME
			ON ME.idMedico = HP.idMedico
			INNER JOIN Medico M
			ON M.idmedico = ME.idMedico
			WHERE P.idpaciente = @idpaciente			
			)
		SELECT * 
		FROM Paciente P
		INNER JOIN HistoriaPaciente HP
		ON HP.idpaciente = P.idpaciente
		INNER JOIN Historia H
		ON H.idHistoria = HP.idHistoria
		INNER JOIN MedicoEspecialidad ME
		ON ME.idMedico = HP.idMedico
		INNER JOIN Medico M
		ON M.idmedico = ME.idMedico
		WHERE P.idpaciente = @idpaciente
ELSE
	--PRINT "Para est� consulta no existen registros" --Para devolver en pantalla
	SELECT 0 AS Resultado_consulta --Para devolver en tabla

