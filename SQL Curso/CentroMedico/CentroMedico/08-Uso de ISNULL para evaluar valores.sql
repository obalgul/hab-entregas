--Procedimiento almacenado

USE [CentroMedico]
GO
/****** Object:  StoredProcedure [dbo].[S_paciente]    Script Date: 29/03/2021 15:26:22 ******/
SET ANSI_NULLS OFF --En las busquedas no mostrar� los campos que se encuentren vac�os. En OFF si mostrar� los campos que est�n vacios
GO
SET QUOTED_IDENTIFIER OFF --Permite crear objetos con nombres de palabras reservadas. Si se pone en OFF, no nos o permitir�
GO


--Procesos almacenados.
--Su objetivo es crear un conjunto de ejecuciones en una sola ejecuci�n.
--Por ejemplo la inserci�n de varios registros.

ALTER PROC [dbo].[S_paciente]( --ALTER modifica nuestro proceso. Para realizar uno nuevo, sustituimos ALTER por CREATE
		@idpaciente int
)
AS

SELECT apellido, nombre, idpais, observacion
FROM paciente
WHERE idpaciente = @idpaciente

--Los procedimientos nos sirven para realizar entre otras b�squedas complejas que se repiten, ejecutando un solo comando.

--Declaraci�n de variable con DECLARE
--Las variables nos sirven para guardar objetos en el script
DECLARE @ordenamiento CHAR(1) --Esta variable ahora es null, con lo que no nos devolver� nada al ejecutarla.
DECLARE @valorordenamiento CHAR(1) --nombre de variable

SET @valorordenamiento = @ordenamiento -- Le damos valor a  @valorordenamiento 

PRINT @valorordenamiento  --Al ejecutar no nos muestra nada. Ya que el valor de @ordenamiento es null.

--Soluci�n para darle valos a la variable
DECLARE @ordenamiento CHAR(1) --Esta variable ahora es null, con lo que no nos devolver� nada al ejecutarla.
DECLARE @valorordenamiento CHAR(1) --nombre de variable

SET @valorordenamiento = ISNULL (@ordenamiento, 'A') -- Le damos valor a  @valorordenamiento y especificamos que @ordenamiento ISNULL y le damos un valor, para que cuando se ejecute nos lo de vuelva.

PRINT @valorordenamiento 