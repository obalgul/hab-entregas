USE [CentroMedico]
GO
/****** Object:  StoredProcedure [dbo].[SELECT_TurnosPaciente]    Script Date: 06/04/2021 12:29:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Stored Procedure para obtener los turnos de un paciente

exec SELECT_TurnosPaciente 2

ALTER PROC [dbo].[SELECT_TurnosPaciente](
		@idpaciente paciente
		)

AS
set nocount on

IF EXISTS( --**Si existen registros para est� consulta 
			SELECT *
			FROM Paciente P --b�scamos en la tabla paciente un idpaciente
			INNER JOIN TurnoPaciente TP --que coincida con un turno en la tabla Turnopaciente
			ON TP.idpaciente = P.idPaciente --B�scamos por idpaciente
			INNER JOIN Turno T --Adem�s a�adimos de la tabla turno el idturno del paciente
			ON T.idTurno = TP.idTurno --Buscamos el idturno
			INNER JOIN MedicoEspecialidad M --b�camos al medico en la tabla medicoespecialista  
			ON M.idMedico= TP.idMedico --B�scando por el idmedico
			WHERE P.idpaciente = @idpaciente --Por �ltimo buscamos en todas las tablas implicadas el idpaciente
			)
--BEGIN  --No hace falta el BEGIN END cuando solo se ejecuta una sentencia. UN SELECT UPDATE DELETE, si son m�s de uno si es obligado ponerlo, ya que sino nos dar� error.
		SELECT *  --**Realizas la consulta
		FROM Paciente P
		INNER JOIN TurnoPaciente TP 
		ON TP.idpaciente = P.idPaciente
		INNER JOIN Turno T
		ON T.idTurno = TP.idTurno 
		INNER JOIN MedicoEspecialidad M  
		ON M.idMedico= TP.idMedico 
		WHERE P.idpaciente = @idpaciente 
--END
ELSE --**SIno me devuelves
	SELECT 0 AS Resultado_consulta --valor 0 en la tabla y llamas la campo Resultado_consulta