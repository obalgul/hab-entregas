--BackUP

BACKUP DATABASE centroMedico
TO DISK = 'D:\Curso SQL\BKUP Base datos\CentroMedico20210406.bak'
WITH NO_COMPRESSION, NAME='CentroMedico1'


--BACKUP con fecha automatizada

DECLARE @fecha char(12)
DECLARE @path varchar(255)
DECLARE @name varchar(50)

set @fecha = convert(char(8), getdate(),112) +  replace(convert(char(8), getdate(),108),':','') 
set @path = 'D:\Curso SQL\BKUP Base datos\CentroMedico'+@fecha+'.bak'
set @name = 'CentroMedico'+@fecha



BACKUP DATABASE centroMedico
TO DISK = @path
WITH NO_COMPRESSION, NAME=@name



