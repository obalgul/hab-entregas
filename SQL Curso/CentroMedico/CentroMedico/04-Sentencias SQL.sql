
--CREATE sirve para crear objetos en SQL.
--Creamos un dato
--CREATE TYPE Pais FROM CHAR(3)

--Crear funciu�n
--ejecutamos la funci�n, siempre con la sentencia SELECT
SELECT dbo.nombrefun (256)
--Creamos la funciu�n
CREATE FUNCTION nombrefun (@var int)
RETURNS int

AS

BEGIN
	set @var = @var * 5
	return @var
END

--ALTER sirve para ejecutar funciones o procesos ya creados sustituyendo CREATE por ALTER
--ALTER tambi�n sirve para modificar tablas.
--Agregar campos a las tablas (ALTER TABLE nombre de tabla ADD nombre de campo tipo de dato)
ALTER TABLE Paciente 
ADD estado smallint

--Modificar un tipo de dato (ALTER TABLE nombre de tabla ALTER COLUMN nombre de campo tipo de dato)
ALTER TABLE Paciente 
ALTER COLUMN estado bit

--Eliminar un campo de la tabla (ALTER TABLE nombre de tabla DROP COLUMN nombre de campo)
ALTER TABLE Paciente 
DROP COLUMN estado

--DROP nos permite eliminar tablas por completo o una base de datos.(DROP TABLE � Nombre del a base de datos)
DROP TABLE ejemplo

--TRUNCATE elimina los datos de una tabla. Y resetea los campos IDENTITY
--Creamos una tabla de ejemlo e insertamos datos (presionando varias veces F5) y la mostramos
CREATE TABLE ejemplo(campo0 int IDENTITY, campo1 int, campo2 int)
insert into ejemplo values (2,3)
--ejecutamos para ver los registros
SELECT *
FROM ejemplo
--reseteamos la tabla
TRUNCATE TABLE ejemplo
--ejecutamos para ver los registros despu�s de resetearla
SELECT *
FROM ejemplo
--insertamos datos en la tabla
insert into ejemplo values (2,3)
--ejecutamos para ver los nuevos registros en la tabla
SELECT *
FROM ejemplo