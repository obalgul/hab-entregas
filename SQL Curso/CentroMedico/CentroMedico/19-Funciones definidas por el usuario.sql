--Funciones escalares definidas por el usuariuo

select dbo.concatenar('Fernandez', 'Camilo')




CREATE FUNCTION concatenar(
		@apellido varchar(50),
		@nombre varchar(50)
		)
		RETURNS varchar(100)

AS
BEGIN
	DECLARE @resultado varchar(100)
	set @resultado = @apellido + ', '+ @nombre
	RETURN @resultado
END

--Crear funcui�n para devolver el valor de una tabla

select dbo.obtenerPais(4)

CREATE FUNCTION obtenerPais(
		@idpaciente paciente
		)
RETURNS varchar(50)

AS 
BEGIN
	DECLARE @pais varchar(50)
	SET @pais = (
				SELECT PA.pais
				FROM Paciente P
				INNER JOIN Pais PA
				ON PA.idPais = P.idPais
				WHERE idPaciente = @idpaciente
				)
			RETURN @pais
END

--Funciiones tipo tabla retorna un conjunto deregistros en una tabla.

select * from dbo.listaPaises()

CREATE FUNCTION listaPaises()
RETURNS @paises TABLE(
					idpais char(3), pais varchar(50)
					)
AS
BEGIN
	INSERT INTO @paises values('ESP','Espa�a')
	INSERT INTO @paises values('CHI','Chile')
	INSERT INTO @paises values('HON','Honduras')
	INSERT INTO @paises values('VEN','Venezuela')

	RETURN
END