--Estructuras de control

--IF serve para evaluar.
--Para mostrar el segundo resultado hacer ctrl + t y luego seleccionar y pulsar F5
/*
DECLARE @idpaciente int
DECLARE @idturno int

set @idpaciente = 9

IF @idpaciente = 9
	set @idturno =20
		SELECT *
		FROM Paciente
		WHERE idpaciente=@idpaciente
		PRINT @idturno --Ejecutar con Ctrl + T para visualizar el resultado
*/
--IF serve para evaluar.
/*
DECLARE @idpaciente int
DECLARE @idturno int

set @idpaciente = 9

IF @idpaciente = 11
	set @idturno =20  --Esta l�nea no la ejecuata porque no se cumple, ya que @idpaciente no es 9
		SELECT *
		FROM Paciente
		WHERE idpaciente=@idpaciente --Este bloque si lo ejecuata ya que existe un @idpaciente 11 y lo muestra
		PRINT @idturno --No lmuestra el resultado porque no se cumple la l�nea set @idturno
*/
--IF con BEGIN END nos permite evaluar un bloque
--Nos devuelve un resultado al cumplirse las condiciones
/*
DECLARE @idpaciente int
DECLARE @idturno int

set @idpaciente = 9

IF @idpaciente =9
	BEGIN --Comienzo del bloque a evaluar
	set @idturno =20 
		SELECT *
		FROM Paciente
		WHERE idpaciente=@idpaciente 
		PRINT @idturno 
	END --Fin del bloque a evaluar
*/
--No nos devuelve un resultado al no cumplirse las condiciones
--ELSE para que nos devuelva un resultado ejecutamos un nuevo bloque BEGIN END a continuaci�n del ELSE 
/*
DECLARE @idpaciente int
DECLARE @idturno int

set @idpaciente = 9

IF @idpaciente =5
	BEGIN --Comienzo del bloque a evaluar
	set @idturno =20 
		SELECT *
		FROM Paciente
		WHERE idpaciente=@idpaciente 
		PRINT @idturno 
	END --Fin del bloque a evaluar
ELSE --Ejecutamos para que si la condici�n anterior no se cumple tener un resultado
	BEGIN --Siempre dentro de un bloque BEGIN END
		PRINT 'No se cumplen la condici�n'
	END
*/
--IF con la clausula EXISTS para devolver el resultado si este existe.
--Anidamos dentro de otro IF
/*
DECLARE @idpaciente int
DECLARE @idturno int

set @idpaciente = 9

IF @idpaciente =9
	BEGIN --Comienzo del bloque a evaluar que s� se cumple
	set @idturno =20 
		SELECT *
		FROM Paciente
		WHERE idpaciente=@idpaciente 
		PRINT @idturno 

		IF EXISTS( -- Condici�n que se cumple
			SELECT *
			FROM Paciente
			WHERE idpaciente = 11
			)
			PRINT 'Existe'
	END
ELSE
	BEGIN
		PRINT 'No se cumplen la condici�n'
	END
*/
--Estructura WHILE, se ejecuta hasta que una condici�n se cumpla.
--Es un bucle que sienmpre se ejecuta con BEGIN END
/*
DECLARE @contador int=0

WHILE @contador <= 10

	BEGIN
		print @contador
		set @contador = @contador + 1
	END
*/
--CASE nos permite realizar otras acciones. CASE === en caso que
/*
DECLARE @valor int
DECLARE @resultado char(10)=''

set @valor = 30

set @resultado = (CASE WHEN @valor = 10 THEN 'ROJO'
						WHEN @valor = 20 THEN 'AMARILLO'
						WHEN @valor = 30 THEN 'VERDE'
				END)
PRINT @resultado
	
--Ejemplo CASE. Podemos darle valores a unos campos seg�n su datos de registro.
SELECT *,
	(
	CASE WHEN estado=1 THEN 'VERDE'
		 WHEN estado=2 THEN 'ROJO'
		 WHEN estado=3 THEN 'AMARILLO'
			ELSE 'GRIS'
	END
	) as colorTurno --asignamos un nombre al campo
FROM turno
*/
--RETURN sirve para salir de forma forzosa de procesos almacenados y todo lo que este por debajo no se ejecuta.
/*
DECLARE @contador int=0

WHILE @contador <= 10

	BEGIN
		print @contador
		set @contador = @contador + 1
			IF @contador = 3
			RETURN
			PRINT 'He parado'
	END
*/
--BREAK permite salir del bucle y sigue ejecutando las siguientes instrucciones. Con RETURN sale y no ejecuta nada m�s.

DECLARE @contador int=0

WHILE @contador <= 10

	BEGIN
		print @contador
		set @contador = @contador + 1
			IF @contador = 3
			BREAK
			
	END
PRINT 'He parado y estoy despu�s del BREAK'

--TRY CACH nos sirve para ver errores en ejecuci�n.
BEGIN TRY
	set @contador = 'texto'
END TRY

BEGIN CATCH
	PRINT 'No se puede dar un valor texto a la variable @contador'
END CATCH
