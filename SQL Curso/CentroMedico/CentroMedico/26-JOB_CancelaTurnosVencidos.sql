--JOB crear un JOB para eliminar los turnos vencidos

use CentroMedico
GO

UPDATE turno
set estado = 2
WHERE convert(char(8), fechaTurno,112) < convert(char(8), getdate(),112)
and estado = 1

select * from Turno