--Vistas implementación

SELECT * FROM PacientetesTurnosEnTratamiento --se llama a la tabla como otra tabla normal. Esta vista se mantiene 
										 --activa para futuras consultas.

CREATE VIEW PacientetesTurnosEnTratamiento AS

SELECT P.idPaciente, P.nombre, P.apellido, T.idturno, T.estado
FROM Paciente P
	INNER JOIN TurnoPaciente TP
	ON TP.idpaciente = P.idpaciente
		INNER JOIN Turno T
		ON T.idturno = TP.idTurno
WHERE isnull(T.estado, 2) = 2

SELECT * FROM vistaPrueba