--LEFT y RIGHY nos permite obterner caracteres a la izquierda o la derecha del tipo CHAR o VARCHAR
--Nos muestra dos caracteres por la izquiera
DECLARE @var1 varchar(20)
SET @var1 = 'Ramiro'
PRINT LEFT (@var1, 2)
--Nos muestra el primer caracter por la izquierda
DECLARE @var2 varchar(20)
DECLARE @var3 varchar(20)
SET @var2 = 'Ramiro'
SET @var3 = 'Cifuentes'
PRINT LEFT (@var2, 1) + LEFT (@var3, 1)

--Nos muestra dos caracteres por la derecha
DECLARE @var4 varchar(20)
SET @var4 = 'Ramiro'
PRINT RIGHT (@var4, 2)
--Nos muestra el primer caracter por la derecha
DECLARE @var5 varchar(20)
DECLARE @var6 varchar(20)
SET @var5 = 'Ramiro'
SET @var6 = 'Cifuentes'
PRINT RIGHT (@var5, 1) + RIGHT (@var6, 1)

--LEN nos permite contar los caracteres de un registro de tipo CHAR o VARCHAR
DECLARE @var7 varchar(20)
SET @var7 = 'Cifuentes'
--nos cuenta los caracteres de @var7
PRINT LEN(@var7)
--Con LEN podemos combinarlo con RIGHT o LEFT
DECLARE @var8 varchar(20)
SET @var8 = 'Ramiro'
--el ejemplo nos devuelve los 6 primeros careateres de la variable
PRINT LEFT (@var8, LEN(@var8)-3)
PRINT RIGHT (@var8, LEN(@var8)-3)

--Lower o upper
--Lower nos pasa los caracteres en mayuscula a minuscula
DECLARE @var9 varchar(20)
SET @var9 = 'CIFuenTES'
PRINT LOWER(@var9)
--Lower nos pasa los caracteres en mayuscula a minuscula
DECLARE @var10 varchar(20)
SET @var10 = 'cifUENTes'
PRINT UPPER(@var10)

--ejemplo poner el primer caracter en mayuscula y los dem�s en minusculas.
DECLARE @var11 varchar(20)
DECLARE @var12 varchar(20)
SET @var12 = 'rAmIRo'
SET @var11 = 'cifUENTes'
PRINT UPPER(LEFT(@var12,1)) + LOWER(RIGHT(@var12, LEN(@var12)-1))
+ ' ' +
UPPER(LEFT(@var11,1)) + LOWER(RIGHT(@var11, LEN(@var11)-1))

--REPLACE permite reemplazar caracteres por otros
--Se puede aplicar a una tabla y un campo concreto de la tabla
--(SELECT nombre de tabla REPLACE(nombre de campo, 'caracter a cambiar','caracter a poner'))
DECLARE @var13 varchar(20)
DECLARE @var14 varchar(20)
SET @var13 = 'Ramir0'
SET @var14 = 'Cifuemntes'
SELECT REPLACE(@var13,'0','o')
SELECT REPLACE(@var14,'m','')

--REPLICATE`permite replicar una cadena o un caracter.
PRINT REPLICATE ('0',5)

--LTRIM y RTRIM eliminan los espacios a la izquierda(LTRIM) y derecha(RTRIM)
--LTRIM
DECLARE @var15 varchar(20)
DECLARE @var16 varchar(20)
SET @var15 = '        Ramiro   '
SET @var16 = '   Cifuentes        '
SELECT LTRIM(RTRIM(@var15))
+ ' ' + + ' ' +
LTRIM(RTRIM(@var16))

--CONCAT concatena varios valores dos o m�s, sin limites.
DECLARE @var17 varchar(20)
DECLARE @var18 varchar(20)
DECLARE @var19 varchar(20)
SET @var17 = 'Ramiro'
SET @var18 = 'Cifuentes'
SET @var19 = 'Garrido'
SELECT CONCAT ((@var17),' ',(@var18),' ',(@var19))

--GETDATE nos da la hora local de la base de datos
SELECT GETDATE()

--GETUTCDATE nos devuelve la hora del meridiano de greenwich
SELECT GETUTCDATE()

--DATEADD nos permite modificar fechas
--(SELECT DATEADD(d�a 0 mes o a�o, cantidad a sumar o restar, fecha a la qu esumar o restar)
SELECT DATEADD(DAY,-2,GETDATE())
SELECT DATEADD(dd,-2,GETDATE())
SELECT DATEADD(MONTH,-2,GETDATE())
SELECT DATEADD(mm,-2,GETDATE())
SELECT DATEADD(YEAR,-2,GETDATE())
SELECT DATEADD(yy,-2,GETDATE())

--DATEDIFF nos devuelve el tiempo entre dos fechas
--(SELECT DATEDIFF(diferencia por la que medir, primera fecha, segunda fecha)
--Como DATEADD se puede utilizar abreviaturas.
SELECT DATEDIFF(yy, GETDATE(), '20200401')
SELECT DATEDIFF(mm, GETDATE(), '20200401')
SELECT DATEDIFF(dd, GETDATE(), '20200401')

--DATEPART nos permite obtener un intervalo
--dw nos devuelve el d�a de la semana empezando en Lunes.
--Como en la santeriores se puede utilizar abreviaturas.
SELECT DATEPART(yy, GETDATE())

--ISDATE nos permite si el formato de fecha es correcto
--Si es correcto devuelve un 1
PRINT ISDATE(getdate())
--Si es incorrecto devuelve un 0
PRINT ISDATE(2020405)

--ejemplo con IF
IF ISDATE(20211330) = 1
		PRINT 'Esto es una fecha correcta'
ELSE
		PRINT 'Es una fecha incorrecta'

--CAST permite mostrar(no cambia) el valor de un registro, n�merico a otro tipo n�merico. O uno texto a otro de texto
--pero nunca de uno n�merico a uno de texto
DECLARE @num MONEY
set @num = 500.40
print @num
--mostramos el tipo de dato a INT integre un n�mero entero
SELECT CAST(@num as INT)
--mostramos un entero a un tipo money de una tabla
--Nos devuelve los idpaciente en formato MONEY.
SELECT CAST(idpaciente AS MONEY)
FROM Paciente

--CONVERT nos permite convertir el tipo de dato. 
--(SELECT o PRINT CONVERT(tipo de variable que queremos cambiar, nombre de variable, o campo de tabla)
DECLARE @num1 MONEY
set @num1 = 500.40
PRINT CONVERT(int, @num1)

--CONVERT con fechas nos permite convertir a otros formatos las fechas.
DECLARE @fecha DATETIME
SET @fecha = GETDATE()
PRINT @fecha
--el c�digo 112 es el formato de fecha en SQL
--el c�digo 104 devuelve dd/mm/yyyy
PRINT CONVERT(char(20), @fecha, 112)
PRINT CONVERT(char(20), @fecha, 104)