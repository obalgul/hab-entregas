--Subconsultas

USE [CentroMedico]
GO
/****** Object:  StoredProcedure [dbo].[S_paciente]    Script Date: 29/03/2021 15:26:22 ******/
SET ANSI_NULLS OFF --En las busquedas no mostrar� los campos que se encuentren vac�os. En OFF si mostrar� los campos que est�n vacios
GO
SET QUOTED_IDENTIFIER OFF --Permite crear objetos con nombres de palabras reservadas. Si se pone en OFF, no nos o permitir�
GO


--Procesos almacenados.
--Su objetivo es crear un conjunto de ejecuciones en una sola ejecuci�n.
--Por ejemplo la inserci�n de varios registros.

ALTER PROC [dbo].[S1_paciente]( --ALTER modifica nuestro proceso. Para realizar uno nuevo, sustituimos ALTER por CREATE
		@idpaciente int
)
AS

SELECT apellido, nombre, idPais, observacion, 
--A�adimos la subconsulta y a�adimos alias a las tablas, para que SQL las pueda distinguir.
		(SELECT ps.pais --a La columna pais le a�adimos delante ps.
		 FROM Pais ps -- a la tabla le a�adimos despu�s el alias
		 WHERE ps.idPais= pa.idpais) descPais --y en aqu� identificamos que idPais pertenece a qui�n segun su alias. Despu�s de cerrar los () le ponemos un nombre a la nueva columna.
FROM paciente pa --identificamos con el alias a que table se refiere
WHERE idPaciente = @idpaciente