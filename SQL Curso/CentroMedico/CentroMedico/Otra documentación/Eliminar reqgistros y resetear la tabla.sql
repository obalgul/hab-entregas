
--Insertamos el valor donde queremos que empiece.
insert into TablaPrueba values(1)

--seleccionamos la tabla a resetear
select * from TablaPrueba

--borramos los datos de la tabla
delete from TablaPrueba

--resetamos la tabla, aun teniendo PRIMARY KEY
dbcc CHECKIDENT ('TablaPrueba',RESEED,0)