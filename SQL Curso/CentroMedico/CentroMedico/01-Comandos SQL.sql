

-- Seleccionar registros (SELECT campo o campos a buscar FROM nombre tabla)
SELECT * FROM paciente

--Para seleccionar varias columnas (SELECT campo o campos a buscar FROM nombre tabla)
SELECT idpaciente, nombre, apellido 
FROM paciente

--Insertar datos en una tabla (INSERT INTO nombre de tabla VALUES valores entrantes en orden a los campos)
INSERT INTO paciente 
values('Eva','Marin','1982-04-16','Mi casa n�23','ESP','','','')

--Como buenas practicas se deben identificar los campos que a�adimos a la tabla
INSERT INTO paciente (nombre,apellido,fNacimiento,domicilio,idPais,telefono,email,observacion) 
values('Beatriz','Nito','1952-04-16','Miar n�3','COL','','','')

--Actualiza todos los registros del campo idpais a 'PER' (UPDATE nombre de tabla SET campo a actualizar = dato de actualizaci�n)
UPDATE paciente SET idpais='PER'

--UPDATE actualizaci�n de datos de un registro, en este caso el id4 (Donde WHERE indica el lugar donde se debe actualizar el registro)
UPDATE paciente 
SET observacion = 'Prueba de registro en el idpaciente 6' 
WHERE idpaciente = 6

--Buscar e idpaciente 6 de la tabla paciente
SELECT *
FROM paciente 
WHERE idpaciente = 6

--Buscar registros concretos con la especificaci�n WHERE (SELECT nombre del campo FROM nombre de la tabla WHERE dato del registro b�scado)
SELECT * FROM paciente 
WHERE apellido = 'Nito'

--Buscar varios datos registro (and nos sirve para encadenar b�queda de datos de registro) 
SELECT * FROM paciente 
WHERE nombre = 'Beatriz' and apellido = 'Nito'

--DELETE (DELETE FROM nombre de tabla WHERE datos de registro. "BORRA TODA LA INFORMACI�N DE ESE REGISTRO")  
--IMPORTANTE!!!!     Si ejecutamos un DELETE sin WHERE borraremos TODOS los regostros de esa tabla.
DELETE FROM paciente
WHERE nombre = 'Beatriz' and apellido = 'Nito'

--Clausula TOP (SELECT TOP n�mero de registros que quiero ver  campos que quiero ver FROM nombre de la tabla)
--TOP trae los primeros registros que indicamos 
SELECT TOP 2 * 
FROM Paciente

--TOP indicando que queremos buscar
SELECT TOP 2 * 
FROM Paciente
WHERE apellido = 'Marin'

--TOP buscar en la tabla turno un registro ordenado por el campo (SELECT TOP n�mero de registros   campos que quiero ver FROM nombre de tabla ORDER BY nombre de campo DESC)
--DESC significa descendente. De mayor a menor
SELECT TOP 1 *
FROM Turno
ORDER BY fechaturno
DESC

--ORDER BY b�squeda por fecha de nacimiento (ORDER BY busca por defecto de forma ascendente, de mayor a menor)
SELECT * 
FROM Paciente
ORDER BY fNacimiento

--ORDER BY b�squeda por fecha de nacimiento de forma descendente, de menor a mayor
SELECT * 
FROM Paciente
ORDER BY fNacimiento
DESC

--TOP con ORDER BY b�squeda de un registro por fecha de nacimiento. El primer registro ascendente, de menor a mayor
SELECT TOP 1 * 
FROM Paciente
ORDER BY fNacimiento

--TOP con ORDER BY b�squeda de un registro por fecha de nacimiento. El primer registro descendente, de menor a mayor
SELECT TOP 1 * 
FROM Paciente
ORDER BY fNacimiento
DESC

--DISTINCT agrupa registros por los que busquemos, solo muestra un registro que este repetido.
--Cuando se busca en un campo se pone entre ()
SELECT DISTINCT (apellido)
FROM paciente

--Cuando se busca en por varios campos se separan por ','  AL buscar por idpaciente aparecentodos los campos ya que no se repiten. Solo agrupa los campos que esten repetidos
SELECT DISTINCT idpaciente, apellido
FROM paciente

--GROUP BY Nos permite buscar. (SELECT campo a buscar FROM nombre tabla GROUP BY campo que buscamos)
SELECT apellido 
FROM paciente
GROUP BY apellido

--MAX nos permite buscar el registro m�s alto, en este caso el idpaciente
SELECT MAX(idpaciente)  
FROM paciente

--MIN nos permite buscar el registro m�s bajo, en este caso el idpaciente
SELECT MIN(idpaciente)  
FROM paciente

--ORDER BY con MAX nos permite buscar el registro m�s alto por la agrupaci�n que realicemos. Nos mostrara el m�s alto de todos los registros
--(SELECT campo por el que b�scamos, MIN por el campo referencia FROM nombre de la tabla GROUP BY campo a agrupar
SELECT apellido, MAX(idpaciente)
FROM paciente
GROUP BY apellido

--ORDER BY con MAX nos permite buscar el registro m�s alto por la agrupaci�n que realicemos. Nos mostrara el m�s bajo de todos los registros
SELECT apellido, MIN(idpaciente)
FROM paciente
GROUP BY apellido

--SUM sumariza la cantidad de registros que b�squemos (suma la cantidad que da el total de registros)
--En este caso suma todos los campos idpaciente y nos devuelve un registro con la suma
SELECT SUM(idpaciente)
FROM paciente


--GROUP BY con SUM. SUM sumariza campos que se puedan contar, que tengan n�meros. Y nos devuelve la cantidad que hay
--En este caso nos suma todos los campos seg�n su  apellido. (nos suma todos los idpaciente del apellido marin, los otros no cambian porque solo hay un caso. S� se repiriesen se les sumar�a el id. 
--Es ut�l para realizar sumas de cantidades.
SELECT SUM(idpaciente),apellido
FROM paciente
GROUP BY apellido

--AVG calcula de medias
--Calcula el valor medio de un campo n�merico. Redondea al entero si no hay d�cimal, es m�s exacto en campos con decimales y campos de moneda
SELECT AVG(idpaciente)
FROM paciente

--COUNT nos suma registros(filas)
--Aqu� nos devuelve la cantidad de registros que tenemos en la tabla paciente
SELECT COUNT(*)
FROM paciente

--Cantidad de registros con el nombre Eva (SELECT COUNT(nombre del campo a contar FROM nombre tabla WHERE campo o rdato de registro a buscar)
SELECT COUNT(idpaciente)
FROM paciente
WHERE nombre = 'Eva'

--Having nos permite buscar un valor que esa el dato a buscar. Siempre puede ir acompa�ada de COUNT o GROUP BY
--(SELECT campo a agrupar FROM nombre tabla GROUP BY campo a agrupar HAVING COUNT campo a agrupar con un operador = > < que un n�mero)
--Devuelve MArin ya que hay tres registros con el apellido MArin. Siempre debe hacerse con un opperador l�gico
SELECT apellido
FROM paciente
GROUP BY apellido
HAVING COUNT (apellido) =3



--Operadores l�gicos (Se pueden combinar entre ellos)
--AND nos permite a�adir y que se cumpla.
SELECT *
FROM paciente
WHERE apellido='marin' and nombre='eva' and idpaciente=9

--OR nos permite a�adir una condici�n que se cumple junto a otra y nos devuelve ambas.
SELECT *
FROM paciente
WHERE apellido='marin' or nombre='patricia'

--Los operadores AND y OR pueden darseles prioridad encerrandolos entre (). Ya que si no se hace puede haber diferencias en los resultados
--Sin () observar el resultado. Devuelve todos los resultados.
SELECT *
FROM paciente
WHERE apellido='marin' AND nombre='patricia' OR idpais='PER'

--Con (), le damos prioridad, observar el resultado. Ahora solo devuelve menos registros.
SELECT *
FROM paciente
WHERE apellido='marin' AND (nombre='patricia' OR idpais='PER')

--IN nos permite buscar por datos de registros que est�n dentro del campo
SELECT *
FROM paciente
WHERE nombre
IN ('Eva', 'Patricia')

--LIKE nos permite buscar por cadena de caracteres que haya en un campo de registro
--Hay que poner un simbolo % antes, despu�s o en ambos seg�n la cadena que b�squemos.
--En este caso buscamos Gorge, con los % antes y despu�s consigue encontrarlo, sino ponemos los % no encontrar� registros
SELECT *
FROM paciente
WHERE nombre LIKE '%org%'

--NOT se utiliza con IN y LIKE y se coloca antes de estos. Nos devuelve la negaci�n de la busqueda que realizamos
-- en el caso de IN, todos los que no sean Eva ni Patricia
SELECT *
FROM paciente
WHERE nombre
NOT IN ('Eva', 'Patricia')

-- en el caso de LIKE, todos los que no contengan '%org%'
SELECT *
FROM paciente
WHERE nombre NOT LIKE '%org%'

--BEETWEEN se utiliza para buscar entre dos valores n�mericos, fechas, o cadenas de texto.
--IMPORTANTE al realizar b�squedas entre fechas tiene encuenta las horas. Por defecto hace la b�squeda en 00:00:00
--fechas
SELECT * 
FROM paciente
WHERE fNacimiento 
BETWEEN '19800215' AND '19890216'

--n�meros
SELECT * 
FROM paciente
WHERE idpaciente 
BETWEEN '7' AND '15'

--cadena de texto (Busca de forma ascendente seg�n el orden del abecedar�o, de la 'a' la 'z')
SELECT * 
FROM paciente
WHERE apellido 
BETWEEN 'a' AND 'g'
