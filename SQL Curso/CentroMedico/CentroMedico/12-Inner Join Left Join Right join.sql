
select * from medico
select * from medicoespecialidad
select * from especialidad

--INNER JOIN busca coincidencias entre dos tablas
--Buscamos coincidencias en las talbas pacientes y turnopaciente nos devuelve todos los idpaciente que hay en ambas tablas
SELECT *
FROM Paciente P --ponemos un alias P para diferenciar los datos que tiene el mismo nombre en ambas tablas
INNER JOIN TurnoPaciente T --Aqu� vinculamos ambas tablas y ponemos un alias T para diferenciar los datos a buscar
ON T.idPaciente = P.idPaciente --IMPORTANTE identificar los campos con sus Alias. Sino nos dar� error

--Buscamos coincidencias idMedico en las tablas medico y medicoEspecialidad
SELECT *
FROM Medico M
INNER JOIN MedicoEspecialidad ME
ON M.idMedico = ME.idMedico

--LEFT JOIN Busca todos los registros de la izquierda ylos que comparta esta tabla
--Buscamos todos los idpaciente de la tabla paciente y que tengan su idpaciente en la tabla turno
SELECT *
FROM Paciente P --ponemos un alias P para diferenciar los datos que tiene el mismo nombre en ambas tablas
LEFT JOIN TurnoPaciente T --Aqu� vinculamos ambas tablas y ponemos un alias T para diferenciar los datos a buscar
ON T.idPaciente = P.idPaciente --IMPORTANTE identificar los campos con sus Alias. Sino nos dar� error

--Buscamos todos los idmedico de la tabla medico y que tengan su idmedico en la tabla medicoespecialidad, o sea todos.
SELECT *
FROM Medico M
LEFT JOIN MedicoEspecialidad ME
ON M.idMedico = ME.idMedico

--RIGHT JOIN Busca todos los registros de la derecha ylos que comparta esta tabla
--Buscamos todos los idpaciente de la tabla paciente y que tengan su idpaciente en la tabla turno
SELECT *
FROM Paciente P --ponemos un alias P para diferenciar los datos que tiene el mismo nombre en ambas tablas
RIGHT JOIN TurnoPaciente T --Aqu� vinculamos ambas tablas y ponemos un alias T para diferenciar los datos a buscar
ON T.idPaciente = P.idPaciente --IMPORTANTE identificar los campos con sus Alias. Sino nos dar� error

--Buscamos todos los idmedico de la tabla medicoespecialidad y que tengan su idmedico en la tabla medico.
SELECT *
FROM Medico M
RIGHT JOIN MedicoEspecialidad ME
ON M.idMedico = ME.idMedico

--UNION nos permite realizar dos busquedas en una misma tabla y no muestra repeticiones
--Esta �b�squeda nos devuelve los estados 1 y 2 de la tabla estado. 
--Se puede utilizar para tablas con los mismos registros y mostrar, sumar ... del mismo dato de registro
SELECT *
FROM Turno
WHERE estado = 1
UNION
SELECT *
FROM Turno
WHERE estado = 2

--Solo funciona para unir una misma tabla, aunque podr�a utilizarse 
--para �scar registros iguales entre tablas no es recomendable.
--LA b�squeda da error
SELECT *
FROM Turno
UNION
SELECT *
FROM Paciente

--No se debe hacer, ya que la respuesta no es coerente.
SELECT idturno
FROM Turno
UNION
SELECT idpaciente
FROM Paciente

--UNION ALL muestra todos los campos con repeticiones.
--con UNION (solo muestra 6 registros)
SELECT *
FROM Turno
UNION
SELECT *
FROM Turno
--con UNION ALL (Aqu� nos muestra 12 registros)
SELECT *
FROM Turno
UNION ALL
SELECT *
FROM Turno
