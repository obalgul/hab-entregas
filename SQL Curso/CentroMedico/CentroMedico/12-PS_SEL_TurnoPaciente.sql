--Stored Procedure para obtener los turnos de un paciente

exec SELECT_TurnosPaciente 1 --ejecutamos la b�squeda. ESTE ES EL ULTIMO PASO.

CREATE PROC SELECT_TurnosPaciente(
		@idpaciente paciente
		)

AS
set nocount on

SELECT *
FROM Paciente P --b�scamos en la tabla paciente un idpaciente
INNER JOIN TurnoPaciente TP --que coincida con un turno en la tabla Turnopaciente
ON TP.idpaciente = P.idPaciente --B�scamos por idpaciente
INNER JOIN Turno T --Adem�s a�adimos de la tabla turno el idturno del paciente
ON T.idTurno = TP.idTurno --Buscamos el idturno
AND TP.idPaciente = P.idPaciente --Y el id paciente
INNER JOIN MedicoEspecialidad M --b�camos al medico en la tabla medicoespecialista  
ON M.idMedico= TP.idMedico --B�scando por el idmedico
WHERE P.idpaciente = @idpaciente --Por �ltimo buscamos en todas las tablas implicadas el idpaciente