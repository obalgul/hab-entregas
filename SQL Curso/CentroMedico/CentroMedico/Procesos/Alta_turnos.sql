
CREATE PROC Alta_Turno(
		@fecha char(14), --formato a�o mes d�a (20001231 12:00) 
		@idpaciente paciente,
		@idmedico medico,
		@observacion observacion =''
		)
AS 

IF NOT EXISTS(
		  SELECT TOP 1 idturno --realiza la b�squeda m�s r�pido que si ponemos *
		  FROM Turno
		  WHERE fechaturno = @fecha
		  )
	BEGIN 
		INSERT INTO Turno (
							fechaturno,
							estado,
							observacion
							  )
		VALUES (
				@fecha,
				0,
				@observacion
				)

		DECLARE @auxIdturno turno
		SET @auxIdturno = @@IDENTITY --con @@IDENTITY busca el �ltimo registro del INSERT anterior introducido en la tabla

		INSERT INTO TurnoPaciente (
									idturno,
									idpaciente,
									idmedico
									)
		VALUES(
				@auxIdturno,
				@idpaciente,
				@idmedico
				)

		PRINT 'El turno se agreg� correctamente'
		RETURN
	END
	ELSE
		BEGIN
			PRINT 'El turno ya existe en la base de datos'
		END