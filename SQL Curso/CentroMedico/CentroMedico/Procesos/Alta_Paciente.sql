

EXEC Alta_Paciente '98451278', 'Carolina', 'Cruz', '19870926', 'Calle Flor n�4', 'MEX', '884486987', 'Carol@lina.ex', ''

ALTER PROC Alta_Paciente(
		@dni varchar(20),
		@nombre varchar(50),
		@apellido varchar(50),
		@fNacimiento varchar(8),
		@domicilio varchar(50),
		@idpais char(3),
		@telefono varchar (20)='',
		@email varchar (30),
		@observacion observacion= ''
		)
AS 

IF NOT EXISTS(
		  SELECT *
		  FROM Paciente
		  WHERE dni= @dni
		  )
	BEGIN 
		INSERT INTO Paciente (
							  dni, 
							  nombre, 
							  apellido, 
							  fNacimiento, 
							  domicilio, 
							  idpais, 
							  telefono, 
							  email, 
							  observacion
							  )
		VALUES (
				@dni,
				@nombre,
				@apellido,
				@fNacimiento,
				@domicilio,
				@idpais,
				@telefono,
				@email,
				@observacion
				)
		PRINT 'El paciente se agrego correctamente'
		RETURN
	END
	ELSE
		BEGIN
			PRINT 'El paciente ya existe en la base de datos'
		END