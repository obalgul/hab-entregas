
/*
select * from Medico
select * from MedicoEspecialidad
*/



--EXEC ALTA_medico 'Gerardo','Martinez',1,'Medico residente'

CREATE PROC ALTA_Medico(
			@nombre varchar(50),
			@apellido varchar(20),
			@idespecialidad int,
			@descripcion varchar(50)
			)

AS
SET NOCOUNT ON

IF NOT EXISTS(
			  SELECT TOP 1 idmedico 
			  FROM Medico 
			  WHERE nombre=@nombre and apellido = @apellido
			  )
BEGIN
	
	INSERT INTO Medico (
						nombre, 
						apellido
						)
	VALUES (
			@nombre,
			@apellido
			)
		
	DECLARE @auxIdmedico int
	SET @auxIdmedico = @@IDENTITY

	INSERT INTO MedicoEspecialidad (
									Idmedico,
									idespecialidad,
									descripcion
									)
	VALUES (
			@auxIdmedico, 
			@idespecialidad,
			@descripcion
			)

	PRINT 'El M�dico se agreg� correctamente'
	RETURN

	
END
ELSE
	BEGIN
		PRINT 'El M�dico ya existe.'
		RETURN
	END

