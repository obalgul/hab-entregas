
/*
select * from turno
select * from paciente
select * from turnopaciente
select * from medico
insert into medico values(2,'Pablo','Ramirez')
*/

--EXEC ALTA_turno '20190218 09:15',6,1,'Nada'

CREATE PROC ALTA_Turno(
			@fecha char(14), --20190215 12:00
			@idpaciente paciente,
			@idmedico medico,
			@observacion observacion=''
			)

AS

/*
ESTADO = 0 (pendiente)
ESTADO = 1 (realizado)
ESTADO = 2 (cancelado)

*/
SET NOCOUNT ON

IF NOT EXISTS(SELECT TOP 1 idturno from Turno WHERE fechaturno=@fecha)
BEGIN
	INSERT INTO Turno (
						fechaturno,
						estado,
						observacion
						)
	VALUES (
			@fecha,
			0,
			@observacion
			)

	DECLARE @auxIdturno turno
	SET @auxIdturno = @@IDENTITY --Sirve para recoger el �itimo registro del anterior INSERT, para utilizarlo en 
								 --la siguiente INSERT

	INSERT INTO TurnoPaciente (
								idturno,
								idpaciente,
								idmedico
								)
	VALUES (
			@auxIdturno,
			@idpaciente,
			@idmedico
			)

	PRINT 'El turno se agreg� correctamente'
	RETURN
	
END
ELSE
	BEGIN
		PRINT 'El turno ya existe.'
		RETURN
	END