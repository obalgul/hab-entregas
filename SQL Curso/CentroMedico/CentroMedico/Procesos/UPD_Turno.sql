


/*
select * from paciente
select * from turno
select * from turnopaciente

*/

--EXEC upd_turno 10,2,'El paciente se retrasa'



CREATE PROC UPD_Turno(
				@idturno turno,
				@estado tinyint,
				@observacion observacion)
				
AS
set nocount on

if exists(
			SELECT * 
			FROM Turno
			WHERE idturno = @idturno
			)
		UPDATE Turno 
		SET estado = @estado,
			observacion = @observacion
		WHERE idturno = @idturno


ELSE
	SELECT 0 as resultado

