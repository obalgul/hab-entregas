--Eliminar turno de paciente

/*
select * from turno
select * from turnopaciente
*/

--exec DEL_Turno 8

CREATE PROC DEL_Turno(
				@idturno turno
				)			
AS
set nocount on

if exists(
			SELECT * 
			FROM Turno
			WHERE idturno = @idturno
			)
	BEGIN
		DELETE FROM Turno
		WHERE idturno = @idturno
		DELETE FROM TurnoPaciente
		WHERE idturno = @idturno
	END
ELSE
	SELECT 0 AS Resultado_consulta