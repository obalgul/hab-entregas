--Transacciones se utiliza para evaluar las manipulaci�n de datos en porciones del c�digo. 
SELECT * 
FROM paciente

--Tracacci�n de tel�fonos en la tabla paciente
--Cuando se ejecutra la Transacci�n, la tabla estar� bloqueada hasta que realicemos un COMMIT o un ROLLBACK
--ejemplo UPDATE correcto
BEGIN TRAN
	UPDATE paciente 
	SET telefono=444
	WHERE idpaciente=6
IF @@ROWCOUNT = 1  --COn @@ROWCOUNT, nos indica la cantidad de cambios realizados.
	COMMIT TRAN --Nos confirma que ha realizado el UPDATE
ELSE
	ROLLBACK TRAN --Si modifica m�s campos de los indicados el ROLLBACK invalida el update.

--ejemplo UPDATE incorrecto, Detecta varios registros, no los cambia porque actua el ROLLBACK
--al haber m�s de cambio.
BEGIN TRAN
	UPDATE paciente 
	SET telefono=777
	WHERE apellido='Marin'
IF @@ROWCOUNT = 1  --COn @@ROWCOUNT, nos indica la cantidad de cambios realizados.
	COMMIT TRAN --Nos confirma que ha realizado el UPDATE
ELSE
	ROLLBACK TRAN --Si modifica m�s campos de los indicados el ROLLBACK invalida el update.

--ejemplo con TRAN y DELETE, Intentamos borrar una usuaria con nombre EVA, por su nommbre
--Indicamos que solo puede borrar un registro, pero al encontrarse 3 con el mismo nombre no borra ninguno.
BEGIN TRAN
	DELETE paciente 
	WHERE nombre='Eva'
IF @@ROWCOUNT = 1  --COn @@ROWCOUNT, nos indica la cantidad de cambios realizados.
	COMMIT TRAN --Nos confirma que ha realizado el UPDATE
ELSE
	ROLLBACK TRAN --Si modifica m�s campos de los indicados el ROLLBACK invalida el update.
