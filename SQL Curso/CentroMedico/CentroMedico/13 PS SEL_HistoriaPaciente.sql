--Obtener una historia cl�nica de un paciente



CREATE PROC SEL_HistoriaPaciente(
					@idpaciente paciente
					)

AS
set nocount on

/*
select * from historia
select * from historiapaciente
*/

SELECT * 
FROM Paciente P
INNER JOIN HistoriaPaciente HP
ON HP.idpaciente = P.idpaciente
INNER JOIN Historia H
ON H.idHistoria = HP.idHistoria
INNER JOIN MedicoEspecialidad ME
ON ME.idMedico = HP.idMedico
INNER JOIN Medico M
ON M.idmedico = ME.idMedico
WHERE P.idpaciente = @idpaciente

