--TRIGGER es un lanzador de eventos, lo realiza con INSERT UPDATE y DELETE
--Nos permite realizar acciones varias, como cargar el registro en otra tabla, dar una alerta...

--TRIGGER para guardar un log

select * from PacienteLog 

CREATE TRIGGER PacientesCreados 
ON Paciente
AFTER INSERT
AS

IF(
	SELECT idpais 
	FROM inserted
	) = 'MEX'
	INSERT INTO PacienteLOg (idpaciente, idpais, fechaAlta)
		SELECT i.idpaciente,i.idpais, getdate() 
		FROM Inserted i