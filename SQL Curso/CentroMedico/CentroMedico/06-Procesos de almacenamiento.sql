USE [CentroMedico]
GO
/****** Object:  StoredProcedure [dbo].[S_paciente]    Script Date: 29/03/2021 15:26:22 ******/
SET ANSI_NULLS ON --En las busquedas no mostrará los campos que se encuentren vacíos. En OFF si mostrará los campos que estén vacios
GO
SET QUOTED_IDENTIFIER ON --Permite crear objetos con nombres de palabras reservadas. Si se pone en OFF, no nos o permitirá
GO


--Procesos almacenados.
--Su objetivo es crear un conjunto de ejecuciones en una sola ejecución.
--Por ejemplo la inserción de varios registros.

ALTER PROC [dbo].[S_paciente]( --ALTER modifica nuestro proceso. Para realizar uno nuevo, sustituimos ALTER por CREATE
		@idpaciente int
)
AS

SELECT *
FROM paciente
WHERE idpaciente = @idpaciente

