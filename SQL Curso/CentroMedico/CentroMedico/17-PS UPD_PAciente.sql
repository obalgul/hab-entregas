--Actualizar un paciente

exec UPD_Paciente 3,'12345678','Gumersindo','Capirote','19880621','353698745','gumer@gumer.bra','diabetico'

CREATE PROC UPD_Paciente(
		@idpaciente paciente,
		@dni varchar(20),
		@nombre varchar(50),
		@apellido varchar(50),
		@fNacimiento varchar(8),
		@telefono varchar(20),
		@email varchar(30),
		@observacion observacion
)

AS
set nocount on

IF EXISTS(
			SELECT *
			FROM Paciente
			WHERE idpaciente = @idpaciente
			)
		UPDATE Paciente
		SET dni = @dni,
			nombre = @nombre,
			apellido = @apellido,
			fNacimiento = @fNacimiento,
			telefono = @telefono,
			email = @email,
			observacion = @observacion
		WHERE idPaciente = @idPaciente
ELSE
	SELECT 0 AS Resultado_consulta