//----------requerimos el modulo dotenv, para guardar variables de entorno------------
require("dotenv").config();

//-------------------------importamos módulos de npm------------------------------
const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");

//------------------------------midleares------------------------------
const entryExists = require("./middlewares/entryExists");
const isUser = require("./middlewares/isUser");
const isAdmin = require("./middlewares/isAdmin");

//------------------------Controllers de usuarios---------------------
const newUser = require("./controllers/users/newUser");
const validateUser = require("./controllers/users/validateUser");
const loginUser = require("./controllers/users/loginUser");
const editUser = require("./controllers/users/editUser");
const deleteUser = require("./controllers/users/deleteUser");
const editPasswordUser = require("./controllers/users/editPasswordUser");
const recoveryPasswordUser = require("./controllers/users/recoveryPasswordUser");
const resetPasswordUser = require("./controllers/users/resetPasswordUser");
const buyerDataUser = require("./controllers/users/buyerDataUser");
const sellerDataUser = require("./controllers/users/sellerDataUser");

//------------------------controllers de artículos--------------------
const listArticle = require("./controllers/articles/listArticle");
const newArticle = require("./controllers/articles/newArticle");
const editArticle = require("./controllers/articles/editArticle");
const deleteArticle = require("./controllers/articles/deleteArticle");
const reserveArticle = require("./controllers/articles/reserveArticle");
const reserveArticleConfirm = require("./controllers/articles/reserveArticleConfirm");
const deleteArticleImage = require("./controllers/articles/deleteArticleImage");
const uploadArticleImage = require("./controllers/articles/uploadArticleImage");
const voteArticle = require("./controllers/articles/voteArticle");

//--------------------------ejecutamos express------------------------------
const app = express();

//----------------------------- Middlewares iniciales-------------------

// Log de peticiones a la consola
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// Procesado de body tipo json
app.use(bodyParser.json());

// Procesado de body tipo form-data
app.use(fileUpload());

//---------------------------ENDPOINTS DE USUARIO---------------------

// Registro de usuarios
// POST - /users
// Público
app.post("/users", newUser);

// Validación de usuarios registrados
// GET - /users/validate/:code
// Público
app.get("/users/validate/:code", validateUser);

// Login de usuarios
// POST - /users/login
// Público
app.post("/users/login", loginUser);

// Editar datos de usuario: email, name
// PUT - /users/:id
// Sólo el propio usuario o el usuario admin
app.put("/users/:id", isUser, editUser);

// Ver información de compras de un usuario
// GET - /users/:id/buy
// Sólo para el usuario registrado o admin
app.get("/users/:id/buy", isUser, buyerDataUser);

// Ver información de ventas de un usuario
// GET - /users/:id/sale
// Sólo para el usuario registrado o admin
app.get("/users/:id/sale", isUser, sellerDataUser);

// Borrar un usuario
// DELETE- /users/:id
// Sólo el admin
app.delete("/users/:id", isUser, isAdmin, deleteUser);

// Editar password de usuario
// POST - /users/:id/password
// Sólo el propio usuario
app.post("/users/:id/password", isUser, editPasswordUser);

// Enviar código de reset de password
// POST - /users/recovery-password
// Público
app.post("/users/recovery-password", recoveryPasswordUser);

// Resetear password de usuario
// POST - /users/reset-password
// Público
app.post("/users/reset-password", resetPasswordUser);

//--------------------------ENDPOINTS DE ARTICULOS--------------------

// Listar multiples entradas de artículos
// GET - /articles
// Público
app.get("/articles", listArticle);

// Crear una nueva entrada de artículo
// POST - /articles
// Sólo usuarios registrados
app.post("/articles", isUser, newArticle);

// Editar un artículo
// PUT - /articles/:id
// Sólo usuario que creara esta entrada o admin
app.put("/articles/:id", isUser, entryExists, editArticle);

// Borrar una imagen de una entrada
// DELETE /articles/:id/images/:imageID
// Solo usuario que creara esa entrada o admin
app.delete(
  "/articles/:id/images/:imageID",
  isUser,
  entryExists,
  deleteArticleImage
);

// Añadir una imagen a un artículo
// POST /articles/:id/images
// Solo usuario que crear esta entrada o admin
app.post("/articles/:id/images", isUser, entryExists, uploadArticleImage);

// Borrar un artículo
// DELETE - /articles/:id
// Sólo admin
app.delete("/articles/:id", isUser, entryExists, deleteArticle);

// Crear una reserva
// POST - /article/:id/reserve
// Solo usuarios registrados
app.post("/articles/:id/reserve", isUser, reserveArticle);

//Confirmar una reserva
//POST - /articles/:id/confirm
// Solo usuarios registrados
app.post("/articles/:id/confirm", isUser, reserveArticleConfirm);

// Votar una entrada
// POST - /articles/:id/vote
// Sólo usuarios registrados que hayan hecho una reserva
app.post("/articles/:id/vote", isUser, entryExists, voteArticle);

//--------------------------- Middlewares finales---------------------

// Error middleware
app.use((error, req, res, next) => {
  console.error(error);

  res.status(error.httpStatus || 500).send({
    status: "error",
    message: error.message,
  });
});

// Not found
app.use((req, res) => {
  res.status(404).send({
    status: "error",
    message: "Not found",
  });
});

//-----------------------------Puerto de conexión del server---------------------------
const port = process.env.PORT;

//----------------------------ejecutamos express para qu eescuche cambios--------------
//-------------------------------dentro del código y mensaje de que está---------------
//---------------------------------------------fucionando------------------------------
app.listen(port, () => {
  console.log(`API funcionando en http://localhost:${port} 🙈`);
});
