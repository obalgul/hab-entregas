//-------------------------importamos módulos de npm------------------------------
const Joi = require("@hapi/joi");

//--------------------importamos función del archivo helpers-----------------
const { generateError } = require("../helpers");

//-----------validación de entrada de nuevos articulos-----------------
//de los campos categoria, nombre, descripción, precio, localización,
//lugar de entrega y clasificación del usuario
const newArticlesSchema = Joi.object().keys({
  category: Joi.string()
    .min(1)
    .max(75)
    .required()
    .error(
      generateError(
        "Categoría tiene que tener como mínimo 1 caracteres y 75 como máximo.",
        400
      )
    ),
  brand: Joi.string()
    .min(1)
    .max(75)
    .required()
    .error(
      generateError(
        "El campo marca del artículo tiene que tener como mínimo 1 caracteres y 50 como máximo",
        400
      )
    ),
  description: Joi.string()
    .min(5)
    .max(500)
    .required()
    .error(
      generateError(
        "La descripción del artículo tiene que tener como mínimo 5 caracteres y 500 como máximo",
        400
      )
    ),
  price: Joi.number()
    .required()
    .error(generateError("EL campo de precio tiene que estar cubierto.", 400)),
  province: Joi.string()
    .min(1)
    .max(75)
    .required()
    .error(
      generateError(
        "La provincia tiene que tener como mínimo 5 caracteres y 100 como máximo.",
        400
      )
    ),
});

//----------------------validación edición de artículos---------------
// igual que la entrada de nuevos artículos
const editArticleSchema = newArticlesSchema;

//-----------------Validación de confirmación de artículo-------------
const reserveArticleConfirmSchema = Joi.object().keys({
  address: Joi.string()
    .min(5)
    .max(70)
    .required()
    .error(
      generateError(
        "La dirección de encuentro tiene que existir y estar cubierto y ser como mínimo 5 caracteres y 100 como máximo.",
        400
      )
    ),
  time: Joi.string()
    .max(5)
    .required()
    .error(
      generateError(
        "La hora tiene que existir y ser formato hh:mm. Ejemplo 12:30",
        400
      )
    ),
  day_week: Joi.string()
    .required()
    .error(
      generateError(
        "Este campo es obligatorio, tiene que contener un día de al semana.",
        400
      )
    ),
});

//--------------------------validación del voto------------------------
const voteArticleSchema = Joi.object().keys({
  vote: Joi.number()
    .min(1)
    .max(5)
    .required()
    .error(generateError("El voto tiene que ser entre 1 y 5.", 400)),
});
//-----------------------Exportamos las validaciones------------------
module.exports = {
  newArticlesSchema,
  editArticleSchema,
  reserveArticleConfirmSchema,
  voteArticleSchema,
};
