//-------------------------importamos módulos de npm------------------------------
const Joi = require("@hapi/joi");

//--------------------importamos función del archivo helpers-----------------
const { generateError } = require("../helpers");

//---------------Validación usuarios nuevos-----------------------
const newUserSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required()
    .error(generateError("La cuenta de correo debe existir y ser válida", 400)),
  password: Joi.string()
    .min(8)
    .required()
    .error(generateError("La debe existir y ser mayor de 8 caracteres", 400)),
});

//---------------Validación login de usuarios-----------------------
const loginUserSchema = newUserSchema;

//---------------Validación edición de usuarios-----------------------
const editUserSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required()
    .error(generateError("La cuenta de correo debe existir y ser válida", 400)),
  name: Joi.string()
    .min(2)
    .max(50)
    .error(
      generateError(
        "El campo nombre tiene que de tener mínimo 2 caracteres y no más de 50 caracteres",
        400
      )
    ),
  last_name: Joi.string()
    .min(2)
    .max(50)
    .error(
      generateError(
        "El campo nombre tiene que de tener mínimo 2 caracteres y no más de 50 caracteres",
        400
      )
    ),
  phone: Joi.string()
    .min(9)
    .max(20)
    .error(
      generateError(
        "El campo teléfono no puede ser superior a 20 dígitos.",
        400
      )
    ),
  address: Joi.string()
    .min(5)
    .max(75)
    .error(
      generateError(
        "La dirección no puede ser menor de 5 caracteres y no superar los 75 de longitud.",
        400
      )
    ),
  city: Joi.string()
    .min(3)
    .max(50)
    .error(
      generateError(
        "El campo ciudad es obligatorio y tiene que tener entre 3 y 50 caracteres.",
        400
      )
    ),
});

//---------------Validación cambio de password-----------------------
const editPasswordUserSchema = Joi.object().keys({
  oldPassword: Joi.string()
    .min(8)
    .required()
    .error(
      generateError(
        "La oldPassword tiene existir y ser mayor de 8 caracteres",
        400
      )
    ),
  newPassword: Joi.string()
    .min(8)
    .required()
    .invalid(Joi.ref("oldPassword"))
    .error(
      generateError(
        "La newPassword tiene que existir, ser diferente a la oldPassword y ser mayor de 8 caracteres",
        400
      )
    ),
});

//---------------Validación de rocovery password-----------------------
const recoveryPasswordUserSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required()
    .error(generateError("El email tiene que ser válido", 400)),
});

//---------------Validación de reset password-----------------------
const resetPasswordUserSchema = Joi.object().keys({
  recoveryCode: Joi.string()
    .length(40)
    .required()
    .error(
      generateError(
        "El recoverCode tiene que ser una cadena de 40 caracteres",
        400
      )
    ),
  newPassword: Joi.string()
    .min(8)
    .required()
    .error(
      generateError(
        "La newPassword tiene que existir, y ser mayor de 8 caracteres",
        400
      )
    ),
});

//--------------Exportación de los módulos-----------------------
module.exports = {
  newUserSchema,
  loginUserSchema,
  editUserSchema,
  editPasswordUserSchema,
  recoveryPasswordUserSchema,
  resetPasswordUserSchema,
};
