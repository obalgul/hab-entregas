const { getConnection } = require("../../db");
const { generateError } = require("../../helpers");

async function deleteUser(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { id } = req.params;

    // Compruebo que existe el usuario
    const [current] = await connection.query(
      `
      SELECT id
      FROM users
      WHERE id=?
    `,
      [id]
    );
    if (req.auth.id !== Number(id) && req.auth.role !== 1) {
      throw generateError("No tienes permisos para editar este usuario", 403);
    }
    if (current.length === 0) {
      throw generateError(
        `Upsss, No nos consta ningún usuario con id ${id} en nuestra base de datos`,
        404
      );
    }

    //Borramos el usuario de la base de datos
    await connection.query(
      `
      DELETE FROM users
      WHERE id=?
    `,
      [id]
    );

    res.send({
      status: "ok",
      message: `El usuario con id ${id} fue eliminado de la base de datos por el TODO PODEROSO "ADMINISTRADOR".`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = deleteUser;
