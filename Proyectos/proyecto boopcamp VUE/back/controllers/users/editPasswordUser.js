const { getConnection } = require("../../db");
const { generateError } = require("../../helpers");

const { editPasswordUserSchema } = require("../../validators/userValidators");

async function editPasswordUser(req, res, next) {
  let connection;

  try {
    //validamos la oldPassword y la newPassword
    await editPasswordUserSchema.validateAsync(req.body);

    connection = await getConnection();

    const { id } = req.params;
    const { oldPassword, newPassword } = req.body;

    //comprobamos que el usuario hace la petición es el
    //mismo que la quiere cambiar
    if (req.auth.id !== Number(id)) {
      throw generateError(
        "Upsss, no se permite cambiar la password de otro usuario., 403"
      );
    }

    //comprobamos que la password antigua no sea igual que la nueva
    const [currentUser] = await connection.query(
      `
        SELECT id
        FROM users
        WHERE id=? AND password=SHA2(?, 512)
        `,
      [id, oldPassword]
    );

    if (currentUser.length === 0) {
      const error = new Error("Upsss, La contraseña antigua no es correcta.");
      error.httpStatus = 401;
      throw error;
    }

    //guardamos la password en la base de datos
    await connection.query(
      `
        UPDATE users
        SET registration_date=UTC_TIMESTAMP, password=SHA2(?, 512), update_date=UTC_TIMESTAMP, lastAuthUpdate=UTC_TIMESTAMP
        WHERE id=?
        `,
      [newPassword, id]
    );

    //respondemos
    res.send({
      status: "ok",
      message:
        "La password ha sido actualizada correctamente. Vuelve a hacer login para volver a entrar.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = editPasswordUser;
