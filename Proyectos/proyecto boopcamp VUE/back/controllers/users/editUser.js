const { getConnection } = require("../../db");
const { randomString, sendMail, generateError } = require("../../helpers");

const { editUserSchema } = require("../../validators/userValidators");

async function editUser(req, res, next) {
  let connection;

  try {
    //validamos los datos del req.body
    await editUserSchema.validateAsync(req.body);

    connection = await getConnection();

    const { id } = req.params;
    const { email, name, last_name, phone, address, city } = req.body;

    // Comprobar que el id de usuario que queremos cambiar es
    // el mismo que firma la petición o bien es admin
    if (req.auth.id !== Number(id) && req.auth.role !== 1) {
      throw generateError(
        "Vaya, no tienes permisos para editar este usuario",
        403
      );
    }

    // Comprobar que el usuario existe
    const [currentUser] = await connection.query(
      `
      SELECT id, email
      FROM users
      WHERE id=?
    `,
      [id]
    );

    if (currentUser.length === 0) {
      throw generateError(`Upsss, el usuario con id ${id} no existe`, 404);
    }

    // Comprobamos si el email es diferente al
    //actual y si no existe en la base de datos
    if (email !== currentUser[0].email) {
      const [existingEmail] = await connection.query(
        `
        SELECT id
        FROM users
        WHERE email=? 
      `,
        [email]
      );

      if (existingEmail.length > 0) {
        throw generateError(
          "Ooooh, Ya existe un usuario con este misma dirección de correo en la base de datos",
          403
        );
      }

      // Verificamos de nuevo el email recibido
      const registrationCode = randomString(40);
      const validationURL = `${process.env.PUBLIC_HOST}/users/validate/${registrationCode}`;

      //Enviamos la url anterior por mail
      try {
        await sendMail({
          email,
          title:
            "Has cambiado tu dirección de correo en SOHPTEC. Por favor valida de nuevo",
          content: `Para validar otra vez tu email SOHPTEC haz click sobre el enlace: ${validationURL}`,
        });
      } catch (error) {
        throw generateError(
          "Upsss, Hemos sufrido un error en el envío de mail",
          500
        );
      }

      //actualizamos los datos en  la base de datos
      await connection.query(
        //email, name, last_name, phone, address
        `
        UPDATE users 
        SET registration_date=UTC_TIMESTAMP, email=?, name=?, last_name=?, phone=?, address=?, city=?, active=0, registration_code=?, update_date=UTC_TIMESTAMP, lastAuthUpdate=UTC_TIMESTAMP
        WHERE id=?
      `,
        [email, name, last_name, phone, address, city, registrationCode, id]
      );

      // Dar una respuesta
      res.send({
        status: "ok",
        message: "Usuario actualizado. Mira tu email para activarlo de nuevo.",
      });
    } else {
      // Actualizar usuario en la base de datos
      // si no cambia el correo
      await connection.query(
        `
        UPDATE users 
        SET registration_date=UTC_TIMESTAMP, email=?, name=?, last_name=?, phone=?, address=?, city=?, update_date=UTC_TIMESTAMP, lastAuthUpdate=UTC_TIMESTAMP
        WHERE id=?
    `,
        [email, name, last_name, phone, address, city, id]
      );

      // Dar una respuesta
      res.send({
        status: "ok",
        message: "Usuario actualizado",
      });
    }
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = editUser;
