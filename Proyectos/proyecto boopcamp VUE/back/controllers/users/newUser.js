//----------------------------importamos la conexión a la base de datos-----------------
const { getConnection } = require("../../db");

//--------------------------imprtamos funciones del archivo helpers---------------------
const { randomString, sendMail, generateError } = require("../../helpers");

//------------------------------------importamos la validación para usuarios------------
const { newUserSchema } = require("../../validators/userValidators");

async function newUser(req, res, next) {
  let connection;

  try {
    //validamos email y password
    await newUserSchema.validateAsync(req.body);

    connection = await getConnection();

    const { email, password } = req.body;

    // comprobar que no existe un usuario con ese mismo email en la base de datos
    const [existingUser] = await connection.query(
      `
      SELECT id 
      FROM users
      WHERE email=?
    `,
      [email]
    );

    if (existingUser.length > 0) {
      throw generateError(
        "Upss, va a ser que ya existe un usuario en la base de datos con ese email",
        409
      );
    }

    // enviar un mensaje de confirmación de registro al email indicado
    // ejemplo url validación:
    // http://localhost:3000/users/validate/454e5109e4f3245c63be6fddb9ab05e4296ad1c6

    const registrationCode = randomString(40);
    const validationURL = `${process.env.PUBLIC_HOST}/users/validate/${registrationCode}`;

    //Enviamos la url anterior por mail
    try {
      await sendMail({
        email,
        title: "Valida tu cuenta de usuario en de SOHPTEC",
        content: `Para validar tu cuenta de usuario en SOHPTEC haz click sobre el enalce: ${validationURL}`,
      });
    } catch (error) {
      throw generateError(
        "Opsss, Ha ocurrido un error en el envío de mail",
        500
      );
    }

    // metemos el nuevo usuario en la base de datos sin activar
    await connection.query(
      `
      INSERT INTO users(registration_date, email, password, registration_code, Update_date, lastAuthUpdate)
      VALUES(UTC_TIMESTAMP, ?, SHA2(?, 512), ?, UTC_TIMESTAMP, UTC_TIMESTAMP)
    `,
      [email, password, registrationCode]
    );

    //devolvemos una respuesta
    res.send({
      status: "ok",
      message:
        "Bien! parece que todo funciona, has registrado tu usuario. Mira tu correo para activarlo. Si no lo ves en la bandeja de entrada, por favos mira en la carpeta de SPAM.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

//---------------------------------------exportamos el modulo------------------------------------
module.exports = newUser;
