const { getConnection } = require("../../db");
const {
  generateError,
  processAndSaveImage,
  showDebug,
} = require("../../helpers");
const { newArticlesSchema } = require("../../validators/articlesValidators");

async function newArticle(req, res, next) {
  let connection;
  try {
    //Ponemos el precio para que solo permita números
    req.body.price = Number(req.body.price);

    //validamos los campos del req.body.
    await newArticlesSchema.validateAsync(req.body);

    connection = await getConnection();
    //sacamos los datos que necesitamos para crear el articulo
    const { category, brand, description, price, province } = req.body;

    const { id } = req.auth;

    const [result] = await connection.query(
      `
    INSERT INTO articles(creation_date, category, brand, description, price, province,  state, update_date, id_seller)
    VALUES(UTC_TIMESTAMP, ?, ?, ?, ?, ?, false, UTC_TIMESTAMP, ?)
    `,
      [category, brand, description, price, province, id]
    );

    const images = [];

    if (req.files && Object.keys(req.files).length > 0) {
      for (const [imageName, imageData] of Object.entries(req.files).slice(
        0,
        3
      )) {
        try {
          showDebug(imageName);

          const processImage = await processAndSaveImage(imageData);
          images.push(processImage);

          // realizamos la query
          await connection.query(
            `
            INSERT INTO articles_images(image, save_image_date, id_article)
            VALUES(?,UTC_TIMESTAMP,?)
        `,
            [processImage, result.insertId]
          );
        } catch (error) {
          throw generateError(
            "Opsss. Vaya hemos tenido un problema, No se ha podido procesar la imagen. Inténtalo de nuevo",
            400
          );
        }
      }
    }

    //Devolver el resultado
    res.send({
      status: "ok",
      data: {
        id: result.insertId,
        category,
        brand,
        description,
        price,
        province,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = newArticle;
