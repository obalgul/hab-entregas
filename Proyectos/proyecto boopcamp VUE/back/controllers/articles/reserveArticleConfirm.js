const { getConnection } = require("../../db");
const { sendMail, randomString } = require("../../helpers");

const {
  reserveArticleConfirmSchema,
} = require("../../validators/articlesValidators");

async function reserveArticleConfirm(req, res, next) {
  let connection;

  try {
    //validamos los datos a introducir en la base de datos
    await reserveArticleConfirmSchema.validateAsync(req.body);

    connection = await getConnection();

    //SAcamos el id del artículo.
    const { id } = req.params;
    //cogemso los datos para almacenar dirección hora y día
    const { address, time, day_week } = req.body;

    //sacamos los datoos que necesitamos del artículo
    const [articleData] = await connection.query(
      `
        SELECT id, brand, description, price
        FROM articles
        WHERE id=?
        `,
      [req.params.id]
    );
    const [currentData] = articleData;

    //creamos el código de reserva que guardaremos y enviaremos
    const reserverCode = randomString(20);

    //actualizamos la preserva para que SEA una reserva con todos
    // los datos de ella
    await connection.query(
      `
      UPDATE reservation
      SET reservation_code=?, reservation_date=UTC_TIMESTAMP, address=?, time=?, day_week=?, confirmed=1, update_date=UTC_TIMESTAMP
        WHERE id_article=?
      `,
      [reserverCode, address, time, day_week, id]
    );

    //cogemos el id del vendedor para poder enviarle el mail
    const [resultData] = await connection.query(
      `
      SELECT id_buyer
      FROM reservation
      WHERE id_article=?
    `,
      [req.params.id]
    );
    const [reserverData] = resultData;

    //sacamos el correo del usuario comprodor= BUYER
    const [userData] = await connection.query(
      `
        SELECT id, email
        FROM users
        WHERE id=?
        `,
      [reserverData.id_buyer]
    );
    const [currentBuyer] = userData;

    //mandamos correo y url para votar a su vendedor
    await sendMail({
      email: currentBuyer.email,
      title: `Reserva confirmada de SOHPTEC`,
      content: `Se ha confirmado la reserva con el código ${reserverCode}, 
        del artículo marca ${currentData.brand} ${currentData.description}
        con precio de ${currentData.price}.
       
        El lugar de encuentro es ${address}, el ${day_week},
         a la siguiente hora ${time}.
         
         Para valorar a tu vendedor puedes hacerlo votando en la siguiente url:
         ${process.env.PUBLIC_HOST}/reservation/${id}/vote`,
    });

    res.send({
      status: "Ok",
      data: "Articulo reservado",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = reserveArticleConfirm;
