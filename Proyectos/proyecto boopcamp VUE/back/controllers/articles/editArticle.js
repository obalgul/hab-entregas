const { getConnection } = require("../../db");
const { generateError } = require("../../helpers");

const { editArticleSchema } = require("../../validators/articlesValidators");

async function editArticle(req, res, next) {
  let connection;

  try {
    //Ponemos el precio para que solo permita números
    req.body.price = Number(req.body.price);

    //validamos los datos a cambiar
    await editArticleSchema.validateAsync(req.body);

    connection = await getConnection();

    //sacamos los datos
    const {
      category,
      brand,
      description,
      price,
      province,
      id_seller,
    } = req.body;
    const { id } = req.params;

    //seleccionamos los datos de la entrada
    const [current] = await connection.query(
      `
        SELECT category, brand, description, price, province, update_date, id_seller
        FROM articles
        WHERE id=?
        `,
      [id]
    );

    const [currentEntry] = current;
    if (currentEntry.id_seller !== req.auth.id && req.auth.role !== 1) {
      throw generateError(
        "Opsss, va  ser que no tienes permisos de administración para editar está entrada.",
        403
      );
    }

    //ejecutamos la query de edición de artículo
    await connection.query(
      `
      UPDATE articles
      SET category=?, brand=?, description=?, price=?, province=?, update_date=UTC_TIMESTAMP
      WHERE id=?
      `,
      [category, brand, description, price, province, id]
    );

    //devolvemos el resultado
    res.send({
      status: "ok",
      data: {
        id,
        category,
        brand,
        description,
        price,
        province,
        id_seller,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = editArticle;
