const { getConnection } = require("../../db");
const { generateError, deleteUpload } = require("../../helpers");

async function deleteArticleImage(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    // sacamos los datos del articulo(id) y de la imagen (imageID)
    const { id, imageID } = req.params;
    console.log(req.params, req.auth.id);
    //seleccionamos el artículo con su id
    const [current] = await connection.query(
      `
        SELECT id_seller
        FROM articles
        WHERE id=?
        `,
      [id]
    );

    //comprobamso que el usuario puede borrar la imagen del artículo
    const [currentEntry] = current;

    if (currentEntry.id_user === req.auth.id && req.auth.role !== 1) {
      throw generateError(
        "Upsss, Va a ser que no tienes permisos de edición para este artículo.",
        403
      );
    }

    //seleccionamos la imagen
    const [image] = await connection.query(
      `
        SELECT image
        FROM articles_images
        WHERE id=? AND id_article=?
        `,
      [imageID, id]
    );

    //comprobamos que hay imagen
    if (image.length === 0) {
      throw generateError("Opsss, no encontramos la imagen.", 404);
    }

    //Borramos la imagen de base de datos y del disco
    await connection.query(
      `
        DELETE
        FROM articles_images
        WHERE id=? AND id_article=?
        `,
      [imageID, id]
    );

    await deleteUpload(image[0].image);

    //damos una respuesta
    res.send({
      status: "ok",
      message: "Imagen borrada correctamente.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = deleteArticleImage;
