const { getConnection } = require("../../db");
const { generateError, sendMail, randomString } = require("../../helpers");

async function reserveArticle(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    // - id de usuario que artículo
    //req.params.id = id artículo, req.auth.id= id usuario comprador;
    const { id } = req.params;

    //consultamos la base de datos para sacar los datos para
    // hacer la preserva. Y sacar el id_seller Usuario vendedor
    // y comprobamos si existe el artículo
    const [result] = await connection.query(
      `
    SELECT id, brand, description, id_seller
    FROM articles
    WHERE id=?
    `,
      [id]
    );

    // desectructuramos el resultasdo para obtener los
    //datos que necesitamos
    const [articleData] = result;

    // creamos una entrada en la tabla reservas como no confirmada
    await connection.query(
      `
      INSERT INTO reservation(reservation_date, id_article, id_seller, id_buyer, confirmed, update_date)
      VALUES (UTC_TIMESTAMP, ?, ?, ?, false, UTC_TIMESTAMP)
      `,
      [req.params.id, articleData.id_seller, req.auth.id]
    );

    //mandar un correo al vendedor con información de que se hizo
    //una prereserva con los datos necesarios
    try {
      // Hacemos una consulta a la base de datos para sacar el
      //email del vendedor
      const [resultData] = await connection.query(
        `
      SELECT id, email
      FROM users
      WHERE id=?
      `,
        [articleData.id_seller]
      );
      //guardamos los datos de id y email
      const [sellerData] = resultData;

      //creamos la url para confirmar la prereserva con un código
      const reserverUrlCode = randomString(40);
      const reserveConfirm = `${process.env.PUBLIC_HOST}/articles/${id}/confirm/${reserverUrlCode}`;

      //mandaaar el correo para confirmar el artículo
      await sendMail({
        email: sellerData.email,
        title: "Reserva de SOHPTEC",
        content: `Acaban de prereservar el artículo de marca ${articleData.brand}, ${articleData.description}. 
          Para activar la reserva, en el siguiente enlace debes enviar un lugar de encuentro y una hora.
          Haz click en el siguiente enlace ${reserveConfirm}. Para terminar la preserva.`,
      });
    } catch (error) {
      throw generateError("Opss, no hemos podido enviar el correo.", 500);
    }

    res.send({
      status: "Ok",
      data: "Articulo Prereservado",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = reserveArticle;
