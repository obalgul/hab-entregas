const { getConnection } = require("../../db");
const { generateError } = require("../../helpers");

async function deleteArticle(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    //sacamos el usuario
    const { id } = req.params;

    //comprobamos qu eexiste el usuario
    const [current] = await connection.query(
      `
        SELECT id
        FROM articles
        WHERE id=?
        `,
      [id]
    );

    if (current[0].user_id !== req.auth.id && req.auth.role !== 1) {
      throw generateError(
        "Uyu yuiii, No puedes borrar el artículo, no tienes permisos de administración.",
        403
      );
    }

    //Borramos de la base de datos el artículo
    await connection.query(`
        DELETE
        FROM articles
        WHERE id=?
        `);

    // enviamos respuesta
    res.send({
      status: "ok",
      message: `EL artículo con ${id} ha sido borrado correctamente`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.realease();
  }
}

module.exports = deleteArticle;
