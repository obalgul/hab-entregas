const { getConnection } = require("../../db");

async function listArticle(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    //desestruturamos los campos por los que recibiremos los datos de busqueda
    const { category, brand, price1, price2, province, city } = req.query;

    //configuramos una busqueda por defecto que nos muestre todos los artículos y la valoración del vendedor
    //esta busqueda hay que pasarla en dos partes para que no de erro.
    let sql = `SELECT A.*, U.name, U.city, AVG(R.rating) AS user_rating
FROM articles A LEFT OUTER JOIN users U ON A.id_seller = U.id
  LEFT OUTER JOIN reservation R ON R.id_seller = U.id

  `;

    //Pasamos la segunda parte de la consulta
    let sql1 = `GROUP BY A.id, U.id`;
    let count = 0;
    let parameters = [];

    //hacenmos la busqueda por campos
    if (category) {
      sql += ` WHERE category = '${category}' `;
      parameters.push(category);
      count++;
    }

    if (brand) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `brand = '${brand}' `;
      parameters.push(brand);
      count++;
    }
    if (price1) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `price >= '${price1}' `;
      parameters.push(price1);
      count++;
    }

    if (price2) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `price <= '${price2}' `;
      parameters.push(price2);
      count++;
    }

    if (province) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `province = '${province}' `;
      parameters.push(province);
      count++;
    }

    if (city) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `city = '${city}' `;
      parameters.push(city);
      count++;
    }

    //creamos una variable para guardar los datos de la query
    let queryResults;

    // hacemos la consulta de la busqueda
    queryResults = await connection.query(sql + sql1, [
      category,
      brand,
      price1,
      price2,
      province,
      city,
    ]);

    const [result] = queryResults;

    //devolvemos la consulta
    res.send({
      status: "ok",
      data: result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = listArticle;
