//------------------------------Importamos conexión a la db-----------------------
const { getConnection } = require("../db");

//-----------------------------Importamos función del archivo helpers--------------
const { generateError } = require("../helpers");

//-------------------creamos middleware para comprobar si existe un usuario--------------
async function entryExists(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { id } = req.params;

    // Comprobar que la entrada que queremos editar exista en la base de datos
    const [current] = await connection.query(
      `
    SELECT id
    FROM articles
    WHERE id=?
  `,
      [id]
    );

    //Si no existe devoldvemos un error
    if (current.length === 0) {
      throw generateError(
        `Upsss, la entrada con id ${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    } else {
      next();
    }
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

//----------------------exportamos el módulo----------------------
module.exports = entryExists;
