//-------------------------importamos módulos de npm------------------------------
const jsonwebtoken = require("jsonwebtoken");

//------------------------------Importamos conexión a la db-----------------------
const { getConnection } = require("../db");

//-----------------------------Importamos función del archivo helpers--------------
const { generateError } = require("../helpers");

async function isUser(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    // Extraer token de los headers de la petición
    const { authorization } = req.headers;

    if (!authorization) {
      throw generateError(
        "Upsss, Nos hemos dado cuenta que no tienes una cabezera autorización.",
        401
      );
    }

    // Comprobar que el token es válido
    // y decodificar el contenido del token
    let tokenInfo;
    try {
      tokenInfo = jsonwebtoken.verify(authorization, process.env.SECRET);
    } catch (error) {
      throw generateError(
        "Vaya!, tenemos un problema. El token que tienes no es válido, vuelve a hacer login para volver a acceder.",
        401
      );
    }

    // Sacamos de la base de datos información de la última vez
    // que el usuario cambió su pass o email
    const [result] = await connection.query(
      `
      SELECT lastAuthUpdate
      FROM users
      WHERE id=?
    `,
      [tokenInfo.id]
    );

    if (result.length === 0) {
      throw generateError(
        "Opsss, no hemos encontrado al usuario en nuestra base de datos.",
        401
      );
    }

    //comprobamos la validación del token y devolvemos un rror si no es válido
    const tokenCreatedAt = new Date(tokenInfo.iat * 1000);
    const userLastAuthUpdate = new Date(result[0].lastAuthUpdate);

    if (tokenCreatedAt < userLastAuthUpdate) {
      throw generateError(
        "Upsss, Tu token ya no es válido. Haz login para conseguir otro",
        401
      );
    }

    // Meter ese contenido en el objeto de petición para futuro uso
    req.auth = tokenInfo;

    // Pasar al siguiente middleware
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

//----------------------exportamos el módulo----------------------
module.exports = isUser;
