//----------requerimos el modulo dotenv, para guardar variables de entorno------------
require("dotenv").config();

//-------------------------importamos módulos de npm----------------------------------
const faker = require("faker/locale/es");

//--------importamos la ejecución de la  conexión a la base de datos------------------
const { getConnection } = require("./db");

let connection;

async function main() {
  try {
    // Conseguir conexión a la base de datos
    connection = await getConnection();

    // Borrar las tablas si existen (diary, diary_votes)
    console.log("Borrando tablas");
    await connection.query("DROP TABLE IF EXISTS reservation");
    await connection.query("DROP TABLE IF EXISTS articles_images");
    await connection.query("DROP TABLE IF EXISTS articles");
    await connection.query("DROP TABLE IF EXISTS users");

    // Crear las tablas de nuevo
    console.log("Creando tablas");

    await connection.query(`
      CREATE TABLE IF NOT EXISTS users
     (
      id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
      registration_date DATETIME NOT NULL,
      email VARCHAR (75) UNIQUE NOT NULL,
      password  VARCHAR (255) NOT NULL,
      role BOOLEAN DEFAULT FALSE,
      name VARCHAR (50),
      last_name VARCHAR (50),
      phone VARCHAR (20),
      address VARCHAR (75),
      city varchar(50),
      active BOOLEAN DEFAULT false,
      registration_code VARCHAR (100),
      recovery_code VARCHAR (100),
      update_date DATETIME,
      lastAuthUpdate DATETIME NOT NULL
     ); 
    `);

    await connection.query(`
      CREATE TABLE IF NOT EXISTS articles
     (
      id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
      creation_date DATETIME,
      category VARCHAR (75),
      brand VARCHAR (50),
      description VARCHAR (500),
      price DECIMAL (10, 2),
      province VARCHAR (75),
      state BOOLEAN DEFAULT FALSE,
      update_date DATETIME,
      id_seller INT UNSIGNED NOT NULL,
      FOREIGN KEY (id_seller) REFERENCES users (id)  ON DELETE CASCADE
      );
    `);

    await connection.query(`
     CREATE TABLE IF NOT EXISTS articles_images
     (
      id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
      image VARCHAR (255),
      save_image_date DATETIME,
      id_article INT UNSIGNED NOT NULL
      );
    `);

    await connection.query(`
     CREATE TABLE IF NOT EXISTS reservation
     (
      id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
      reservation_code VARCHAR(40),
      reservation_date DATETIME,
      address VARCHAR (100),
      time TIME,
      day_week VARCHAR(10),
      rating DECIMAL (1, 0),
      confirmed BOOLEAN DEFAULT false,
      update_date DATETIME,
      id_article INT UNSIGNED,
      FOREIGN KEY (id_article) REFERENCES articles (id)  ON DELETE CASCADE,
      id_seller INT UNSIGNED,
      FOREIGN KEY (id_seller) REFERENCES users (id)  ON DELETE CASCADE,
      id_buyer INT UNSIGNED,
      FOREIGN KEY (id_buyer) REFERENCES users (id)  ON DELETE CASCADE
      );
    `);

    // Meter datos de prueba en las tablas

    console.log("Creando usuario TOTO PODEROSO");

    await connection.query(
      `
      INSERT INTO users(registration_date, email, password, role, name, last_name, phone, address, city, active, update_date, lastAuthUpdate)
      VALUES(UTC_TIMESTAMP, "obalgul@gmail.com", SHA2("${process.env.DEFAULT_ADMIN_PASSWORD}", 512), 1, "Oscar", "Balsa", "999 999 999", "calle Suarribas Nº28", "Santiago de Compostela", 1, UTC_TIMESTAMP, UTC_TIMESTAMP)
    `
    );

    console.log("Metiendo datos de prueba en la tabla users");
    const users = 10;

    for (let index = 0; index < users; index++) {
      const email = faker.internet.email();
      const name = faker.name.firstName();
      const lastName = faker.name.lastName();
      const phone = faker.phone.phoneNumber();
      const address = faker.address.streetAddress();
      const city = faker.address.city();

      await connection.query(
        `
        INSERT INTO users(registration_date, email, password, role, name, last_name, phone, address, city, active, update_date, lastAuthUpdate)
        VALUES (UTC_TIMESTAMP, "${email}", SHA2("${process.env.DEFAULT_ADMIN_PASSWORD}", 512), 0, "${name}", "${lastName}", "${phone}", "${address}", "${city}", 1, UTC_TIMESTAMP, UTC_TIMESTAMP)
      `
      );
    }
    /*
    console.log("Metiendo datos de prueba en la tabla articles");
    await connection.query(`
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values (UTC_TIMESTAMP, 'Televisión', 'Xiaomi', 'Xiaomi Mi TV 4A 32" LED HD', 2755.9, 'Santiago de Compostela', 0, UTC_TIMESTAMP, 7);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Televisión', 'LG', 'LG 86SM9000PLA 86" LED NanoCell UltraHD 4K', 735.21, 'Santiago de Compostela', 0, UTC_TIMESTAMP, 5);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Televisión', 'Samsung', 'Samsung QE75Q85R 75" QLED UltraHD 4K', 5289.99, 'Santiago de Compostela', 0, UTC_TIMESTAMP, 2);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Cámara Dígital', 'Fujifilm', 'Fujifilm Instax Mini 9 Azul Hielo', 399.16, 'Coruña', 0, UTC_TIMESTAMP, 9);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller) 
      values(UTC_TIMESTAMP, 'Cámara Dígital', 'Panasonic', 'Panasonic Lumix DC-G90M 20MP WiFi + Objetivo 12-60mm F3.5-5.6 ASPH OIS', 574.15, 'Coruña', 0, UTC_TIMESTAMP, 8);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Cámara Dígital', 'Fujifilm', 'Fujifilm Instax Mini 90 Neo Classic Negra', 732.23, 'Coruña', 0, UTC_TIMESTAMP, 3);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Portátil', 'MSI', 'MSI GF75 Thin 9SC-277XES Intel Core i7-9750H/16GB/512GB SSD/GTX1650/17.3"', 1987.32, 'Coruña', 0, UTC_TIMESTAMP, 5);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Portátil', 'Lenovo', 'Lenovo V145-15AST AMD A4-9125/8GB/256GB SSD/15.6', 378.97, 'Vigo', 0, UTC_TIMESTAMP, 7);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Portátil', 'HP', 'HP Pavilion 15-CS3003NS Intel Core i5-1035G1/8GB/512GB SSD/GTX 1050/15.6"', 809.68, 'Coruña', 0, UTC_TIMESTAMP, 3);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'CPU', 'Acer', 'Acer Aspire XC-886 Intel Core i5-9400/8GB/1TB SSD', 1288.7, 'Coruña', 0, UTC_TIMESTAMP, 11);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'CPU', 'Acer', 'Acer Aspire XC-886 Intel Core i3-9100/8GB/256GB SSD/GT 720', 972.42, 'Coruña', 0, UTC_TIMESTAMP, 4);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'CPU', 'Acer', 'Acer Aspire XC-886 Intel Core i5-9600/16GB/2TB SSD/GT 720', 1507.74, 'Coruña', 0, UTC_TIMESTAMP, 7);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Smartphone', 'Apple', 'Apple iPhone 11 Pro Max 512GB Verde Noche Libre', 1146.43, 'Coruña', 0, UTC_TIMESTAMP, 9);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Smartphone', 'Apple', 'Apple iPhone 8 64Gb Plata Libre', 1387.2, 'Coruña', 0, UTC_TIMESTAMP, 6);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Smartphone', 'Apple', 'Apple iPhone XS 64Gb Plata Libre', 1264.07, 'Coruña', 0, UTC_TIMESTAMP, 2);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Smartphone', 'Apple', 'Apple iPhone XS Max 512Gb Gris Espacial Libre', 1763.85, 'Coruña', 0, UTC_TIMESTAMP, 10);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Cámara Reflex', 'Canon', 'Canon 2000D + Objetivo 18-55 IS II + Objetivo 50 F1.8 STM', 4039.07, 'Coruña', 0, UTC_TIMESTAMP, 6);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Cámara Reflex', 'Canon', 'Canon EOS 77D 24.2MP WiFi + EF-S 18-55mm F3.5-5.6 IS STM', 4467.15, 'Coruña', 0, UTC_TIMESTAMP, 2);
      insert into articles (creation_date, category, brand, description, price, province, state, update_date, id_seller)
      values(UTC_TIMESTAMP, 'Cámara Reflex', 'Canon', 'Canon EOS 2000D 24.1MP WiFi + EF-S 18-55mm F3.5-5.6 IS II', 4626.13, 'Coruña', 0, UTC_TIMESTAMP, 4);
    `);
    */
  } catch (error) {
    console.error(error);
  } finally {
    console.log("Todo hecho, liberando conexión");
    if (connection) connection.release();
    process.exit();
  }
}

main();
