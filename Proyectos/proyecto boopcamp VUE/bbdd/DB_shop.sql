USE DB_shop;

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE IF NOT EXISTS users
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
registration_date DATETIME NOT NULL,
email VARCHAR (75) UNIQUE NOT NULL,
password  VARCHAR (255) NOT NULL,
role BOOLEAN DEFAULT FALSE,
name VARCHAR (50),
last_name VARCHAR (50),
phone VARCHAR (20),
address VARCHAR (75),
city VARCHAR (50),
active BOOLEAN DEFAULT false,
registration_code VARCHAR (100),
recovery_code VARCHAR (100),
update_date DATETIME,
lastAuthUpdate DATETIME NOT NULL
); 

 CREATE TABLE IF NOT EXISTS articles
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
creation_date DATETIME,
category VARCHAR (75),
brand VARCHAR (50),
description VARCHAR (500),
price DECIMAL (10, 2),
province VARCHAR (75),
state BOOLEAN DEFAULT FALSE,
update_date DATETIME,
id_seller INT UNSIGNED NOT NULL,
FOREIGN KEY (id_seller) REFERENCES users (id)  ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS articles_images
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
image VARCHAR (255),
save_image_date DATETIME,
id_article INT UNSIGNED NOT NULL,
FOREIGN KEY (id_article) REFERENCES articles (id)  ON DELETE CASCADE
);
      
CREATE TABLE IF NOT EXISTS reservation
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
reservation_code VARCHAR(40),
reservation_date DATETIME NOT NULL,
address VARCHAR (100),
time TIME,
day_week VARCHAR(10),
rating DECIMAL (1, 0),
confirmed BOOLEAN DEFAULT false,
update_date DATETIME,
id_article INT UNSIGNED NOT NULL,
FOREIGN KEY (id_article) REFERENCES articles (id)  ON DELETE CASCADE,
id_seller INT UNSIGNED,
FOREIGN KEY (id_seller) REFERENCES users (id)  ON DELETE CASCADE,
id_buyer INT UNSIGNED,
FOREIGN KEY (id_buyer) REFERENCES users (id)  ON DELETE CASCADE
);

SET FOREIGN_KEY_CHECKS=1;


SELECT A.*, AVG(R.rating) AS rating
FROM articles A LEFT OUTER JOIN reservation R ON A.id=R.id_article
WHERE A.category LIKE '%?%' AND A.brand LIKE ? AND A.description LIKE ? AND A.price LIKE ? AND A.province LIKE ? 
		AND A.creation_date LIKE ? AND city LIKE ? AND rating LIKE ?
GROUP BY A.id
ORDER BY R.id_buyer ASC;


SELECT A.*, U.name, U.city, AVG(R.rating) AS user_rating
FROM articles A LEFT OUTER JOIN users U ON A.id_seller = U.id
	LEFT OUTER JOIN reservation R ON R.id_seller = U.id
WHERE A.category = 'cpu' AND A.brand = 'acer'  AND A.price > 500 AND A.price < 1500 AND A.province LIKE 'coruña' 
GROUP BY A.id, U.id



SELECT R.reservation_date, R.reservation_code, A.category, 
	A.brand, A.description, A.price, A.state, R.rating, U.name, U.last_name
FROM articles A INNER JOIN reservation R ON A.id=R.id_article
	LEFT OUTER JOIN users U ON U.id = R.id_seller
WHERE R.id_seller=4
GROUP BY R.id
ORDER BY R.id_buyer ASC

  SELECT R.reservation_date, R.reservation_code, A.category, 
	        A.brand, A.description, A.price, A.state, R.rating, U.name, U.last_name
      FROM articles A INNER JOIN reservation R ON A.id=R.id_article
	        LEFT OUTER JOIN users U ON U.id = R.id_seller
      WHERE R.id_buyer=6
      GROUP BY R.id
      ORDER BY R.id_seller ASC
      
      
SELECT R.reservation_date, R.reservation_code, A.category, 
	        A.brand, A.description, A.price, A.state, R.rating, U.name, U.last_name
      FROM articles A INNER JOIN reservation R ON A.id=R.id_article
	        LEFT OUTER JOIN users U ON U.id = R.id_seller
      WHERE R.id_buyer=2
      GROUP BY R.id
      ORDER BY R.id_buyer ASC
      
      
SELECT * FROM articles
WHERE brand  'acer
