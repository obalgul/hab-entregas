import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",

    component: () => import("../views/LoginPage.vue"),
  },
  {
    path: "/registred",
    name: "Registro",

    component: () => import("../views/RegistredPage.vue"),
  },
  {
    path: "/validate/activate",
    name: "Activate",

    component: () => import("../views/Activate.vue"),
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
  },
  {
    path: "/homeuser",
    name: "HomeUser",
    component: () => import("../views/HomeUser.vue"),
  },
  {
    path: "*",
    name: "Error",
    component: () => import("../views/Error.vue"),
  },
  {
    path: "/recoveryPassword",
    name: "RecoveryPassword",

    component: () => import("../views/RecoveryPassword.vue"),
  },
  {
    path: "/editUser",
    name: "EditUser",

    component: () => import("../views/EditUser.vue"),
  },
  {
    path: "/editArticle",
    name: "EditArticle",

    component: () => import("../views/EditArticle.vue"),
  },
  {
    path: "/newarticle",
    name: "NewArticle",

    component: () => import("../views/NewArticle.vue"),
  },
  {
    path: "/listbuy",
    name: "ListBuy",

    component: () => import("../views/ListBuy.vue"),
  },
  {
    path: "/listseller",
    name: "ListSeller",

    component: () => import("../views/ListSeller.vue"),
  },
  {
    path: "/resetpassword",
    name: "ResetPassword",

    component: () => import("../views/ResetPassword.vue"),
  },
  {
    path: "/coderesetpassword",
    name: "CodeResetPassword",

    component: () => import("../views/CodeResetPassword.vue"),
  },
  {
    path: "/confirmreserve",
    name: "ConfirmReserve",

    component: () => import("../views/ConfirmReserve.vue"),
  },
  {
    path: "/vote",
    name: "Vote",

    component: () => import("../views/Vote.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
