const fs = require("fs").promises;
const path = require("path");
const sharp = require("sharp");
const uuid = require("uuid");

const { format } = require("date-fns");

//defiimos directorio imagenes
const imageUploadPath = path.join(process.env.UPLOADS_DIR);

//fecha compatible con sql
function formatDateToDB(date) {
  return format(new Date(date), "yyyy-MM-dd HH:mm:ss");
}

//redimensionar fotos
async function processAndSaveImage(uploadedImage) {
  //creamos el directorio (con recursive: true) por si hay subdirectorios.
  await fs.mkdir(imageUploadPath, { recursive: true });

  //leer imagen que se subio
  const image = sharp(uploadedImage.data);

  //sacamos info de la imagen
  const imageInfo = await image.metadata();

  //cambiamos tamaño foto si es preciso
  if (imageInfo.width > 1000) {
    image.resize(1000);
  }

  //guardar foto en directorio UPLOADS
  const imageFileName = `${uuid.v4()}.jpg`;
  await image.toFile(path.join(imageUploadPath, imageFileName));

  //devolvemos nombre guardado
  return imageFileName;
}

//borrar fotos
async function deleteUpload(uploadedImage) {
  await fs.unlink(path.join(imageUploadPath, uploadedImage));
}

// envio de errores
function generateError(message, code = 500) {
  const error = new Error(message);
  error.httpStatus = code;
  return error;
}

//showdebug
function showDebug(message) {
  if (process.env.NODE_ENV === "development") {
    console.log(message);
  }
}

//exportamos las funciones
module.exports = {
  formatDateToDB,
  processAndSaveImage,
  deleteUpload,
  generateError,
  showDebug,
};
