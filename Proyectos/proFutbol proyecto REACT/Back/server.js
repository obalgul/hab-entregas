require("dotenv").config();

const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const cors = require("cors");

//controllers
const newTeam = require("./controllers/newTeam");
const newPlayer = require("./controllers/newPlayer");
const newStadium = require("./controllers/newStadium");
const listTeam = require("./controllers/listTeam");
const listPlayer = require("./controllers/listPlayer");
const singlePlayer = require("./controllers/singlePlayer");
const singleTeam = require("./controllers/singleTeam");
const singleStadium = require("./controllers/sigleStadium");
const stadium = require("./controllers/stadium");
const playersOneTeam = require("./controllers/playersOneTeam");
const editTeam = require("./controllers/editTeam");
const editPlayer = require("./controllers/editPlayer");
const editStadium = require("./controllers/editStadium");
const deleteTeam = require("./controllers/deleteTeam");
const deleteStadium = require("./controllers/deleteStadium");
const deletePlayer = require("./controllers/deletePlayer");

const app = express();

//middlewares iniciales

//log petición consola
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

//procesado del body tipo JSON
app.use(bodyParser.json());

//Procesado body tipo FORM-DATA
app.use(fileUpload());

app.use(cors());

app.use(express.static("static"));

//endpoints

//ruta almacén fotos
//GET - /teams
//GET - /players
//app.get("/static/uploads", listTeam);
// app.get("/listPlayer", listPlayer)

//crear un nuevo equipo
//POST -/teams
app.post("/teams", newTeam);

//crear un nuevo jugador
//POST -/players
app.post("/players", newPlayer);

//crear un nuevo estadio
//POST -/stadium
app.post("/stadiums", newStadium);

//lisar clubs
//GET - /listTeam
app.get("/listTeam", listTeam);

//lisar jugadores
//GET - /listPlayers
app.get("/listPlayer", listPlayer);

//lisar un equips
//GET - /singleTeam/:id/
app.get("/singleTeam/:id", singleTeam);

//lisar un estadio con id_team
//GET - /singleStadium/:id/
app.get("/singleStadium/:id", singleStadium);

//lisar un estadio con id del propio estadio
//GET - /stadium/:id/
app.get("/stadium/:id", stadium);

//lisar un jugador
//GET - /singlePlayer/:id/
app.get("/singlePlayer/:id", singlePlayer);

//lisar los jugadores de un equipo
//GET - /playersOneTeam/:id(id del equipo)/
app.get("/playersOneTeam/:id", playersOneTeam);

//Editar datos de un equipo
//PUT /teams/:id
app.put("/editTeam/:id", editTeam);

//Editar datos de un estadio
//PUT /stadium/:id
app.put("/editStadium/:id", editStadium);

//Editar datos de un jugador
//PUT /editPlayer/:id
app.put("/editPlayer/:id", editPlayer);

//Borrar un club
//DELETE /editTeam/:id
app.delete("/team/:id", deleteTeam);

//Borrar un jugador
//DELETE /players/:id
app.delete("/player/:id", deletePlayer);

//Borrar un estadio
//DELETE /stadium/:id
app.delete("/stadium/:id", deleteStadium);

//Middlewares finales

//Error middleware
app.use((error, req, res, next) => {
  console.error(error);

  res.status(error.httpStatus || 500).send({
    status: "error",
    message: error.message,
  });
});

//not found
app.use((req, res) => {
  res.status(404).send({
    status: "error",
    message: "Not found",
  });
});

//puerto conexión
const port = process.env.PORT;

//ejecutamos express para escuhar cambios y mensaje qu eesta fen funcionamiento
app.listen(port, () => {
  console.log(`Api en funcinamiento en http://localhost:${port} 🙈🙈🙈`);
});
