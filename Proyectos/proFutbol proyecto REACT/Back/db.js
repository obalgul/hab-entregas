require("dotenv").config();

const mysql = require("mysql2/promise");

//detructuramos variables de entorno guardadas en .env
const { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;

//creamos conexión bbdd
let pool;

async function getConnection() {
  if (!pool) {
    pool = mysql.createPool({
      connectionLimit: 10,
      host: MYSQL_HOST,
      user: MYSQL_USER,
      password: MYSQL_PASSWORD,
      database: MYSQL_DATABASE,
      timezone: "Z",
    });
  }
  return await pool.getConnection();
}

//exportamos la función de conexión
module.exports = {
  getConnection,
};
