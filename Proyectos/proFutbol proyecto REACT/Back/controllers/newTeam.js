const { getConnection } = require("../db");
const { generateError, processAndSaveImage, showDebug } = require("../helpers");

async function newTeam(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const {
      namecompetition,
      name,
      yearfundation,
      yearsinleague,
      historypoints,
      historyposition,
      address,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      mundialclub,
      others_title,
    } = req.body;
    console.log("0", req.body);
    const [result] = await connection.query(
      `
        INSERT INTO teams(namecompetition, name, yearfundation, yearsinleague, address, historyposition,  historypoints, league, cup, supercup, championsleague, europaligue, europasupercup, mundialclub, others_title, update_date )
        VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP)
       `,
      [
        namecompetition,
        name,
        yearfundation,
        yearsinleague,
        address,
        historypoints,
        historyposition,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        mundialclub,
        others_title,
      ]
    );
    //insertamos foto escudo
    const images = [];
    let index = 1;

    if (req.files && Object.keys(req.files).length > 0) {
      for (const [image, imageData] of Object.entries(req.files).slice(0, 3))
        try {
          showDebug(image);

          const processImage = await processAndSaveImage(imageData);
          images.push(processImage);
          console.log("2", processImage);
          // realizamos la query
          await connection.query(
            `
            UPDATE teams SET image${index}=? WHERE id=?
        `,
            [processImage, result.insertId]
          );
          index++;
        } catch (error) {
          throw generateError(
            "Opsss. Vaya hemos tenido un problema, No se ha podido procesar la imagen. Inténtalo de nuevo",
            400
          );
        }
    }

    res.send({
      status: "ok",
      data: {
        namecompetition,
        name,
        yearfundation,
        yearsinleague,
        address,
        historypoints,
        historyposition,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        mundialclub,
        others_title,
      },
    });
    console.log("resultado", res.data);
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = newTeam;
