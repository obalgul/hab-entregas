const { getConnection } = require("../db");
const { generateError } = require("../helpers");

async function stadium(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { id } = req.params;
    console.log("1", req.params);
    const [data] = await connection.query(
      `
            SELECT 
            *
            FROM stadiums
            WHERE id= ?
            `,
      [id]
    );
    console.log("2", data);
    if (data.lenght === 0) {
      throw generateError(
        `Opps, El equipo seleccionado con id ${id} no existe. Compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        400
      );
    }

    res.send({
      status: "ok",
      data: {
        data,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = stadium;
