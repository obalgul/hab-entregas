const { getConnection} = require("../db");
const { generateError } = require("../helpers");

async function singlePlayer(req, res, next){
    let connection;
    try {
        connection  = await getConnection();

        const {id} = req.params;
        

        const [data] =await connection.query(
            `
            SELECT *
            FROM players
            WHERE id = ?
            `, 
            [id]
            );

            if (data.length === 0) {
                throw generateError(
                  `Upsss, el jugador seleccionado con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
                  404
                );
              }    

            res.send({
                status: "ok",
                data: {
                    data,
                }
            })

    } catch (error) {
       next(error)
    } finally {
        if (connection) connection.release();
    }
}

module.exports = singlePlayer;