const { getConnection } = require("../db");
const { generateError, processAndSaveImage, showDebug } = require("../helpers");

async function newStadium(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { name, yearinagurate, capacity, address, id_team } = req.body;

    const [result] = await connection.query(
      `
            INSERT INTO stadiums(name, yearinagurate, capacity, address, update_date, id_team )
            VALUES(?, ?, ?, ?, UTC_TIMESTAMP, ?)
            `,
      [name, yearinagurate, capacity, address, id_team]
    );

    //insertamos foto estadio
    const images = [];
    let index = 1;
    if (req.files && Object.keys(req.files).length > 0) {
      for (const [imageName, imageData] of Object.entries(req.files).slice(
        0,
        2
      ))
        try {
          showDebug(imageName);

          const processImage = await processAndSaveImage(imageData);
          images.push(processImage);

          // realizamos la query
          await connection.query(
            `
            UPDATE stadiums
            SET image${index}=? 
            WHERE id=?
        `,
            [processImage, result.insertId]
          );
          index++;
        } catch (error) {
          throw generateError(
            "Opsss. Vaya hemos tenido un problema, No se ha podido procesar la imagen. Inténtalo de nuevo",
            400
          );
        }
    }

    res.send({
      status: "ok",
      data: {
        id: result.insertId,
        name,
        yearinagurate,
        capacity,
        address,
        id_team,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = newStadium;
