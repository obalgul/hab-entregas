const { getConnection } = require("../db");
const { generateError } = require("../helpers");

async function deleteTeam(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const { id } = req.params;

    const [current] = await connection.query(
      `
        SELECT id FROM teams
        WHERE id=?
        `,
      [id]
    );

    if (current.length === 0) {
      throw generateError(
        `Upsss, el club seleccionado con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    }

    await connection.query(
      `
        DELETE FROM teams
        WHERE id=?
        `,
      [id]
    );

    res.send({
      status: "ok",
      message: `El club con id:${id} ha sido borrado correctamente`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = deleteTeam;
