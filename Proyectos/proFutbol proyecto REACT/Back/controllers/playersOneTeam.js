const { getConnection } = require("../db");
const { generateError } = require("../helpers");

async function playersOneTeam(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { id } = req.params;
    console.log(req.params);
    const [data] = await connection.query(
      `
            SELECT *
            FROM players
            WHERE id_team=?
            `,
      [id]
    );

    if (data.length === 0) {
      throw generateError(
        `Upsss, los jugadores seleccionados con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    }
    console.log(data);
    res.send({
      status: "ok",
      data: {
        data,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = playersOneTeam;
