const { getConnection } = require("../db");
const { generateError, processAndSaveImage, showDebug } = require("../helpers");

async function editStadium(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    //sacamos los datos
    const { name, yearinagurate, capacity, address, id_team } = req.body;

    const { id } = req.params;

    //seleccionamos los datos de la entrada
    const [current] = await connection.query(
      `
        SELECT
        id,
        name,
        yearinagurate,
        capacity,
        address,
        id_team
        FROM stadiums
        WHERE id=?
        `,
      [id]
    );

    if (current.length === 0) {
      throw generateError(
        `Upsss, el equipo seleccionado con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    }

    //ejecutamos la query de edición de artículo
    await connection.query(
      `
      UPDATE stadiums
      SET name=?,
      yearinagurate=?,
      capacity=?,
      address=?,
      update_date=UTC_TIMESTAMP
      WHERE id_team=?
      `,
      [name, yearinagurate, capacity, address, id_team]
    );

    const images = [];
    let index = 1;
    if (req.files && Object.keys(req.files).length > 0) {
      for (const [imageName, imageData] of Object.entries(req.files).slice(
        0,
        2
      ))
        try {
          showDebug(imageName);

          const processImage = await processAndSaveImage(imageData);
          images.push(processImage);

          // realizamos la query
          await connection.query(
            `
            UPDATE stadiums
            SET image${index}=? 
            WHERE id=?
        `,
            [processImage, id]
          );

          index++;
        } catch (error) {
          throw generateError(
            "Opsss. Vaya hemos tenido un problema, No se ha podido procesar la imagen. Inténtalo de nuevo",
            400
          );
        }
    }
    //devolvemos el resultado
    res.send({
      status: "ok",
      data: {
        id,
        name,
        yearinagurate,
        capacity,
        address,
        id_team,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = editStadium;
