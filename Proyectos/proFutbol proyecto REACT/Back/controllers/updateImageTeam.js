const { getConnection } = require("../db");
const { processAndSaveImage, generateError } = require("../helpers");

async function updateImageTeam(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { id } = req.params;
    const { slot } = req.body;

    const [current] = await connection.query(
      `
                SELECT id
                FROM clubs
                WHERE id=?
            `,
      [id]
    );
    if (current.length === 0) {
      throw generateError(
        `Upsss, el equipo seleccionado con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    }

    if (req.files && req.files.image) {
      try {
        const processedImage = await processAndSaveImage(req.files.image);

        await connection.query(
          `
          UPDATE teams SET image${slot}=?
          WHERE id=?
        
          `,
          [processedImage, id]
        );
      } catch (error) {
        console.error(error);
        throw generateError(
          "Ooooh, vaya. No hemos podido procesar la imagen. Inténtalo de nuevo",
          400
        );
      }
    } else {
      throw generateError("Upss, la imagen no se ha subido", 400);
    }

    res.send({
      status: "ok",
      message: "Imagen subida",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = updateImageTeam;
