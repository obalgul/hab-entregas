const { getConnection } = require("../db");
const { generateError, processAndSaveImage, showDebug } = require("../helpers");

async function editTeam(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    //sacamos los datos
    const {
      namecompetition,
      name,
      yearfundation,
      yearsinleague,
      historypoints,
      historyposition,
      address,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      mundialclub,
    } = req.body;

    const { id } = req.params;

    //seleccionamos los datos de la entrada
    const [current] = await connection.query(
      `
      SELECT id,
      namecompetition,
      name,
      yearfundation,
      yearsinleague,
      historypoints,
      historyposition,
      address,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      mundialclub
      FROM teams
      WHERE id=?
      `,
      [id]
    );

    if (current.length === 0) {
      throw generateError(
        `Upsss, el equipo seleccionado con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    }

    //ejecutamos la query de edición de artículo
    await connection.query(
      `
      UPDATE teams
      SET namecompetition=?,
      name=?,
      yearfundation=?,
      yearsinleague=?,
      historypoints=?,
      historyposition=?,
      address=?,
      league=?,
      cup=?,
      supercup=?,
      championsleague=?,
      europaligue=?,
      europasupercup=?,
      mundialclub=?,
      update_date=UTC_TIMESTAMP
      WHERE id=?
      `,
      [
        namecompetition,
        name,
        yearfundation,
        yearsinleague,
        historypoints,
        historyposition,
        address,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        mundialclub,
        id,
      ]
    );

    const images = [];
    let index = 1;
    if (req.files && Object.keys(req.files).length > 0) {
      for (const [imageName, imageData] of Object.entries(req.files).slice(
        0,
        2
      ))
        try {
          showDebug(imageName);

          const processImage = await processAndSaveImage(imageData);
          images.push(processImage);

          // realizamos la query
          await connection.query(
            `
            UPDATE teams
            SET image${index}=? 
            WHERE id=?
        `,
            [processImage, id]
          );

          index++;
        } catch (error) {
          throw generateError(
            "Opsss. Vaya hemos tenido un problema, No se ha podido procesar la imagen. Inténtalo de nuevo",
            400
          );
        }
    }
    //devolvemos el resultado
    res.send({
      status: "ok",
      data: {
        id,
        namecompetition,
        name,
        yearfundation,
        yearsinleague,
        historypoints,
        historyposition,
        address,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        mundialclub,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = editTeam;
