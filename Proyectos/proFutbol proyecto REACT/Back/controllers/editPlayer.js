const { getConnection } = require("../db");
const { generateError, processAndSaveImage, showDebug } = require("../helpers");

async function editPlayer(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    //sacamos los datos
    const {
      club,
      name,
      lastname,
      nickname,
      nationality,
      numbermachclub,
      selectteam,
      goalsinclub,
      asistenceteam,
      start,
      localityborn,
      date_briday,
      number,
      position,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      mundialclub,
      americacup,
      eurocup,
      liguenations,
      mundial,
      others_title,
      id_team,
    } = req.body;
    console.log("datos", req.body);
    const { id } = req.params;
    console.log("este es el id:", id);
    //seleccionamos los datos de la entrada
    const [current] = await connection.query(
      `
      SELECT 
      id,
      club,
      name,
      lastname,
      nickname,
      nationality,
      numbermachclub,
      selectteam,
      goalsinclub,
      asistenceteam,
      start,
      localityborn,
      date_briday,
      number,
      position,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      mundialclub,
      americacup,
      eurocup,
      liguenations,
      mundial,
      others_title,
      id_team
      FROM players
      WHERE id=?
      `,
      [id]
    );

    if (current.length === 0) {
      throw generateError(
        `Upsss, el jugador seleccionado con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    }

    //ejecutamos la query de edición de artículo
    await connection.query(
      `
      UPDATE players
      SET club=?,
      name=?,
      lastname=?,
      nickname=?,
      nationality=?,
      numbermachclub=?,
      selectteam=?,
      goalsinclub=?,
      asistenceteam=?,
      start=?,
      localityborn=?,
      date_briday=?,
      number=?,
      position=?,
      league=?,
      cup=?,
      supercup=?,
      championsleague=?,
      europaligue=?,
      europasupercup=?,
      mundialclub=?,
      americacup=?,
      eurocup=?,
      liguenations=?,
      mundial=?,
      others_title=?,
      id_team=?,
      update_date=UTC_TIMESTAMP
      WHERE id=?
      `,
      [
        club,
        name,
        lastname,
        nickname,
        nationality,
        numbermachclub,
        selectteam,
        goalsinclub,
        asistenceteam,
        start,
        localityborn,
        date_briday,
        number,
        position,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        mundialclub,
        americacup,
        eurocup,
        liguenations,
        mundial,
        others_title,
        id_team,
        id,
      ]
    );

    const images = [];
    let index = 1;
    if (req.files && Object.keys(req.files).length > 0) {
      console.log("22222", req.files);
      for (const [imageName, imageData] of Object.entries(req.files).slice(
        0,
        2
      ))
        try {
          showDebug(imageName);

          const processImage = await processAndSaveImage(imageData);
          images.push(processImage);

          // realizamos la query
          const [data1] = await connection.query(
            `
            UPDATE players
            SET image${index}=? 
            WHERE id=?
        `,
            [processImage, id]
          );
          console.log("sql", data1);
          index++;
        } catch (error) {
          throw generateError(
            "Opsss. Vaya hemos tenido un problema, No se ha podido procesar la imagen. Inténtalo de nuevo",
            400
          );
        }
    }

    //devolvemos el resultado
    res.send({
      status: "ok",
      data: {
        id,
        club,
        name,
        lastname,
        nickname,
        nationality,
        numbermachclub,
        selectteam,
        goalsinclub,
        asistenceteam,
        start,
        localityborn,
        date_briday,
        number,
        position,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        mundialclub,
        americacup,
        eurocup,
        liguenations,
        mundial,
        others_title,
        id_team,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = editPlayer;
