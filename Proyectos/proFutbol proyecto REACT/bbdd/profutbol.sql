USE profutbol;

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE IF NOT EXISTS teams
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
namecompetition VARCHAR (255),
image1 VARCHAR (255),
name VARCHAR (255),
yearfundation VARCHAR (255),
yearsinleague VARCHAR (255),
address VARCHAR (255),
historypoints VARCHAR (255),
historyposition VARCHAR (255),
league VARCHAR (255),
cup VARCHAR (255),
supercup VARCHAR (255),
championsleague VARCHAR (255),
europaligue VARCHAR (255),
europasupercup VARCHAR (255),
mundialclub VARCHAR (255),
others_title TEXT,
update_date DATETIME NOT NULL
);

CREATE TABLE IF NOT EXISTS players
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
image1 VARCHAR (255),
club VARCHAR (255),
name VARCHAR (255),
lastname VARCHAR (255),
nickname VARCHAR (255),
numbermachclub VARCHAR (255),
selectteam VARCHAR (255),
goalsinclub VARCHAR (255),
asistenceteam VARCHAR (255),
start VARCHAR (255),
localityborn VARCHAR (255),
nationality VARCHAR (255),
date_briday DATETIME NULL,
number VARCHAR (255),
position VARCHAR (255),
league VARCHAR (255),
cup VARCHAR (255),
supercup VARCHAR (255),
championsleague VARCHAR (255),
europaligue VARCHAR (255),
europasupercup VARCHAR (255),
eurocup VARCHAR (255),
mundial VARCHAR (255),
liguenations VARCHAR (255),
americacup VARCHAR (255),
mundialclub VARCHAR (255),
others_title TEXT,
update_date DATETIME,
id_team INT UNSIGNED NOT NULL,
FOREIGN KEY (id_team) REFERENCES teams (id)
);

CREATE TABLE IF NOT EXISTS stadiums
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
name VARCHAR (255),
image1 VARCHAR (255),
yearinagurate VARCHAR (255),
capacity VARCHAR (255),
address VARCHAR (255),
update_date DATETIME,
id_team INT UNSIGNED NOT NULL,
FOREIGN KEY (id_team) REFERENCES teams (id)
);

SET FOREIGN_KEY_CHECKS=1;