"use strict";

let params = process.argv.slice(2);

let num1 = parseFloat(params[0]);
let num2 = parseFloat(params[1]);

let plantilla = `
                El resultado de la suma es: ${num1 + num2}
                El resultado de la resta es: ${num1 - num2}
                El resultado de la multiplicación es: ${num1 * num2}
                El resultado de la división es: ${num1 / num2}
`;

console.log(plantilla);
