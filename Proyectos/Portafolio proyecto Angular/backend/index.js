"use strict";

//conexión a la bbdd
let mongoose = require("mongoose");
let app = require("./app");
let port = 3700;

mongoose.Promise = global.Promise;
mongoose
  .connect("mongodb://localhost:27017/portafolio")
  .then(() => {
    console.log("Conexión a la bbdd esta corriendo... ;-)");

    //SERVIDOR
    app.listen(port, () => {
      console.log("El SERVIDOR esta funcionando... :-)");
    });
  })
  .catch((err) => console.log(err));
