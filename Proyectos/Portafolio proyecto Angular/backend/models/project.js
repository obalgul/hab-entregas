"use strict";

let mongoose = require("mongoose");
let Schema = mongoose.Schema;

//esquema de proyectos
let ProjectSchema = Schema({
  name: String,
  description: String,
  category: String,
  langs: String,
  year: Number,
  image: String,
});

module.exports = mongoose.model("Project", ProjectSchema);
//projects --> guarda los documents en la colección
