const { getConnection } = require("../db");
const { generateError, processAndSaveImage, showDebug } = require("../helpers");

async function newPlayer(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const {
      club,
      name,
      lastname,
      numbermachclub,
      numbermachcarrier,
      goalsinclub,
      asistenceteam,
      start,
      localityborn,
      date_briday,
      number,
      position,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      eurocup,
      mundial,
      liguenations,
      americacup,
      mundialclub,
      nationality,
    } = req.body;

    const [result] = await connection.query(
      `
            INSERT INTO players(club,
      name,
      lastname,
      numbermachclub,
      numbermachcarrier,
      goalsinclub,
      asistenceteam,
      start,
      localityborn,
      date_briday,
      number,
      position,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      eurocup,
      mundial,
      liguenations,
      americacup,
      mundialclub,
      nationality, update_date )
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,UTC_TIMESTAMP)
            `,
      [
        club,
        name,
        lastname,
        numbermachclub,
        numbermachcarrier,
        goalsinclub,
        asistenceteam,
        start,
        localityborn,
        date_briday,
        number,
        position,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        eurocup,
        mundial,
        liguenations,
        americacup,
        mundialclub,
        nationality,
      ]
    );

    //insertamos foto escudo y el estadio
    const images = [];
    let index = 1;
    if (req.files && Object.keys(req.files).length > 0) {
      for (const [imageName, imageData] of Object.entries(req.files).slice(
        0,
        2
      ))
        try {
          showDebug(imageName);

          const processImage = await processAndSaveImage(imageData);
          images.push(processImage);

          // realizamos la query
          await connection.query(
            `
            UPDATE players SET image${index}=? WHERE id=?
        `,
            [processImage, result.insertId]
          );
          index++;
        } catch (error) {
          throw generateError(
            "Opsss. Vaya hemos tenido un problema, No se ha podido procesar la imagen. Inténtalo de nuevo",
            400
          );
        }
    }
    res.send({
      status: "ok",
      data: {
        id: result.insertId,
        club,
        name,
        lastname,
        numbermachclub,
        numbermachcarrier,
        goalsinclub,
        asistenceteam,
        start,
        localityborn,
        date_briday,
        number,
        position,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        eurocup,
        mundial,
        liguenations,
        americacup,
        mundialclub,
        nationality,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = newPlayer;
