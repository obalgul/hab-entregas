const { getConnection } = require("../db");
const { generateError } = require("../helpers");

async function editPlayer(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    //sacamos los datos
    const {
      club,
      name,
      lastname,
      nationality,
      numbermachclub,
      numbermachcarrier,
      goalsinclub,
      asistenceteam,
      start,
      localityborn,
      date_briday,
      number,
      position,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      mundialclub,
      americacup,
      eurocup,
      liguenations,
      mundial,
    } = req.body;

    const { id } = req.params;

    //seleccionamos los datos de la entrada
    const [current] = await connection.query(
      `
        SELECT id,
      club,
      name,
      lastname,
      nationality,
      numbermachclub,
      numbermachcarrier,
        goalsinclub,
        asistenceteam,
        start,
        localityborn,
        date_briday,
        number,
        position,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
        europasupercup,
      mundialclub,
        americacup,
        eurocup,
        liguenations,
        mundial
        FROM players
        WHERE id=?
        `,
      [id]
    );

    if (current.length === 0) {
      throw generateError(
        `Upsss, el jugador seleccionado con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    }

    //ejecutamos la query de edición de artículo
    await connection.query(
      `
      UPDATE players
      SET club=?,
      name=?,
      lastname=?,
      nationality=?,
      numbermachclub=?,
      numbermachcarrier=?,
        goalsinclub=?,
        asistenceteam=?,
        start=?,
        localityborn=?,
        date_briday=?,
        number=?,
        position=?,
      league=?,
      cup=?,
      supercup=?,
      championsleague=?,
      europaligue=?,
        europasupercup=?,
      mundialclub=?,
        americacup=?,
        eurocup=?,
        liguenations=?,
        mundial=?,
      update_date=UTC_TIMESTAMP
      WHERE id=?
      `,
      [
        club,
        name,
        lastname,
        nationality,
        numbermachclub,
        numbermachcarrier,
        goalsinclub,
        asistenceteam,
        start,
        localityborn,
        date_briday,
        number,
        position,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        mundialclub,
        americacup,
        eurocup,
        liguenations,
        mundial,
        id,
      ]
    );

    //devolvemos el resultado
    res.send({
      status: "ok",
      data: {
        id,
        club,
        name,
        lastname,
        nationality,
        numbermachclub,
        numbermachcarrier,
        goalsinclub,
        asistenceteam,
        start,
        localityborn,
        date_briday,
        number,
        position,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        mundialclub,
        americacup,
        eurocup,
        liguenations,
        mundial,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = editPlayer;
