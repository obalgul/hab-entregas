const { getConnection } = require("../db");
const { generateError, processAndSaveImage, showDebug } = require("../helpers");

async function newteam(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const {
      namecompetition,
      club,
      yearfundation,
      yearsinleague,
      namestadio,
      yearinaguratestadio,
      address,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      americacup,
      recup,
      mundialclub,
    } = req.body;

    const [result] = await connection.query(
      `
            INSERT INTO clubs(namecompetition, club, yearfundation, yearsinleague, namestadio, yearinaguratestadio, address, league, cup, supercup, championsleague, europaligue, europasupercup, americacup, recup, mundialclub, update_date )
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP)
            `,
      [
        namecompetition,
        club,
        yearfundation,
        yearsinleague,
        namestadio,
        yearinaguratestadio,
        address,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        americacup,
        recup,
        mundialclub,
      ]
    );

    //insertamos foto escudo y el estadio
    const images = [];
    let index = 1;

    if (req.files && Object.keys(req.files).length > 0) {
      for (const [imageName, imageData] of Object.entries(req.files).slice(
        0,
        3
      ))
        try {
          showDebug(imageName);

          const processImage = await processAndSaveImage(imageData);
          images.push(processImage);

          // realizamos la query
          await connection.query(
            `
            UPDATE clubs SET image${index}=? WHERE id=?
        `,
            [processImage, result.insertId]
          );
          index++;
        } catch (error) {
          throw generateError(
            "Opsss. Vaya hemos tenido un problema, No se ha podido procesar la imagen. Inténtalo de nuevo",
            400
          );
        }
    }

    res.send({
      status: "ok",
      data: {
        id: result.insertId,
        namecompetition,
        club,
        yearfundation,
        namestadio,
        yearinaguratestadio,
        yearsinleague,
        address,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        americacup,
        recup,
        mundialclub,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = newteam;
