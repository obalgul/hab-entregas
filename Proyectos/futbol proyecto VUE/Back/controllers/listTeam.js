const { getConnection } = require("../db");

async function listTeam(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const { club, namestadio } = req.query;

    let sql = `SELECT *
            FROM  clubs
            `;

    let count = 0;
    let parameters = [];

    if (club) {
      sql += `WHERE club = '${club}'`;
      parameters.push(club);
      count++;
    }

    if (namestadio) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `namestadio = '${namestadio}'`;
      parameters.push(namestadio);
      count++;
    }

    let queryResults;
    queryResults = await connection.query(sql, [club, namestadio]);
    const [result] = queryResults;
    console.log([result]);
    res.send({
      status: "ok",
      data: result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = listTeam;
