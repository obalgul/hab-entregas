const { getConnection } = require("../db");
const { generateError } = require("../helpers");

async function editTeam(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    //sacamos los datos
    const {
      namecompetition,
      club,
      yearfundation,
      yearsinleague,
      namestadio,
      yearinaguratestadio,
      address,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      recup,
      mundialclub,
    } = req.body;
    console.log("1", req.body);
    const { id } = req.params;
    console.log("2", req.params);
    //seleccionamos los datos de la entrada
    const [current] = await connection.query(
      `
        SELECT id,
        namecompetition,
      club,
      yearfundation,
      yearsinleague,
      namestadio,
      yearinaguratestadio,
      address,
      league,
      cup,
      supercup,
      championsleague,
      europaligue,
      europasupercup,
      recup,
      mundialclub
        FROM clubs
        WHERE id=?
        `,
      [id]
    );

    if (current.length === 0) {
      throw generateError(
        `Upsss, el equipo seleccionado con id:${id} no existe, compueba que estas poniendo una entrada correcta y vuelve a inténtalo.`,
        404
      );
    }

    console.log("3", current);

    //ejecutamos la query de edición de artículo
    await connection.query(
      `
      UPDATE clubs
      SET namecompetition=?,
      club=?,
      yearfundation=?,
      yearsinleague=?,
      namestadio=?,
      yearinaguratestadio=?,
      address=?,
      league=?,
      cup=?,
      supercup=?,
      championsleague=?,
      europaligue=?,
      europasupercup=?,
      recup=?,
      mundialclub=?,
      update_date=UTC_TIMESTAMP
      WHERE id=?
      `,
      [
        namecompetition,
        club,
        yearfundation,
        yearsinleague,
        namestadio,
        yearinaguratestadio,
        address,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        recup,
        mundialclub,
        id,
      ]
    );

    //devolvemos el resultado
    res.send({
      status: "ok",
      data: {
        id,
        namecompetition,
        club,
        yearfundation,
        yearsinleague,
        namestadio,
        yearinaguratestadio,
        address,
        league,
        cup,
        supercup,
        championsleague,
        europaligue,
        europasupercup,
        recup,
        mundialclub,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = editTeam;
