const { getConnection } = require("../db");

async function listPlayer(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const {
      club,
      name,
      lastname,
      nationality,
      localitatyborn,
      number,
      position,
    } = req.query;

    let sql = `SELECT * 
            FROM  players
            `;

    let count = 0;
    let parameters = [];

    if (club) {
      sql += `WHERE club = '${club}'`;
      parameters.push(club);
      count++;
    }
    if (name) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `name = '${name}'`;
      parameters.push(name);
      count++;
    }
    if (lastname) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `lastname = '${lastname}'`;
      parameters.push(lastname);
      count++;
    }
    if (nationality) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `nationality = '${nationality}'`;
      parameters.push(nationality);
      count++;
    }
    if (localitatyborn) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `localitatyborn = '${localitatyborn}'`;
      parameters.push(localitatyborn);
      count++;
    }
    if (number) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `number = '${number}'`;
      parameters.push(number);
      count++;
    }
    if (position) {
      if (count > 0) {
        sql += " AND ";
      } else {
        sql += " WHERE ";
      }
      sql += `position = '${position}'`;
      parameters.push(position);
      count++;
    }
    let queryResults;
    queryResults = await connection.query(sql, [
      club,
      name,
      lastname,
      nationality,
      localitatyborn,
      number,
      position,
    ]);
    const [result] = queryResults;

    res.send({
      status: "ok",
      data: result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = listPlayer;
