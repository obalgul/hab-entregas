require("dotenv").config();

const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const cors = require("cors");

//controllers
const newTeam = require("./controllers/newTeam");
const newPlayer = require("./controllers/newPlayer");
const listTeam = require("./controllers/listTeam");
const listPlayer = require("./controllers/listPlayer");
const editTeam = require("./controllers/editTeam");
const editPlayer = require("./controllers/editPlayer");
const deleteTeam = require("./controllers/deleteTeam");
const deletePlayer = require("./controllers/deletePlayer");
const updateImageTeam = require("./controllers/updateImageTeam");
const updateImagePlayer = require("./controllers/updateImagePlayer");

const app = express();

//middlewares iniciales

//log petición consola
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

//procesado del body tipo JSON
app.use(bodyParser.json());

//Procesado body tipo FORM-DATA
app.use(fileUpload());

app.use(cors());

app.use(express.static("static"));

//endpoints

//ruta almacén fotos
//GET - /teams
//GET - /players
app.get("/listTeam", listTeam);
//app.get("/listPlayer", listPlayer)

//crear un nuevo equipo
//POST -/teams
app.post("/teams", newTeam);

//crear un nuevo equipo
//POST -/teams
app.post("/players", newPlayer);

//lisar clubs
//GET - /listTeam
app.get("/listTeam", listTeam);

//lisar jugadores
//GET - /listPlayers
app.get("/listPlayer", listPlayer);

//Editar datos de un equipo
//PUT /teams/:id
app.put("/editTeam/:id", editTeam);

//Editar datos de un jugador
//PUT /editPlayer/:id
app.put("/editPlayer/:id", editPlayer);

//Editar una imagen de equipo
//POST /team/image/:id
app.post("/team/:id/images", updateImageTeam);

//Editar una imagen de equipo
//POST /player/image/:id
app.post("/player/:id/images", updateImagePlayer);

//Borrar un club
//DELETE /editTeam/:id
app.delete("/team/:id", deleteTeam);

//Borrar un club
//DELETE /players/:id
app.delete("/player/:id", deletePlayer);

//Middlewares finales

//Error middleware
app.use((error, req, res, next) => {
  console.error(error);

  res.status(error.httpStatus || 500).send({
    status: "error",
    message: error.message,
  });
});

//not found
app.use((req, res) => {
  res.status(404).send({
    status: "error",
    message: "Not found",
  });
});

//puerto conexión
const port = process.env.PORT;

//ejecutamos express para escuhar cambios y mensaje qu eesta fen funcionamiento
app.listen(port, () => {
  console.log(`Api en funcinamiento en http://localhost:${port} 🙈🙈🙈`);
});
