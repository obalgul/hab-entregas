import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/about",
    name: "About",

    component: () => import("../views/About.vue"),
  },
  {
    path: "/ListTeam",
    name: "ListTeam",

    component: () => import("../views/ListTeam.vue"),
  },
  {
    path: "/ListPlayer",
    name: "ListPlayer",

    component: () => import("../views/ListPlayer.vue"),
  },
  {
    path: "/NewTeam",
    name: "NewTeam",

    component: () => import("../views/NewTeam.vue"),
  },
  {
    path: "/NewPlayer",
    name: "NewPlayer",

    component: () => import("../views/NewPlayer.vue"),
  },
  {
    path: "/EditTeam",
    name: "EditTeam",

    component: () => import("../views/EditTeam.vue"),
  },
  {
    path: "/EditPlayer",
    name: "EditPlayer",

    component: () => import("../views/EditPlayer.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
