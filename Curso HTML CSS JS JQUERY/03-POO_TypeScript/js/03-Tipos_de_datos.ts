//Tipos de datos

//Si asignamos a una variable un valor que no sea el asignado,
//en la consola de tsc, nos devolvera un error indicando
//la línea y el tipo de error cometido. O sea, si a una variable 
//le damos un valor de string, no podremos cambiarlo a otro tipo de 
//variable.

//string (ver en la consola)
let cadena: string = "Esto es un string o cadena de texto";
console.log(cadena);
/****************************************/

//numero
let numero: number = 23;
console.log();
/****************************************/

//booleano
let verdadero_falso: boolean= true;
console.log(verdadero_falso);
/****************************************/

//Para poder utilizar varios tipos de variable en una se utiliza el
//simbolo | para añadir un nuevo valor a la variable. (| pasa a ser ó)
let palabra: string | number = "Con 'string | number' puedo ser una cadena";
console.log(palabra);
palabra = 25;
console.log(palabra, "O como ves ahora un número");
/****************************************/

//any (variable a la que se le puede dar cualquier valor)
//este tipo de variable puede cambiar de string a número
//o al reves. 
let cualquier: any ="soy una variable 'any'"
console.log(cualquier, "Es un string");
cualquier= 77
console.log(cualquier, "Ahora soy un número, pero sigo siendo una variable 'any'");
/****************************************/

//arrays, Podemos hacer que los arrays tengan tipado fuerte,
//diciendole que sus elementos son un tipo de variable
let letras: Array<string> = ['a', 'b', 'c']
console.log(letras);

let numeros: number[] = [1,2,3,4]
console.log(numeros);
/****************************************/

//arrays para cualquier tipo de variable.
let letras_numeros: any[]=['a', 'b', 'c', 1, 2, 3]
console.log(letras_numeros)
/****************************************/

//tipo de variables personalizadas con type (nombre de la variable que le demos) = a los tipos de variable
type alfanumerico = string | number;
let numerosLetras: alfanumerico = "Esto es una variable personalizada que acepta numeros y letras"
console.log(numerosLetras);
numerosLetras = 125;
console.log(numerosLetras, "ahora soy un número");
/****************************************/


