var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
//decoradores
function estampar(logo) {
    return function (target) {
        target.prototype.estampacion = function () {
            console.log("Camisetra estampada con el logo de:" + logo);
        };
    };
}
//Aplicamos el decorador
var Camiseta = /** @class */ (function () {
    //métodos constructor de la clase -> son las funciones o acciones que tiene el objeto
    function Camiseta(color, modelo, marca, talla, precio) {
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.talla = talla;
        this.precio = precio;
    }
    Camiseta.prototype.setColor = function (color) {
        this.color = color;
    };
    Camiseta.prototype.getColor = function () {
        return this.color;
    };
    Camiseta = __decorate([
        estampar('Kappa')
    ], Camiseta);
    return Camiseta;
}());
//let camiseta = new Camiseta("Rojo", "sin mangas", "Puma", "L", 30)
//console.log(camiseta);
//Herencia -> clase hija
var Sudadera = /** @class */ (function (_super) {
    __extends(Sudadera, _super);
    function Sudadera() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Sudadera.prototype.setCapucha = function (capucha) {
        this.capucha = capucha;
    };
    Sudadera.prototype.getCapucha = function () {
        return this.capucha;
    };
    return Sudadera;
}(Camiseta));
var camiseta = new Camiseta("Rojo", "sin mangas", "Puma", "L", 30);
console.log(camiseta);
//lamamos al metodo estampacion()
camiseta.estampacion();
//llamamos a la clase con las propiedades descritas
var sudadera_nike = new Sudadera("Amarilla", "manga larga", "Puma", "L", 40);
console.log(sudadera_nike);
//llamamos a la clase y cambiamos la propiedades capucha
console.log(sudadera_nike);
sudadera_nike.setCapucha(false);
//llamamos a la clase y cambiamos la propiedades color
sudadera_nike.setColor("Verde");
console.log(sudadera_nike);
