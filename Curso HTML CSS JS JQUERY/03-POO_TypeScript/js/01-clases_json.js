//clases orientada a objetos
let bicicleta = {
  color: "Rojo",
  modelo: "BMX",
  frenos: "De disco",
  velocidadMaxima: "160 Km/h",
  cambiaColor: function (nuevo_color) {
    //bicicleta.color = nuevo_color
    this.color = nuevo_color;
    console.log(this);
  },
};

bicicleta.cambiaColor("Azul");
