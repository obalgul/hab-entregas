//clases -> es como un módelo del objeto
/*
class Camiseta{
   
    //Propiedades de la clase -> son las caracteristicas que tiene el objeto
    //Cuando las propiedades son pribadas solo se pueden modificar desde
    //dentro de la clase. Para poderlos cambiar desde fura de la clase debe
    //crearse un método que lo permita. 
    private color: string;
    private modelo: string;
    private marca: string;
    private talla: string;
    private precio: number;
    
   //métodos constructor de la clase -> son las funciones o acciones que tiene el objeto
   constructor(color, modelo, marca, talla, precio){
       this.color =color;
       this.modelo = modelo;
       this.marca = marca;
       this.talla = talla;
       this.precio = precio;
   } 
   
   public setColor(color){
        this.color = color;
        }
    public getColor(){
        return this.color
        }

}

let camiseta = new Camiseta("Rojo","Manga Larga", "Nike", "XXL", 75);
camiseta.setColor("Rojo");
//camiseta.getColor()

let camiseta_futbol = new Camiseta("Verde", "Manga Corta", "Adidas", "XS", 45);
camiseta_futbol.setColor("Franjas Amarillas y Rojas")

console.log(camiseta, camiseta_futbol);
*/
//interfaces -> esto indica los campos obligatorios que tendrá la clase,
//si los métodos no se corresponden nos dará error.
interface CamisetaBase{
    setColor(color);
    getColor()
}

//decoradores
function estampar(logo:string){
    return function(target: Function){
        target.prototype.estampacion =function():void{
            console.log("Camisetra estampada con el logo de:"+ logo);
            
        }
    }
}

//Aplicamos el decorador
@estampar('Kappa')

class Camiseta implements Camiseta{
   
    //Propiedades de la clase -> son las caracteristicas que tiene el objeto
    //Cuando las propiedades son pribadas solo se pueden modificar desde
    //dentro de la clase. Para poderlos cambiar desde fura de la clase debe
    //crearse un método que lo permita. 
    private color: string;
    private modelo: string;
    private marca: string;
    private talla: string;
    private precio: number;
    
   //métodos constructor de la clase -> son las funciones o acciones que tiene el objeto
   constructor(color, modelo, marca, talla, precio){
       this.color =color;
       this.modelo = modelo;
       this.marca = marca;
       this.talla = talla;
       this.precio = precio;
   } 
   
   public setColor(color){
        this.color = color;
        }
    public getColor(){
        return this.color
        }

}

//let camiseta = new Camiseta("Rojo", "sin mangas", "Puma", "L", 30)
//console.log(camiseta);

//Herencia -> clase hija
class Sudadera extends Camiseta{
    public capucha: boolean;
    
    setCapucha(capucha:boolean){
        this.capucha = capucha;
    }
    getCapucha():boolean{
        return this.capucha;
    }
}

let camiseta = new Camiseta("Rojo", "sin mangas", "Puma", "L", 30)
console.log(camiseta);

//lamamos al metodo estampacion()
camiseta.estampacion();

//llamamos a la clase con las propiedades descritas
let sudadera_nike = new Sudadera("Amarilla", "manga larga", "Puma", "L", 40);
console.log(sudadera_nike);
//llamamos a la clase y cambiamos la propiedades capucha
console.log(sudadera_nike);
sudadera_nike.setCapucha(false)
//llamamos a la clase y cambiamos la propiedades color
sudadera_nike.setColor("Verde")
console.log(sudadera_nike);



