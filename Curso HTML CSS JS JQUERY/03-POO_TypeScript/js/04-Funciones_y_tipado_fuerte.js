//funciones en javascript
function ponNumero(numero) {
    return "el número es: " + numero;
}
console.log(ponNumero(15), "javascript");
//funciones en typeScript, si le cambiamos el tipo de variable,
//a número por string, nos devolvera un error.
function ponOtroNumero(numero) {
    if (numero === void 0) { numero = 15; }
    return "el número es: " + numero;
}
console.log(ponOtroNumero(15), "typescript");
