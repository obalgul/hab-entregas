//Typescript es un javascript de typado muy fuerte, que se utiliza para 
//que el código sea compatible para la mayoría de navegadores.
//typescript se traduce a javascript.
//Para trabajar con typescript, debemos instalarlo mediante npm. (ver en su página web)
//Con él, si no esta instalado, el nodejs. A continuación debemos
//añadir el script con la ruta del archivo en el index.html, 
//con extensión .ts, y para que los cambios se realicen en tiempo real, 
//ejecutar en la ruta del archivo .ts, en el terminal el comando  tsc -w *.ts
//Si este comando no esta en uso, no se realizaran los cambios en le código. 
console.log("Empezamos a probar el typescript");
