$(document).ready(function () {
  //LOAD
  /*
  $("#datos").load("https://reqres.in/");
  */

  //Get
  $.get("https://reqres.in/api/users", { page: 2 }, function (response) {
    response.data.forEach((user, index) => {
      $("#datos").append(
        "<p>" + user.first_name + " " + user.last_name + "</p>"
      );
    });
  });

  //post (Comprobar formulario en le ajax.html)
  $("#formulario").submit(function (event) {
    event.preventDefault(); //capturamos el evento
    const usuario = {
      name: $('input[name="name"]').val(), //capturamos los inputs
      email: $('input[name="email"]').val(),
    };
    console.log(usuario); //comprobamos los datos capturados
    /*
    $.post($(this).attr("action"), usuario, function (response) {
      console.log(response);
    }).done(() => {
      alert("Usuario creado correctamente."); //confirmamos que la petición se ha efectuado
    });

    return false;
  });
  */
    //métodos de petición ajax más comodo
    $.ajax({
      type: "POST",
      url: $(this).attr("action"), //ruta del formulario que esta en action
      data: usuario,
      beforeSend: function () {
        //ejecuta algo antes de el envio.
        console.log("Enviando datos..");
      },
      success: function (response) {
        //ejecuta algo cuando va todo bien.
        console.log(response);
      },
      error: function () {
        //ejecuta algo cuando se da un error.
        console.log("Algo ha ido mal...");
      },
      timeout: 2000,
    });
    return false;
  });
});
