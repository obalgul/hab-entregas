$(document).ready(() => {
  /*  
    //recorrer las etiquetas "a" (se añaden al textos.html una lista con su "ul" e "li")
    $("a").each(function (index) {
      enlace = $(this);
      let enlaces = enlace.attr("href");
      enlace.text(enlaces);
    });
    */

  //añadir enlaces utilizamos el .attr() (se añaden al textos.html un input,un botón
  //y un id="lista al ul"). Para añadir después de la lista append
  // pra hacerlo antes prepend. Con before y after, lo añaden antes y
  //despues de la lista, pero fuera de esta.
  links();

  $("#addButton").click(() => {
    $("#lista").prepend(
      '<li><a href="https://' + $("#addLink").val() + '"></a></li>'
    );
    //vaciar el input después de accionar el boton añadir
    $("#addLink").val("");
    links();
  });

  function links() {
    $("a").each(function (index) {
      enlace = $(this);
      let enlaces = enlace.attr("href");
      enlace.text(enlaces);

      enlace.attr("target", "_blank");
    });
  }

  //añadir enlaces utilizamos el .attr() para eliminar
  //atributos usamos removeattr()
  $("#addButton").removeAttr("disabled");
});
