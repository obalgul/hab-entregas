$(document).ready(function () {
  //Mover elementos dentro da la pantalla con draggable()
  $(".elemento").draggable();

  //Rediemsionar elementos .resizable() Para ello hay que cargar
  //los links necesarios en el archivo html, en este caso en
  //06-interfaces.html
  $(".elemento").resizable();

  //Lista con elementos seleccionables. Solo para listas. .selectable()
  //Hay que darle estilos a la lista en el html ul .ui-selecting cuando
  //estamos seleccionando y ul .ui-selected cuando se ha seleccionado
  //en este caso
  //$(".lista").selectable();

  //sortable() para ordenar elementos de una lista. No funciona con
  //con selectable(). Por eso está comentada la línea 14.
  //Este método puede guardar eventos de las posiciones mediante
  //una función de callback
  $(".lista").sortable({
    update: function (event, ui) {
      console.log("La lista ha cambiado");
    },
  });
  //droppable() efecto arrastrar y soltar. Para poder arrastrar el
  //elemento que queremos arrastrar debe tener el método draggable().
  //Este elemento tiene la opción de callback drop.
  $("#mover").draggable();
  $("#area").droppable({
    drop: function () {
      console.log("Has soltado el elemento");
    },
  });

  //efectos explosión. Se ejecuntan con un callback y el metodo
  //toggle() y dentro de el efecto deseado. Estos efectos tienen
  //opciones por ejemplo .toggle("EFECTO a realizar", {opciones, se ejecuta como un json}, tiempo en ms)
  /*
  $("#mostrar").click(function () {
    $("#elemento1").toggle("explode");
  });
  */
  //efectos cortina. Se ejecuntan con un callback y el metodo
  //toggle() y dentro de el efecto deseado.
  /*
  $("#mostrar").click(function () {
    $("#elemento1").toggle("blind");
  });
  */
  //efectos cortina. Abre lateralmente. Se ejecuntan con un callback y el metodo
  //toggle() y dentro de el efecto deseado.
  /*
  $("#mostrar").click(function () {
    $("#elemento1").toggle("slide"); //drop lo hace con un degradado.
  });
  */
  //efectos escala. Se ejecuntan con un callback y el metodo
  //toggle() y dentro de el efecto deseado.
  /*
  $("#mostrar").click(function () {
    $("#elemento1").toggle("scale");
  });
  */
  //efectos shake. Se ejecuntan con un callback y el metodo
  //toggle() y dentro de el efecto deseado.
  $("#mostrar").click(function () {
    $("#elemento1").toggle("shake", 3000);
  });

  //Tooltips efecto popup al pasar por un elemento.
  //$(document).toolpit();

  //cuadros de dialogo Mensaje personalizable
  $("#pulsar").click(function () {
    $("#popup").dialog();
  });

  //efecto calendario datepicker
  $("#calendario").datepicker();

  //Efecto Tabs, pestañas.
  $("#tabs").tabs();
});
