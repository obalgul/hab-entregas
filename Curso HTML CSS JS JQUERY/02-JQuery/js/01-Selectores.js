$(document).ready(function () {
  //selector id, se usa para un elemento(al ponerlo en una variable se puede reutilizar)
  let rojo = $("#rojo").css("background", "red").css("color", "blue");
  let amarillo = $("#amarillo").css("background", "yellow").css("color", "red");
  let verde = $("#verde").css("background", "green").css("color", "yellow");

  //selector class, se uas para muchos elementos
  let zebra = $(".zebra").css("border", "2px dashed grey");

  //selectores ejecutando eventos.
  //cuando pulsemos sobre los parrafos con clases sinBorde
  //estas cambiaran. Creamos una clase "zebra" en el indx.html
  //entre etiquetas "style" y la aplicamos en los parrafos
  let clase = $(".sinBorde").css("padding", "5px");
  $(".sinBorde").click(function () {
    $(this).addClass("zebra");
  });

  //selectores de etiqueta
  //creamos una clase "grande" en el indx.html entre etiquetas "style"
  //y la aplicamos en los parrafos

  let parrafos = $("p");

  parrafos.click(function () {
    let claseGrande = $(this);
    if (!claseGrande.hasClass("grande")) {
      claseGrande.addClass("grande");
    } else {
      claseGrande.removeClass("grande");
    }
  });

  //selectores por atributos. (Se seleccionan entre corchetes)
  $("[title='private']").css("background", "red");
  $("[title='sport']").css("background", "yellow");
  $("[title='google']").css("font-size", "40px");
  $("[title='yahoo']").css("color", "grey");

  //Otros selectores
  //creamos una clase "margenSuperior" en el indx.html entre etiquetas "style"
  //y la aplicamos a los parrafos "p" y enlaces "a"
  $("p, a").addClass("margenSuperior");

  //Buscar elementos con jQuery, podemos utilizar find(), para buscar clases
  //y parent para movernos por el DOM.
  let busqueda = $("#elemento2").parent().parent().find(".subrayado");

  console.log(busqueda);
});
