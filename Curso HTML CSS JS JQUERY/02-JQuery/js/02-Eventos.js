$(document).ready(function () {
  //eventos
  //mouseover y mouseout
  /*
  let caja = $("#caja");

  caja.mouseover(function () {
    caja.css("background", "yellow");
  });

  caja.mouseout(function () {
    caja.css("background", "blue");
  });
  */
  //hover
  /*
  let caja = $("#caja");
  function cambiaAmarillo() {
    caja.css("background", "yellow");
  }

  function cambiaAzul() {
    caja.css("background", "blue");
  }

  caja.hover(cambiaAmarillo, cambiaAzul);
  */
  //click y doble click
  /*
  let caja = $("#caja");
  caja.click(() => {
    caja.css("background", "rebeccapurple").css("color", "gold");
  });
  caja.dblclick(() => {
    caja.css("background", "magenta").css("color", "orange");
  });
  */
  //focus y blur
  /*
  let nombre = $("#nombre");
  nombre.focus(() => {
    nombre.css("border", "10px solid green");
  });
  nombre.blur(() => {
    nombre.css("border", "10px solid pink");

    $("#datos").text($(nombre).val()).show();
    */

  //mousedown, mouseup y mousemove
  let nombre = $("#nombre");
  let datos = $("#datos");
  nombre.focus(() => {
    nombre.css("border", "10px solid green");
  });
  nombre.blur(() => {
    nombre.css("border", "10px solid pink");

    datos.text($(nombre).val()).show();
  });
  //mousedown
  datos.mousedown(() => {
    datos.css("background", "rebeccapurple");
  });
  //mouseup
  datos.mouseup(() => {
    datos.css("background", "gold");
  });
  //mousemove
  $(document).mousemove(() => {
    console.log("En X: ", event.clientX);
    console.log("En Y: ", event.clientY);
  });
});
