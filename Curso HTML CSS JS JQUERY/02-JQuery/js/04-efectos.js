$(document).ready(() => {
  let caja = $("#caja");
  //metodos hide y show
  /*
  $("#mostrar").click(function () {
    caja.show();
  });
  $("#ocultar").click(function () {
    caja.hide();
  });
  */
  //ocultar y mostrar con un solo boton dentro de los () podemos
  //indicar la velocidad de desvanecimiento. Puedes usar fast o normal
  /*
  $("#mostrar").hide();

  $("#mostrar").click(function () {
    $(this).hide();
    $("#ocultar").show();
    caja.show("normal");
  });

  $("#ocultar").click(function () {
    $(this).hide();
    $("#mostrar").show();
    caja.hide("normal");
  });
  */
  //fundidos con fadeIn() y fadeOut(), puedes poner entre () slow, normal o fast
  /*
  $("#mostrar").hide();

  $("#mostrar").click(function () {
    $(this).hide();
    $("#ocultar").show();
    caja.fadeIn("slow");
  });

  $("#ocultar").click(function () {
    $(this).hide();
    $("#mostrar").show();
    caja.fadeOut("slow");
  });
  */
  //Opacidad fadeTo("slow", 0) hace el efecto de desvanecimiento entre 0 y 1
  /*
  $("#mostrar").hide();

  $("#mostrar").click(function () {
    $(this).hide();
    $("#ocultar").show();
    caja.fadeTo("slow", 1);
  });

  $("#ocultar").click(function () {
    $(this).hide();
    $("#mostrar").show();
    caja.fadeTo("slow", 0.2);
  });
  */
  //Opacidad slideUp() y slideDown() abre y cierra en horizontal
  $("#mostrar").hide();

  $("#mostrar").click(function () {
    $(this).hide();
    $("#ocultar").show();
    caja.slideDown("slow");
  });

  $("#ocultar").click(function () {
    $(this).hide();
    $("#mostrar").show();
    caja.slideUp("slow");
  });

  //efecto toggle() para minimizar el código. Puedes poner entre () slow, normal o fast
  /*
  $("#mostrarocultar").click(function () {
    caja.toggle("slow");
  });
  */
  //efecto fadeToggle(). Puedes poner entre () slow, normal o fast
  /*
  $("#mostrarocultar").click(function () {
    caja.fadeToggle("slow");
    */
  //efecto slideToggle(). Puedes poner entre () slow, normal o fast
  $("#mostrarocultar").click(function () {
    caja.slideToggle("slow");
  });

  //animaciones con el metodo animate()
  $("#animacion").click(function () {
    caja
      .animate(
        {
          marginLeft: "500px",
          fontSize: "2rem",
        },
        "slow"
      )
      .animate(
        {
          marginLeft: "0px",
          fontSize: "1rem",
        },
        "slow"
      )
      .animate(
        {
          borderRadius: "50px",
          fontSize: "2rem",
        },
        "slow"
      );
  });

  //Añadir callback a la animación
  $("#mostrarocultar").click(function () {
    caja.slideToggle("slow", function () {
      document.write("La animación se ha ocultado.");
    });
  });
});
