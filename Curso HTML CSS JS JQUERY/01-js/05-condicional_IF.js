"use strict";

//condicional IF
// si A es igual a B
/*
let edad1 = 90;
let edad2 = 60;

if (edad1 > edad2) {
  console.log("la edad2 es mayor que edad1");
} else {
  console.log("La edad1 es menor");
}

let edad3 = 85;

if (edad3 >= 18) {
  console.log("Es mayor de edad.");
  if (edad3 === 25) {
    console.log("Aun eres joven");
  } else if (edad3 >= 80) {
    console.log("Eres muy mayor");
  } else {
    console.log("eres adolescente");
  }
} else {
  console.log("Es menor de edad.");
}
*/

//operadores logicos

//negacion

let year = 2016;

if (year !== 2020) {
  console.log(" el año no es 2020. Es el año " + year);
}

//AND

if (year >= 2000 && year <= 2020 && year !== 2018) {
  console.log("Estamos en la era de la era");
} else {
  console.log("Vaya tela");
}

//OR

if (year === 2008 || year === 2018) {
  console.log("El año acaba en 8");
} else {
  console.log("El año no es 2008 ni 2018");
}
