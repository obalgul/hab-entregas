"use strict";

//altura de la ventana
console.log(window.innerHeight); //en uso
console.log(screen.height); //valor fijo

//ancho de la pantalla
console.log(window.innerWidth); //en uso
console.log(screen.width); //valor fijo

//url en uso
console.log(window.location); //datos generales
console.log(window.location.href); //url
