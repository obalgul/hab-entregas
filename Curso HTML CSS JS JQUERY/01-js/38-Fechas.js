"use strict";

//Fechas

const fecha = new Date();
const year = fecha.getFullYear();
const month = fecha.getMonth();
const day = fecha.getDate();
const hour = fecha.getHours();
const minutes = fecha.getMinutes();
const seconds = fecha.getSeconds();
const miliseconds = fecha.getMilliseconds();

const textoTiempo = `
 Estás en el año: ${year}
 En el mes: ${month + 1} 
 Y el día es: ${day}
 Ahora es la hora: ${hour} 
 y los minutos son: ${minutes} 
 y estos los segundos: ${seconds} 
 y los milisegundos son: ${miliseconds} 
`;

//El mes empieza a contar desde 0, (el +1 es una solución rápida)
console.log(fecha);
console.log(textoTiempo);
