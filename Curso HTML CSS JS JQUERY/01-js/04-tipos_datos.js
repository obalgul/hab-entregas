"use strict";

//operadores
let numero1 = 10;
let numero2 = 5;
let operacion0 = numero1 + numero2;
let operacion1 = numero1 - numero2;
let operacion2 = numero1 * numero2;
let operacion3 = numero1 / numero2;
let operacion4 = numero1 ** numero2;

console.log(
  "el resultado es",
  operacion0,
  operacion1,
  operacion2,
  operacion3,
  operacion4
);

//tipos de datos

let numero_entero = 5;
let cadena_texto = "Hola Pedrín";
let vedadero_o_falso = true;

//Para pasar una variable que es un número a un cadena de texto
//se utiliza de está forma ==> numero_entero.toString()

console.log(numero_entero, cadena_texto, vedadero_o_falso);

let numero_falso = "33";

console.log(Number(numero_falso + 7));
console.log(Number(numero_falso) + 7);

//typeof

console.log(typeof numero_entero);
console.log(typeof cadena_texto);
console.log(typeof vedadero_o_falso);
console.log(typeof numero_entero.toString());
