"use strict";

function cambiaColor(color) {
  caja.style.background = color;
}

// const caja = document.getElementById("micaja");

// seleccionar mediante querySelector
const caja = document.querySelector("#micaja");

caja.innerHTML = "Nuevo texto desde JS";
caja.style.background = "green";
caja.style.padding = "20px";
caja.className = "pong una clase";

console.log(caja);
