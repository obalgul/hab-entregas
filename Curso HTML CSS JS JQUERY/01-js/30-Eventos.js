"use strict";

//evento load
//ejecuta la línea de script del index al finalizar
//la carga de la páguina. Dentro de la función se
//ponemos los ejecuciones del JS

window.addEventListener("load", () => {
  //Eventos ratón

  function cambiarColor() {
    console.log("Has hecho click");

    let bg = boton.style.background;

    if (bg === "green") {
      boton.style.background = "blue";
    } else {
      boton.style.background = "green";
    }
    boton.style.padding = "20px";
    return true;
  }

  let boton = document.querySelector("#boton");

  //evento click
  boton.addEventListener("click", function () {
    cambiarColor();
  });

  //mouse hover
  boton.addEventListener("mouseover", function () {
    boton.style.background = "red";
  });

  //mouseout
  boton.addEventListener("mouseout", function () {
    boton.style.background = "black";
  });
  /***************************/

  const input = document.querySelector("#nombre");
  //focus
  input.addEventListener("focus", function () {
    console.log("[FOCUS] estas en el input");
  });

  //blur
  input.addEventListener("blur", function () {
    console.log("[BLUR] estas fuera del input");
  });
  //keydown
  input.addEventListener("keydown", function () {
    console.log("[KEYDOWN] has pulsado", String.fromCharCode(event.keyCode));
  });

  //keypress
  input.addEventListener("keypress", function () {
    console.log(
      "[KEYPRESS] has presionado",
      String.fromCharCode(event.keyCode)
    );
  });
  //keyup
  input.addEventListener("keyup", function () {
    console.log(
      "[KEYUP] has soltado la tecla",
      String.fromCharCode(event.keyCode)
    );
  });
});
