"use strict";

//notación de objetos javascript

const pelicula = {
  title: "Batman",
  year: 2017,
  country: "USA",
};

//cambiar valores del objeto

pelicula.year = 2020;
console.log("cambiamos el año", pelicula, pelicula.year);

//array de objetos

const peliculas = [
  { title: "Superman", year: 1979, country: "USA" },
  pelicula,
  { title: "SuperLópez", year: "2008", country: "España" },
];

console.log(peliculas);

//Itinerar el array de objeto

const listaPeliculas = document.querySelector("#peliculas");
let index;
for (index in peliculas) {
  let parrafo = document.createElement("p");
  parrafo.append(peliculas[index].title + " - " + peliculas[index].year);
  listaPeliculas.append(parrafo);
}
