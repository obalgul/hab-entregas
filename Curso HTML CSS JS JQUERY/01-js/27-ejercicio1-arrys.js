"use strict";

/*
1. Pide 6 números por pantalla y los meta en un array
2. Mostrar el array en consola y pantalla
3. Mostrar el array ordenado
4. Invertir el ordendel array
5. Mostrar cuantos elementos tiene
6. Buscar un elemento del array
*/
/*
//1. Pide 6 números por pantalla y los meta en un array
let numeros = new Array(6);

//2. Mostrar el array
//Primera manera de guardar los números en el array
for (let index = 0; index <= 5; index++) {
  numeros[index] = parseInt(prompt("Introduce tus números aleatorios"));
}
console.log(numeros);
*/

//2. Mostrar el array en consola
//Segunda manera de guardar los números en el array
let numeros = [];

for (let index = 0; index <= 5; index++) {
  numeros.push(parseInt(prompt("Introduce tus números aleatorios")));
}
console.log(numeros);

//2. Mostrar el array en pantalla

document.write("<h1>Contenido del array</h1>");

document.write("<ul>");
numeros.forEach((numero, index) => {
  document.write(`<li> ${numero} </li>`);
});
document.write("</ul>");
//3. Mostrar el array ordenado
document.write(`<strong> Array ordenado </strong>`);
document.write(
  numeros.sort((a, b) => {
    return a - b;
  })
);
console.log(numeros, "Array ordenado");

//3. Mostrar el array ordenado con una función
function mostrarArray(elementos, texto = "") {
  document.write(`<h1>Contenido del array ${texto}</h1>`);
  document.write("<ul>");
  elementos.forEach((elemento, index) => {
    document.write(`<li> ${elemento}</li>`);
  });
  document.write("</ul>");
}
//llamamos a la función para mostrarlo ordenados
numeros.sort((a, b) => {
  return a - b;
});
//mostrarArray(numeros, "Array ordenado");

//4. Invertir el ordendel array
mostrarArray(numeros.reverse(), "Array invertido; mayor a menor");

//5. Mostrar cuantos elementos tiene.
document.write(
  "longuitud del array o cantiadad de elementos es ",
  numeros.length
);

//6. Buscar la posición de un elemento del array
//Es la posición del array original. El primero que aparece en
//la consola.
let buscaNumero = parseInt(prompt("Busca un número del array"));

let posicion = numeros.findIndex((numero) => numero === buscaNumero);

if (posicion && posicion !== -1) {
  document.write("<h1>Encontrado!!!</h1>");
  document.write(`<h1>La posición del número es: ${posicion} </h1>`);
} else {
  document.write("<h1>Núnero no encontrado!!!</h1>");
}
