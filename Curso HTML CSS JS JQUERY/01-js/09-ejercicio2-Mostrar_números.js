"use strict";

//Hacer un programa que muestre todos los números
//introducidos por el usuario.

let numero1 = parseInt(prompt("Introduce el primer número"));
let numero2 = parseInt(prompt("Introduce el segundo número"));

for (let index = numero1; index <= numero2; index++) {
  document.write(
    `<h3>Estos son los números entre ${numero1} y el ${numero2}: ${index}</h3>`
  );
}
