"use strict";

let peliculas = [
  "Bad Boys",
  "Focus",
  "Star Wars",
  "Anabell",
  "La cena de los idiotas",
];

//buscar en un array
//find

const buscar = peliculas.find((pelicula) => {
  return pelicula === "Focus";
});

//es lo mismo que hacer

const buscar1 = peliculas.find((pelicula) => pelicula === "Focus");

console.log(buscar);
console.log(buscar1);

//some()
//busca valores iguales o mayores que,
//esto devuelve un true o false.

const precios = [10, 21, 55, 68, 37, 99];

const buscar2 = precios.some((precio) => precio < 75);

console.log(buscar2);
