"use strict";

//Peticiones a servicios API Rest
//Fetch es susutituto de AJAX.
//Fetch realiza consultas asincronas.

let div_tiempo = document.querySelector("#tiempo");
let nombres = [];

fetch("https://www.el-tiempo.net/api/json/v2/provincias")
  .then((data) => data.json())
  .then((data) => {
    nombres = data.provincias;
    console.log(nombres);
    for (let index = 0; index < data.provincias.length; index++) {
      let nombre = document.createElement("h3");

      nombre.innerHTML = `  ${data.provincias[index].NOMBRE_PROVINCIA} Capital de la provincia: ${data.provincias[index].CAPITAL_PROVINCIA}`;

      div_tiempo.appendChild(nombre);
      document.querySelector(".load").style.display = "none";
    }
  });
