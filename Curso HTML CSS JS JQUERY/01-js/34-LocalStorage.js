"use strict";

//Almacenamiento en el navegador. ¡¡¡SOLO EN ESE NAVEGADOR!!!

//Comprobar si tenemos localstorage

if (typeof Storage !== "undefined") {
  console.log("LocalStorage está disponible");
} else {
  console.log("Incompatible, no esta disponible");
}

//Guardar datos

localStorage.setItem("Nombre", "Oscar");

//recuperar datos

console.log("Recuperar datos: ", localStorage.getItem("Nombre"));

//recuperar datos y mostrarlos en pantalla

document.write(localStorage.getItem("Nombre"));

//recuperar datos y mostrarlos en pantalla en lugas de "Mis peliculas"

document.querySelector("#peliculas").innerHTML = localStorage.getItem("Nombre");

//Guardar objetos

const usuario = {
  nombre: "Oscar",
  email: "oscar@oscar.es",
  edad: 48,
};

//para guardar los datos de un objeto debe hacerse transformandolo
//a string.

localStorage.setItem("usuario", JSON.stringify(usuario));

//recuperar datos de un objeto

const dato = JSON.parse(localStorage.getItem("usuario"));

console.log(dato);

//Mostrar la información en pantalla
document.write(`NOMBRE :   ${dato.nombre}  `);
document.write(`Correo :  ${dato.email}  `);
document.write(`Edad :  ${dato.edad}`);

//Borrar datos concretos
localStorage.removeItem("usuario");

//Limpiar todo

localStorage.clear();
