"use strict";

//Todas las tablas de multiplicar

let numero = "";

for (let multiplicador = 0; multiplicador <= 10; multiplicador++) {
  document.write(`<h1>Tabla del ${numero}</h1>`);
  for (let numero = 0; numero <= 10; numero++) {
    document.write(
      `<h3>${numero} x ${multiplicador} = ${numero * multiplicador}</h3>`
    );
  }
}
