"use strict";

//Mostrar si un número es par o impar
//1. Ventana promnt
//2. Si no es valido nos pida el número denuevo.

let numero = parseInt(prompt("Pon el número"));

while (isNaN(numero)) {
  numero = parseInt(prompt("Pon el número"));
}

if (numero % 2 === 0) {
  alert(`El número ${numero} es par`);
} else {
  alert(`El número ${numero} es impar`);
}
