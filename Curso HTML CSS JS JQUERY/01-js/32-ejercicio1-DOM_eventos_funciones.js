"use strict";
console.log("CACA");
window.addEventListener("load", function () {
  console.log("DOM cargado!!");

  const formulario = document.querySelector("#formulario");
  const dashed = document.querySelector(".dashed");

  formulario.addEventListener("submit", function () {
    console.log("Evento submit capturado");

    const nombre = document.querySelector("#nombre").value;
    const apellidos = document.querySelector("#apellidos").value;
    const edad = parseInt(document.querySelector("#edad").value);

    if (nombre.trim() === null || nombre.trim().length === 0) {
      alert("El campo nombre debe rellenarse");
      document.querySelector("#errorNombre").innerHTML =
        "El nombre no es válido";
      return false;
    }
    if (apellidos.trim() === null || apellidos.trim().length === 0) {
      alert("El campo apellidos debe rellenarse");
      return false;
    }
    if (edad === null || edad <= 18 || isNaN(edad)) {
      alert("Debes rellenar este campo y tener 18 o más años");
      return false;
    }

    const p_nombre = document.querySelector("#p_nombre");
    const p_apellidos = document.querySelector("#p_apellidos");
    const p_edad = document.querySelector("#p_edad");

    p_nombre.innerHTML = nombre;
    p_apellidos.innerHTML = apellidos;
    p_edad.innerHTML = edad;
    console.log(nombre, apellidos, edad);
  });
});
