"use strict";

let texto1 = "Hola bienvenido, hoy tenemos un día muy bonito";
let texto2 =
  "Esto es un curso que va a ser muy largo, pero es un curso muy bueno.";

//Todos los métodos funcionan con strings y números.

//indexOf ó search
//buscar la posición (en el caracter que empieza) de la
//palabra que buscas, en el ejemplo la palabra "hoy"
//Si la palabra no esta, devuelve un -1.
//Siempre busca la primera coincidencia, sólo vale
//para encontrar la primera coincidencia
//Con lastIndexOf(), buscas la última coincidencia
console.log("indexOf() = search()");
const buscar = texto1.indexOf("hoy");
console.log(texto1);
console.log("La palabra 'hoy' empieza en el caracter ", buscar);
console.log("*******************************");

//Con lastIndexOf(), buscas la última coincidencia
console.log("lastIndexOf() = search()");
const buscar1 = texto2.lastIndexOf("curso");
console.log(texto2);
console.log(
  "La última coincidencia de 'curso' empieza en el caracter ",
  buscar1
);
console.log("*******************************");

//match (buscca la primera coincidencia)
//Busca la palabra y la devuelve dentro de un array
//Te indica el caracter en el que en empieza la palabra.
console.log("match()");
const buscar2 = texto1.match("un");
console.log(buscar2);
console.log("*******************************");

//substr(numero1, numero2)
//muestra una cantidad de caracteres donde número1 es a partír que
//caracter empezamos a mostrar y número2 la cantidad de caracteres
//que mostramos
console.log("substr(11,5)");
const buscar3 = texto2.substr(11, 5);
console.log(`Los caracteres buscados en: ${texto2} `, "son: ", buscar3);
console.log("*******************************");

//chartAt(numero)
//Muestra el caracter que se encuentra en ese número
console.log("chartAt(x)");
const buscar4 = texto2.charAt(11);
console.log(
  buscar4,
  `es el caracter que se encuentra en la posición 11 de la frase: ${texto2}`
);
console.log("*******************************");

//startsWith()
//Busca en el principio de la cadena, lo que indiquemos entre los ()
//Si coincide devuelve true y si no lo encuentra da false
console.log("startsWith()");
const buscar5 = texto1.startsWith("Hola bien");
console.log(
  `Buscamos 'Hola bien", en la frase ${texto1} y nos devuelve`,
  buscar5
);
const buscar6 = texto1.startsWith("bienvenido");
console.log(
  `Buscamos 'bienvenido", en la frase ${texto1}  y nos devuelve`,
  buscar6
);
console.log("*******************************");

//endsWith()
//Busca al final de la cadena, lo que indiquemos entre los ()
//Si coincide devuelve true y sino lo encuentra da false
console.log("endsWith()");
const buscar7 = texto1.endsWith("muy bonito");
console.log(
  `Buscamos 'muy bonito", en la frase ${texto1} y nos devuelve`,
  buscar7
);
const buscar8 = texto1.endsWith("muy feo");
console.log(
  `Buscamos 'muy feo", en la frase ${texto1} y nos devuelve`,
  buscar8
);
console.log("*******************************");

//includes
//busca la palabra y devuelve true o false.
console.log("includes()");
const buscar9 = texto1.includes("muy");
console.log(`Buscamos 'muy", en la frase ${texto1} y nos devuelve`, buscar9);
const buscar0 = texto1.includes("ganso");
console.log(`Buscamos 'ganso", en la frase ${texto1}  y nos devuelve`, buscar0);
console.log("*******************************");
