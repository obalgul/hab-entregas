"use strict";

window.addEventListener("load", function () {
  //setInterval (se ejecuta en bucle)
  /*
  const tiempo = setInterval(function () {
    console.log("Ejecutado el set interval");
    const titulo = document.querySelector("h1");
    if (titulo.style.fontSize === "60px") {
      titulo.style.fontSize = "30px";
      titulo.style.background = "red";
    } else {
      titulo.style.fontSize = "60px";
      titulo.style.background = "green";
    }
  }, 500);
  */
  //setInterval donde puedes parar y reiniciar el proceso
  function intervalo() {
    const tiempo = setInterval(() => {
      console.log("Ejecutado el set interval");
      const titulo = document.querySelector("h1");
      if (titulo.style.fontSize === "60px") {
        titulo.style.fontSize = "30px";
        titulo.style.background = "red";
      } else {
        titulo.style.fontSize = "60px";
        titulo.style.background = "green";
      }
    }, 1000);
    return tiempo;
  }

  const tiempo = intervalo();
  const stop = document.querySelector("#stop");

  stop.addEventListener("click", function () {
    alert("Has parado la transición");
    clearInterval(tiempo);
  });

  const start = document.querySelector("#start");

  start.addEventListener("click", function () {
    alert("Has iniciado la transición");
    intervalo();
  });

  /*
  //setTimeOut (se ejecuta solo una vez)
  //setInterval (se ejecuta en bucle)
  const tiempo = setTimeout(function () {
    console.log("Ejecutado el set TimeOut");
    const titulo = document.querySelector("h1");
    if (titulo.style.fontSize === "60px") {
      titulo.style.fontSize = "30px";
      titulo.style.background = "red";
    } else {
      titulo.style.fontSize = "60px";
      titulo.style.background = "green";
    }
  }, 500);
  */
});
