"use strict";

//Tabla de multiplicar de un número introducido por pantalla

let numero = parseInt(prompt("Introduce el número de la tabla a mostrar"));

while (isNaN(numero)) {
  numero = parseInt(prompt("Introduce el número de la tabla a mostrar"));
}

document.write(`<h1>Tabla del ${numero}</h1>`);
for (let index = 0; index <= 10; index++) {
  document.write(`<h3>${numero} * ${index} = ${numero * index}</h3>`);
}
