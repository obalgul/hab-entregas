"use strict";

const formulario = document.querySelector("#formPeliculas");

formulario.addEventListener("submit", function () {
  const titulo = document.querySelector("#addPeliculas").value;
  if (titulo.length >= 1) {
    localStorage.setItem(titulo, titulo);
  } else {
    alert("El campo no puede estar vacío");
  }
});

let ul = document.querySelector("#listaPeliculas");

for (let index in localStorage) {
  if (typeof localStorage[index] === "string") {
    let li = document.createElement("li");
    li.append(localStorage[index]);
    ul.append(li);
  }
}

const formularioBorrar = document.querySelector("#erasePeliculas");

formularioBorrar.addEventListener("submit", function () {
  const titulo = document.querySelector("#deletePeliculas").value;
  if (titulo.length >= 1) {
    localStorage.removeItem(titulo, titulo);
  }
});
