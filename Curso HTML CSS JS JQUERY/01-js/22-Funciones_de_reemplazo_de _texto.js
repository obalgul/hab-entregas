"use strict";

let texto = "Esto es un curso que va a ser muy largo.";
let texto_espacios = "        Esto es un curso que va a ser muy largo.      ";
//replace(par1, par2)
//Nos sirve para reemplazar una palabra o cadena por otra
//par1 es la palabra origen, la palabra a sustituir.
//par2 es la palabra destino, es la palabra que sustituya a par1

const texto1 = texto.replace("muy", "algo");
console.log(`Texto original: ${texto}`, `Texto modificado: ${texto1}`);
console.log("***************************");

//slice(x)
//Devuelve la cadena string desde el número que indiquemos
//La x es el caracter donde empezaria la nueva cadena o string

const texto2 = texto.slice(8);
console.log(`Texto original: ${texto}`, `Texto modificado: ${texto2}`);
console.log("***************************");

//slice(x,x2)
//Devuelve la cadena string entrelos número que indiquemos
//La x es el caracter donde empezaria la nueva cadena o string
//x2 sería el final de la cadena o string

const texto3 = texto.slice(8, 24);
console.log(`Texto original: ${texto}`, `Texto modificado: ${texto3}`);
console.log("***************************");

//split()
//Guarda la cadena o string en un array. Si entre los parentesis
//ponemos un espacio (" "), separará las palabras con ','

const texto4 = texto.split();
console.log(`Texto original: ${texto}`, texto4);
console.log("***************************");

const texto5 = texto.split(" ");
console.log(`Texto original: ${texto}`, texto5);
console.log("***************************");

//trim()
//Elimina los espacios tanto por delante como por detrás

const texto6 = texto_espacios.trim();
console.log(`Texto original: ${texto_espacios}`, `Texto modificado: ${texto6}`);
console.log("***************************");
