"use strict";

let numero = 5;
let texto = "Hola mundo soy una variable global";

function holaMundo(texto) {
  let hola_mundo = "texto dentro de función";
  console.log(texto);
  console.log(numero);
  console.log(hola_mundo);
}

holaMundo(texto);
console.log(hola_mundo);

//La variable hola_mundo sólo esta disponible dentro de la función
//Ya que es aquí donde se ha creado. Si se encontrase fuera de la
//función como la variable numero, entonces la puedes llamar tanto
//dentro como fuera de la función ya que fue creada en el ambito
//global.
