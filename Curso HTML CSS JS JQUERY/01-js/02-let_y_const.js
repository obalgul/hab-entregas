"use strict";

//Prueba con var
var numero1 = 40;
console.log(numero1, "con var"); //resultado 40

if (true) {
  var numero1 = 50;
  console.log(numero1, "con var"); //resultado 50
}

console.log(numero1, "con var es una variable global. No se utiliza."); //resultado 40

//Prueba con let
var numero2 = 40;
console.log(numero2, "con var"); //resultado 40

if (true) {
  let numero2 = 50;
  console.log(numero2, "con let, solo guarda el valor dentro del IF"); //resultado 50
}

console.log(numero2, "con var"); //resultado 40
