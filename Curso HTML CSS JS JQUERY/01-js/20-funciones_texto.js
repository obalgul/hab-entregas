"use strict";

//Transformación de texto
//cambiar a string

let numero = 125;
let dato = numero.toString();
console.log(dato, "/ Ahora es un string");
console.log(typeof dato);
console.log("**************************");
//pasar a minusculas

let frase_mayusculas = "SOY EL MEJOR";
let frase1 = frase_mayusculas.toLowerCase();
console.log(frase1, "/ Esto estaba en mayusculas");
console.log("**************************");
//cambiar a mayusculas

let frase_minusculas = "soy el mejor";
let frase2 = frase_minusculas.toUpperCase();
console.log(frase2, "/ Esto estaba en minusculas");
console.log("**************************");
//Calcular longuitud del texto

let texto_length = "¿Cúantos caracteres tengo?";
console.log(texto_length, "/ tengo ", texto_length.length);
console.log("**************************");
//concatenar

let texto1 = "Hola bienvenido";
let texto2 = "Esto es un curso";
let textoTotal = `${texto1} ${texto2}`;
console.log(texto1, " ", texto2);
console.log(textoTotal, "/ Concatenar con '`'");
console.log("**************************");
//concatenar con función concat
let textoTotal1 = texto1.concat(" ", texto2);
console.log(texto1, " ", texto2);
console.log(textoTotal1, "/ Concatenar con la función concat()");
console.log("**************************");
