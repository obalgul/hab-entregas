"use strict";

//Funciones anonimas
//Es una función sin nombre
/*
let pelicula = function (nombre) {
  return `La pelicula se llama ${nombre}`;
};

//callbacks
/*
//Función normal
function suma(numero1, numero2) {
  let sumar = numero1 + numero2;
  return sumar;
}

console.log(suma(10, 5));

//función callback
//La función callback realizará la suma y la suma *2

function sumame(numero1, numero2, suma, sumaPorDos, sumaPorTres) {
  let sumar = numero1 + numero2;

  suma(sumar);
  sumaPorDos(sumar);
  sumaPorTres(sumar);
  return sumar;
}

sumame(
  10,
  5,
  function (dato) {
    console.log(`La suma de 10 + 5 es ${dato}`);
  },
  function (dato) {
    console.log(`La suma de 10 + 5 * 2 es ${dato * 2}`);
  },
  function (dato) {
    console.log(`La suma de 10 + 5 * 2 es ${dato * 3}`);
  }
);
*/
//Funciones flecha
//En la misma función anterior sustituimos dentro de la llamada a sumame
//los function por => y lo ponemos a la derecha del parametro, y esto
// ya es una función flecha.
function sumame(numero1, numero2, suma, sumaPorDos, sumaPorTres) {
  let sumar = numero1 + numero2;

  suma(sumar);
  sumaPorDos(sumar);
  sumaPorTres(sumar);
  return sumar;
}

sumame(
  10,
  5,
  (dato) => {
    console.log(`La suma de 10 + 5 es ${dato}`);
  },
  (dato) => {
    console.log(`La suma de 10 + 5 * 2 es ${dato * 2}`);
  },
  (dato) => {
    console.log(`La suma de 10 + 5 * 2 es ${dato * 3}`);
  }
);
