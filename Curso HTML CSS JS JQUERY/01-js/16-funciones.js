"use strict";
/*
//Defino la función
function calculadora(){
    //conjunto de instrucciones a realiar
    return "Hoal soy la calculadora"
}

//Invocar o llamar a la función
console.log(calculadora());
*/

//Parametros de la función
//numero1, numero2 son los parametros en está función
/*
function calculadora(numero1, numero2) {
  console.log(`Suma:` + (numero1 + numero2));
  console.log(`Resta:` + (numero1 - numero2));
  console.log(`Multiplicación:` + numero1 * numero2);
  console.log(`División:` + numero1 / numero2);
  console.log("*****************************");

  return "Estos son los resultados";
}

calculadora(5, 10);
calculadora(20, 10);
*/
//función para realizar varias operaciones con un bucle
/*
function calculadora(numero1, numero2) {
  console.log(`Suma:` + (numero1 + numero2));
  console.log(`Resta:` + (numero1 - numero2));
  console.log(`Multiplicación:` + numero1 * numero2);
  console.log(`División:` + numero1 / numero2);
  console.log("*****************************");

  return "Estos son los resultados";
}

for (let index = 1; index < 10; index++) {
  console.log(`Operacines con el número 10 y el ${index}`);
  calculadora(10, index);
}
*/
//parametros opcionales (true false)

function calculadora(numero1, numero2, mostrar = false) {
  if (mostrar === false) {
    console.log(`Suma:` + (numero1 + numero2));
    console.log(`Resta:` + (numero1 - numero2));
    console.log(`Multiplicación:` + numero1 * numero2);
    console.log(`División:` + numero1 / numero2);
    console.log("*****************************");
  } else {
    document.write(`<h2>Suma: ${numero1 + numero2}</h2>`);
    document.write(`<h2>Resta: ${numero1 - numero2}</h2>`);
    document.write(`<h2>Multiplicación: ${numero1 * numero2}</h2>`);
    document.write(`<h2>División: ${numero1 / numero2}</h2>`);
    document.write("*****************************");
  }
  return "Estos son los resultados";
}

calculadora(10, 5);
calculadora(20, 10, true);
