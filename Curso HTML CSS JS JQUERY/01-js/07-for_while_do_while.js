"use strict";

//FOR

let numero = 100;

for (let index = 0; index <= numero; index++) {
  console.log(index);
}

//while

let year = 2018;

while (year <= 2030) {
  console.log(" Estamos en el año ", year);
  year++;
}

let year1 = 2018;

while (year1 !== 2000) {
  console.log(" Estamos en el año ", year1);
  year1--;
}

//do while

let year2 = 30;

do {
  console.log("Solo cuando se diferente de 20");
  year2--;
} while (year2 > 24);
