"use strict";

//Parametro Rest (...)
function listaFrutas(fruta1, fruta2, ...resto_de_frutas) {
  console.log("Fruta1: ", fruta1);
  console.log("Fruta2: ", fruta2);
  console.log(resto_de_frutas);
}

listaFrutas("Naranja", "Pera", "Cereza", "Fresa", "Platano", "Sandia");

//Es lo mismo que lo anterior se llama Parametro Spread (...)
let frutas = ["Naranja", "Pera"];
listaFrutas(...frutas, "Cereza", "Fresa", "Platano", "Sandia");
