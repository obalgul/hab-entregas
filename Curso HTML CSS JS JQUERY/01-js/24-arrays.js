"use strict";

//arrays básicos

const nombre = "Oscar";
let nombres = ["Oscar", "Jose", "Roberto", "Marta", "Jenny"];

let lenguajes = new Array("PHP", "Java", "JavaScript", "Phyton");

console.log(nombres);
console.log(lenguajes);

//Métodos para array
//ver la longuitrud del array

console.log("Longuitud del array nombres:", nombres.length);

//Sacar un elemento de un array
//llamamos al array nombres y entre corchetes ponemos el indice del
//elemento
/*
console.log("Así sacamos el indice 3 del array, que es:", nombres[3]);

const elemento = parseInt(prompt("¿Qué elemento del array quieres?"));
if (elemento >= nombres.length) {
  alert("Pon un número menor que " + nombres.length);
} else {
  alert(
    `El nombre que hay en la posición ${nombres.length} es: ` +
      nombres[elemento]
  );
}
*/
//Mostrar todos los elementos del array.
//recorrer el array con for

document.write("<h1>Lista de lenguajes de programación.</h1>");
document.write("<ul>");
for (let index = 0; index < lenguajes.length; index++) {
  document.write(`<li>${lenguajes[index]}</li>`);
}
document.write("</ul>");

//recorrer el array con forEach(elemento, indice, array)
//El parametro elemento son los elementos del array, indice es la
//posición dentro del array y array es el array original

document.write("<h1>Lista de lenguajes de programación.</h1>");
document.write("<ul>");

lenguajes.forEach((elemento, index) => {
  document.write(`<li>${elemento} posición dentro del array ${index}</li>`);
});
document.write("</ul>");

//recorrer el array con for in

document.write("<h1>Lista de lenguajes de programación.</h1>");
document.write("<ul>");

for (let lenguaje in lenguajes) {
  document.write(
    `<li>${lenguajes[lenguaje]} posición dentro del array ${[lenguaje]}</li>`
  );
}
document.write("</ul>");
