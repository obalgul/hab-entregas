"use strict";

//Programa que pida dos numneros e identifique si son iguales
// mayor que  menor uno del otro. Si los numeros son menores
//o iguales a cero, nos vi¡ueleva a pedir introducir los números

let numero1 = Number(prompt("Introduce el primer número"));
let numero2 = Number(prompt("Introduce el segundo número"));

while (numero1 <= 0 || numero2 <= 0 || isNaN(numero1) || isNaN(numero2)) {
  numero1 = parseInt(prompt("Introduce el primer número"));
  numero2 = parseInt(prompt("Introduce el primer número"));
}

if (numero1 > numero2) {
  alert(
    `El primer número ${numero1} es mayor que el segundo número ${numero2}`
  );
} else if (numero1 < numero2) {
  alert(
    `El primer número ${numero1} es menor que el segundo número ${numero2}`
  );
} else if (numero1 === numero2) {
  alert(
    `El primer número ${numero1} y el segundo número${numero2} son iguales`
  );
} else if ((numero1 && numero2) === 0 || (numero1 && numero2) === String) {
  alert(
    Number(
      prompt(
        "El número introducido debe ser mayor que cero. Vuelve a introduceir los números"
      )
    )
  );
}
