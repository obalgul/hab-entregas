"use strict";

//programar una calculadora

let numero1 = parseInt(prompt("introduce el primer número"));
let numero2 = parseInt(prompt("introduce el segundo número"));

while (numero1 < 0 || numero2 < 0 || isNaN(numero1) || isNaN(numero2)) {
  numero1 = parseInt(prompt("introduce el primer número"));
  numero2 = parseInt(prompt("introduce el segundo número"));
}

/**************  SALIDA POR CONSOLA ***************/

let resultado = console.log("El resultado de la operación es:");
console.log(`La suma es ${numero1} + ${numero2} = ${numero1 + numero2}`);
console.log(` La resta es ${numero1} - ${numero2} = ${numero1 - numero2}`);
console.log(
  ` La multiplicación es ${numero1} * ${numero2} = ${numero1 * numero2}`
);
console.log(` La división es ${numero1} / ${numero2} = ${numero1 / numero2}`);

/***************  SALIDA EN DOCUMENTO (PANTALLA) ******************/

let resultado1 = document.write("<h1>El resultado de la operación es:</h1>");
document.write(
  `<p> La suma es ${numero1} + ${numero2} = ${numero1 + numero2}</p>` +
    `<p> La resta es ${numero1} - ${numero2} = ${numero1 - numero2}</p>` +
    `<p> La multiplicación es ${numero1} * ${numero2} = ${
      numero1 * numero2
    }</p>` +
    `<p> La división es ${numero1} / ${numero2} = ${numero1 / numero2}</p>`
);

/****************      SALIDA POR AVISO (ALERT) **************************/

let resultado2 = alert("El resultado de la operación es:");
alert(`La suma es ${numero1} + ${numero2} = ${numero1 + numero2}`);
alert(` La resta es ${numero1} - ${numero2} = ${numero1 - numero2}`);
alert(` La multiplicación es ${numero1} * ${numero2} = ${numero1 * numero2}`);
alert(` La división es ${numero1} / ${numero2} = ${numero1 / numero2}`);
