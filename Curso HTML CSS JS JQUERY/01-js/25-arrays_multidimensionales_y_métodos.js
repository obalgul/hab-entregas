"use strict";

//arrays multidimensionales (arrays dentro de otros arrays)
//Para mostrar los elementos del array, tenemos que llamarlos
//desde el array cine[x][x2], donde [x] es la posición del array
//del que queremos mostrar el elemento y [x2] es el elemento del
//array escogido en [x]

let categorias = ["Acción", "Terror", "Comedia"];
let peliculas = [
  "Bad Boys",
  "Focus",
  "Star Wars",
  "Anabell",
  "La cena de los idiotas",
];

let cine = [categorias, peliculas];
console.log(cine);
console.log(cine[0][0]);
console.log(cine[1][3]);

//Añadir elementos a un array
//push()
//Se añade el elemento que ponemos entre los ().
console.log("Metodo push(), añadimos Superman");
peliculas.push("Superman");
console.log(peliculas);
console.log("***********************");

//pop()
//elimina el último elemento del array
console.log("Método pop(), eliminamos la última posición");
peliculas.pop();
console.log(peliculas);
console.log("***********************");

//splice(index, numero)
//el primer parametro index es la posición del elemento dentro del array
//el parametro numero es el número de elementos que queremos eliminar
console.log("Metodo splice(1,1), eliminamos Terror");
console.log(categorias);
categorias.splice(1, 1);
console.log("Después de borrar", categorias);
console.log("***********************");

//desde esa posición.
//eliminar elementos del array segun su indice
//dentro del indexOf(), introduciremos la pelicula a eliminar
console.log("Buscando en el array splice(index, 1), eliminamos Focus");
let indice = peliculas.indexOf("Focus");
if (indice > -1) {
  peliculas.splice(indice, 1);
}

console.log("Después de borrar 'Focus'", peliculas);
console.log("***********************");

//join()
//Transformar un array en un string o cadena de texto

const peliculas_string = peliculas.join();

console.log(peliculas_string);
console.log("***********************");

//split()
//transforma un string en un array
//Si no usas parametros todo será un elemento, pero pones un
//parametreo que los separe por comas, cada grupo de palabra
//será un elemento del array

const masPeliculas = "Superdetective 86, 007, Mortadelo y Filemón, Krank";

console.log(masPeliculas.split(","));
console.log("***********************");

//sort()
//ordenar arrays (por orden alfabetico)
console.log(peliculas);
const peliculas_ordenadas = peliculas.sort();
console.log(peliculas_ordenadas);
console.log("***********************");

//reverse()
//le da la vuelta al array, pone del último de primero,
//y así con todos los elementos hasta llegar al último
console.log(peliculas);
const peliculas_revertidas = peliculas.reverse();
console.log(peliculas_revertidas);
console.log("***********************");
