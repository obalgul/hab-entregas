import React from "react";
import Todo from "./Todo";

export default function TodoList({ list }) {
  return (
    <ul>
      {list.map((item) => {
        return (
          <li key={item.id}>
            <Todo text={item.text} done={item.done} />
          </li>
        );
      })}
    </ul>
  );
}
