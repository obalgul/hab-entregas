import React from "react";

export default function Footer() {
  return (
    <footer className="principal">
      <p>Hecho en el Taller de React en Octubre de 2020</p>
    </footer>
  );
}
