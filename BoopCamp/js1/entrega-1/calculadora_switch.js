"use strict";

let numberA = +prompt("Introduce número A para la operación");
let numberB = +prompt("Introduce número B para la operación");
let operadores = prompt("Introduce el operador para realizar la operacdión");

switch (operadores) {
  case "+":
    console.log("La suma de A más B es " + (numberA + numberB));
    break;
  case "-":
    console.log("La resta de A menos B es " + (numberA - numberB));
    break;
  case "*":
    console.log("La miultiplicación de A por B es " + numberA * numberB);
    break;
  case "/":
    console.log("La división entre A y B es " + numberA / numberB);
    break;
  case "**":
    console.log("La potencia de A a B es " + numberA ** numberB);
    break;
  default:
    console.error("No se ha seleccionado un tipo de operación correcto.");
    break;
}
