"use strict";

const mediaTeamMaria = (62 + 34 + 55) / 3;
const mediaTeamPaula = (35 + 60 + 59) / 3;
const mediaTeamRebeca = (40 + 39 + 63) / 3;

if (mediaTeamMaria > mediaTeamPaula && mediaTeamMaria > mediaTeamRebeca) {
  console.log(
    "El equipo de Maria tiene la media más alta y es de " + mediaTeamMaria
  );
} else if (
  mediaTeamPaula > mediaTeamMaria &&
  mediaTeamPaula > mediaTeamRebeca
) {
  console.log(
    "El equipo de Paula tiene la media más alta y es de " + mediaTeamPaula
  );
} else if (
  mediaTeamRebeca > mediaTeamPaula &&
  mediaTeamRebeca > mediaTeamMaria
) {
  console.log(
    "El equipo de Rebeca tiene la media más alta y es de " + mediaTeamRebeca
  );
}
