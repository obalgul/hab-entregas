"use strict";

function JugarPartida(puntosAJugar) {
  let totalPuntos = 0;
  let maximoPuntos = puntosAJugar;
  for (
    let intentosLanzados = 1;
    intentosLanzados < maximoPuntos;
    intentosLanzados++
  ) {
    let tirarDado = Math.ceil(Math.random() * 6);
    totalPuntos = totalPuntos + tirarDado;
    if (totalPuntos < maximoPuntos) {
      console.log(
        `Esta es tu tirada número ${intentosLanzados}, te ha salido un ${tirarDado}. Y has acumulado ${totalPuntos} puntos.`
      );
    } else if (totalPuntos >= maximoPuntos) {
      return console.log(
        `!!!ENHORABUENA¡¡¡ Has ganado en ${intentosLanzados} tiradas, has sacado en tu última tirada un ${tirarDado}. Y has ganado con una puntuación de ${totalPuntos} puntos.`
      );
    }
  }
}

console.log(JugarPartida(100));

/* Ya he solucionado el problema de que no sumase bien.
Además de solucionar esto. He creado un parametro que
(puntos AJugar), que permite cambiar la cifra a la que
vamos a jugar.*/
