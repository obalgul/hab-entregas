"use strict";

let numberA = +prompt("Introduce número A para la operación");
let numberB = +prompt("Introduce número B para la operación");
let operadores = prompt("Introduce el operador para realizar la operacdión");

if (operadores === "+") {
  console.log("El resultado de la suma es " + (numberA + numberB));
} else if (operadores === "-") {
  console.log("El resultado de la resta es " + (numberA - numberB));
} else if (operadores === "*") {
  console.log("El resultado de la multiplicación es " + numberA * numberB);
} else if (operadores === "/") {
  console.log("El resultado de la división es " + numberA / numberB);
} else if (operadores === "**") {
  console.log("El resultado de la potencia de A a B es " + numberA ** numberB);
} else {
  console.error("No se ha seleccionado un tipo de operación correcta.");
}
