'use strict';

const euros = 10;
console.log(`Cantidad a cambiar ${euros} €`);

async function changeEuroToDollarToYen(euros) {
  const response1 = await fetch('https://api.exchangerate-api.com/v4/latest/EUR');
  const responseData = await response1.json();
  const dollarData = responseData.rates.USD * euros;
  const dateChange = responseData.date;
  console.log(`Hoy ${dateChange}, la cantidad de ${euros}€ al cambio son ${dollarData.toFixed(2)}$`);
  const yenData = responseData.rates.JPY * dollarData;
  console.log(`Hoy ${dateChange}, la cantidad de ${dollarData.toFixed(2)}$ al cambio son ${yenData.toFixed(2)}¥`);
}
changeEuroToDollarToYen(euros);
