const itemNames = ['Camisa', 'Pantalon', 'Calcetines'];
const itemPrices = [13, 27, 100];

class Item {
  name;
  price;
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }
}

class CartItem extends Item {
  add = 1;

  constructor(item) {
    super(item.name, item.price);
  }

  increaseadd() {
    this.add++;
  }

  get total() {
    return this.add * this.price;
  }
}

class User {
  #shoppingCar = [];

  addToCart(item) {
    const itemFound = this.#shoppingCar.find((cartItem) => {
      return item.name === cartItem.name;
    });
    if (itemFound) {
      itemFound.increaseadd();
    } else {
      this.#shoppingCar.push(new CartItem(item));
    }
  }

  get cart() {
    return this.#shoppingCar;
  }

  fillToCart(inventory, add) {
    for (let i = 0; i < add; i++) {
      const inventoryIndex = Math.floor(Math.random() * inventory.length);
      this.addToCart(inventory[inventoryIndex]);
    }
  }

  buy() {
    Shop.checkout(this.#shoppingCar);
  }
}

class Shop {
  static checkout(shoppingCar) {
    console.log(`tienda`);

    const initialTotals = { totalPrice: 0, totalUnits: 0 };
    const totals = shoppingCar.reduce((accumulator, currentItem) => {
      console.log(
        `${currentItem.name} ${currentItem.price}€ ${currentItem.add}U Total de prendas: ${currentItem.total}`
      );
      accumulator.totalPrice += currentItem.total;
      accumulator.totalUnits += currentItem.add;
      return accumulator;
    }, initialTotals);

    console.log(`Unidades totales ${totals.totalUnits}, Total compra ${totals.totalPrice}€`);
  }
}

const inventory = itemNames.map((name, index) => {
  return new Item(name, itemPrices[index]);
});

const myUser = new User();
myUser.fillToCart(inventory, 15);
myUser.buy();

// David, este eljercicio esta con las correcciones de Ivan.
