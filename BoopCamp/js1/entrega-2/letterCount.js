"use strict";

function letterCount(str) {
  let myStringSplit = str.split(" "); //creamos array
  let longitudPalabra = 0;
  let textoString = ""; //variables para guardar los elementos del array
  for (let i = 0; i < myStringSplit.length; i++) {
    if (myStringSplit[i].length > longitudPalabra) {
      //buscamos dentro del array la longuitut de la palabra más larga
      longitudPalabra = myStringSplit[i].length; //aplicamos la primera condición, longuitud de palabra
      textoString = myStringSplit[i]; //aplicamos la segunda condición, buscar la palabra más larga
    }
  }
  return textoString; //devuelve la palabra mas larga
}
console.log(letterCount("Hoy es un día estupendo y fantastico."));
//llamamos a la función
