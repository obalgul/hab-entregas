"use trict";

let numberBinary = prompt("Introduce número binario a convertir en decimal:");

numberBinary = numberBinary.toString();
numberBinary = numberBinary.split("");
let result = 0;
let contador = 0;
for (let i = numberBinary.length - 1; i >= 0; i--) {
  if (numberBinary[i] === "1") {
    result = result + 2 ** contador;
  }
  contador++;
}

console.log(result);
