"use strict";

function palimdromeTwo(str) {
  let myString = str.toLowerCase().replace(/ /g, "");
  console.log(myString);
  for (let i = 0; i < myString.length; i++) {
    if (myString[i] !== myString[myString.length - i - 1]) {
      return false;
    }
  }
  return true;
}

console.log(palimdromeTwo("Arriba la birra"));
