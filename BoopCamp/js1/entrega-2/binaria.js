"use strict";

function binaryConverter(str) {
  let numeroBinario = str;
  return parseInt(str).toString(2);
}

console.log(binaryConverter(2));

/* después de darle un millón de vueltas intentando con
arrays y bucles, decidi repasar los temas de MDN y me
encontre con la expresión parseInt. Desués de darle unas 
vueltas consegui que funcionase.
*/
