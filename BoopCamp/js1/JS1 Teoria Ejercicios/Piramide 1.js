'use strict';

// Piramide 1

function buildFloor(size) {
  let floor = '';
  for (let i = 0; i < size; i++) {
    floor = floor + '*';
  }
  return floor;
}

function printPyramid1(floors) {
  for (let i = 0; i < floors; i++) {
    console.log(buildFloor(i + 1));
  }
}

console.log(printPyramid1(5));
