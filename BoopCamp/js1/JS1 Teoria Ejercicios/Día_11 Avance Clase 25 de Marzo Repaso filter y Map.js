Avance clase 25 de marzo

Repaso de filter y map

Reduce
Reduce en MDN
Convierte un array de valores en un sólo valor. Es muy útil si quieres por ejemplo convertir un array de valores en su suma, pero no sólo para eso, con reduce podemos generar objetos o incluso replicar el comportamiento de map o filter
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

// 1 + 2 + 3 + 4
console.log(array1.reduce(reducer));
// 10

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5)); // Establecemos un valor inicial para el acumulador
// 15

Some
Some en MDN
Nos devuelve true o false en función de si algún elemento del array cumple la condición que establecemos en el callback. En cuanto encuentra el primero que cumple deja de comprobar los siguientes, así ahorra computación.

Every
Every en MDN
Devuelve true si todos los elementos del array cumplen la condición establecida en el callback. Cuando uno no cumple para de recorrer el array y devuelve false.

Find
Find en MDN
Nos devuelve el valor del primer elemento del array que satisface la condición que establecemos mediante nuestro callback. Una vez que encuentra el primero no sigue buscando para ahorrar computación. Si en lugar del valor nos interesase saber el indice que ocupa ese dato en el array, usaríamos findIndex

¿Cómo funcionan algunos de estos métodos por dentro? ¡Vamos a imitarlos!

Ejercicios map filter y reduce

Ejercicio de pacientes
Vamos a tener un array con nombres de personas, otro con las edades de estos usuarios y otro con booleanos que indican si el usuario está infectado. Deberemos crear un array con objetos que represente a cada una de las personas con map, después filtraremos este array para dejar sólo a los infectados con filter y por último sumaremos sus edades con reduce.

Reduce
Reduce en MDN
Convierte un array de valores en un sólo valor. Es muy útil si quieres por ejemplo convertir un array de valores en su suma, pero no sólo para eso, con reduce podemos generar objetos o incluso replicar el comportamiento de map o filter
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

// 1 + 2 + 3 + 4
console.log(array1.reduce(reducer));
// 10

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5)); // Establecemos un valor inicial para el acumulador
// 15

Some
Some en MDN
Nos devuelve true o false en función de si algún elemento del array cumple la condición que establecemos en el callback. En cuanto encuentra el primero que cumple deja de comprobar los siguientes, así ahorra computación.

Every
Every en MDN
Devuelve true si todos los elementos del array cumplen la condición establecida en el callback. Cuando uno no cumple para de recorrer el array y devuelve false.

Find
Find en MDN
Nos devuelve el valor del primer elemento del array que satisface la condición que establecemos mediante nuestro callback. Una vez que encuentra el primero no sigue buscando para ahorrar computación. Si en lugar del valor nos interesase saber el indice que ocupa ese dato en el array, usaríamos findIndex

Sort
Sort en MDN
¡CUIDADO SORT MODIFICA EL ARRAY ORIGINAL A DIFERENCIA DE MAP FILTER...!
Sort sirve para ordenar un array, por defecto convierte todas las posiciones a texto y las ordena según la codificación UTF16. Si queremos ordenar por otro criterio tenemos que pasarle un callback que tendrá dos parámetros e indicaremos en nuestra lógica cual es mayor y menor.

Ejemplos de repaso map y filter
let myUsers = [
  { name: 'Ivan', age: 35 },
  { name: 'Ana', age: 17 },
  { name: 'David', age: 27 }
];

let saludos = myUsers.map((user, index) => {
  return `${index}:Me llamo ${user.name}, tengo ${user.age} años`;
});

console.log(myUsers, saludos);

let adultUsers = myUsers.filter(function(user) {
  return user.age >= 18;
});

console.log(adultUsers);

Ejemplos reduce some every find y findIndex
let myUsers = [
  { name: 'Ivan', age: 35 },
  { name: 'Ana', age: 17 },
  { name: 'David', age: 27 }
];

// En este ejemplo usamos reduce como si fuese array.length
const userCount = myUsers.reduce((accumulator) => {
  return accumulator + 1;
}, 0);
console.log(userCount);

// Aquí sumamos las edades de un array de usuario
const totalAge = myUsers.reduce((accumulator, currentUser) => {
  return accumulator + currentUser.age;
}, 0);

console.log(totalAge);

// Con este reduce en realidad estamos haciendo un map como el del repaso de map
const salu2 = myUsers.reduce((saludos, usuario) => {
  saludos.push(`Me llamo ${usuario.name}, tengo ${usuario.age} años`);
  return saludos;
}, []);

console.log(salu2);

// Con este reduce estamos haciendo un filter como el del repaso de filter
const notAdults = myUsers.reduce((accumulator, user) => {
  if (user.age >= 18) {
    accumulator.push(user);
  }
  return accumulator;
}, []);

console.log(notAdults);

// En este ejemplo calculamos el total de un carrito de la compra
const carrito = [
  { name: 'camisa', price: 20, units: 2 },
  { name: 'camisa', price: 20, units: 5 },
  { name: 'pantalon', price: 50, units: 1 },
  { name: 'pantalon', price: 50, units: 3 }
];

// Primero con un bucle
let totalPrice = 0;
for (const item of carrito) {
  totalPrice = totalPrice + item.price * item.units;
}

// Y esto es lo mismo que lo del bucle pero implementado con reduce
const total = carrito.reduce((accumulator, item) => {
  return accumulator + item.price * item.units;
}, 0);

console.log(total, totalPrice);


//Con los siguiente some y filter conseguimos lo mismo, pero filter ejecuta más veces el callback
const someShirt = carrito.some((item) => {
console.log('VUELTA SOME');
  return item.name === 'camisa';
});
console.log(someShirt);

const someShirtFilter =
  carrito.filter((item) => {
    console.log('VUELTA FILTER');
    return item.name === 'camisa';
  }).length > 0;

console.log(someShirtFilter);

// Comprobamos si todos las posiciones del array tienen una camisa
const everyShirt = carrito.every((item) => {
  console.log('VUELTA EVERY');
  return item.name === 'camisa';
});

console.log(everyShirt);

//Buscamos el primer pantalon de un array
const findTrouser = carrito.find((item) => {
  console.log('VUELTA FIND');
  return item.name === 'pantalon';
});

console.log(carrito, findTrouser);

//Buscamos el primer indice de cualquier pantalón del array
const findIndexTrouser = carrito.findIndex((item) => {
  console.log('VUELTA FINDINDEX');
  return item.name === 'pantalon';
});

console.log(carrito, findIndexTrouser);

Ejemplo sort
const unsortedArray = [9, 3, 76, 2, 5, 8];
console.log(unsortedArray);
unsortedArray.sort((a, b) => {
  return a - b;
});
console.log(unsortedArray);

¿Cómo funcionan algunos de estos métodos por dentro? ¡Vamos a imitarlos!
Implementaciones propias de map y reduce. Son funciones que funcionan de forma similar a como lo hacen map y reduce en los arrays, no quiere decir que en relidad estén implementados así en el motor de Javascript.
A estas funciones le pasaremos como primer parámetro el array y de segundo el callback.
let myUsers = [
  { name: 'Ivan', age: 35 },
  { name: 'Ana', age: 17 },
  { name: 'David', age: 27 }
];

function myReduce(sourceArray, callback, initialValue) {
  let result = initialValue;
  for (let i = 0; i < sourceArray.length; i++) {
    result = callback(result, sourceArray[i]);
  }
  return result;
}

const addAges = (accumulator, user) => {
  return accumulator + user.age;
};

console.log(myUsers, myReduce(myUsers, addAges, 0), myUsers.reduce(addAges, 0));

function myMap(sourceArray, callback) {
  const result = [];
  for (let i = 0; i < sourceArray.length; i++) {
    result.push(callback(sourceArray[i], i, sourceArray));
  }
  return result;
}

console.log(
  myUsers,
  myMap(myUsers, (user) => {
    return user.age;
  }),
  myUsers.map((user) => {
    return user.age;
  })
);

Ejercicios map filter y reduce

Ejercicio de pacientes
Vamos a tener un array con nombres de personas, otro con las edades de estos usuarios y otro con booleanos que indican si el usuario está infectado. Deberemos crear un array con objetos que represente a cada una de las personas con map, después filtraremos este array para dejar sólo a los infectados con filter y por último sumaremos sus edades con reduce.
const names = ['Ivan', 'Ana', 'David'];
const ages = [15, 25, 46];
const infected = [true, false, true];

// Primero construimos un array de usuarios con map, este array lo hacemos mapeando por ejemplo el de names
const patients = names.map((name, index) => {
  return { name: name, age: ages[index], infected: infected[index] };
});
console.log(patients);

// Después filtramos y dejamos solo a los infectados
const infectedPatients = patients.filter((patient) => {
  return patient.infected;
});

console.log(infectedPatients);

// Por último sumamos las edades
const totalAge = infectedPatients.reduce((accumulator, infectedPatient) => {
  return accumulator + infectedPatient.age;
}, 0);
console.log(totalAge);
El anterior código está bien si queremos tener todos los arrays intermedios. Si lo único que nos interesa es la suma de las edades, podemos encadenar map, filter, reduce e incluso some, every...
const totalAgeOneStep = names
  .map((name, index) => {
    return { name: name, age: ages[index], infected: infected[index] };
  })
  .filter((patient) => {
    return patient.infected;
  })
  .reduce((accumulator, infectedPatient) => {
    return accumulator + infectedPatient.age;
  }, 0);

console.log(totalAgeOneStep);
