const names = ['Willy', 'Ivan', 'Ramiro'];
const eyeColor = ['azul', 'marron', 'azul'];
const height = ['bajo', 'alto', 'alto'];
const tattooed = [true, false, false];
const tip = [
  {
    height: 'alto',
  },
  {
    eyeColor: 'marron',
  },
  {
    tattooed: false,
  },
];

class Suspect {
  name;
  eyeColor;
  height;
  tattooed;
  #tip;
  constructor(name, eyeColor, height, tattooed, tip) {
    this.name = name;
    this.eyeColor = eyeColor;
    this.height = height;
    this.tattooed = tattooed;
    this.#tip = tip;
  }

  confess() {
    return this.#tip;
  }
}

class Detective {
  suspects;
  summary;
  constructor(suspects) {
    this.suspects = suspects;
  }
  investigate() {
    this.summary = this.suspects.map((suspect) => {
      return suspect.confess();
    });
    console.log(this.summary);
    const guilty = this.suspects.filter((suspect) => {
      return this.summary.every((tip) => {
        const tipKey = Object.keys(tip);
        return tip[tipKey] === suspect[tipKey];
      });
    });
    return guilty;
  }
}

const suspects = names.map((name, index) => {
  return new Suspect(name, eyeColor[index], height[index], tattooed[index], tip[index]);
});
const myDetective = new Detective(suspects);
console.log(myDetective.investigate());
console.log(suspects, myDetective);
