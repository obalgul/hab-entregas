'use strict';

const euros = 10;

async function changeToEurToDollarToYen(euros) {
  let response1 = await fetch('https://api.exchangerate-api.com/v4/latest/EUR');
  let responseData = await response1.json();
  let dollarData = responseData.rates.USD * euros;
  let libraData = responseData.rates.GBP * euros;
  let yenData = responseData.rates.JPY * euros;
  let dateChange = responseData.date;
  console.log(`A fecha de ${dateChange} el cambio de ${euros} €, son ${dollarData.toFixed(2)} Dolares`);
  console.log(`A fecha de ${dateChange} el cambio de ${euros} €, son ${libraData.toFixed(2)} Libras`);
  console.log(`A fecha de ${dateChange} el cambio de ${euros} €, son ${yenData.toFixed(2)} Yenes`);
}

console.log(changeToEurToDollarToYen(100));
