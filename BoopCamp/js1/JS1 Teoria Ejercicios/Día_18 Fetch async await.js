
JSON-fetch-async-await
 
@@ -71,10 +71,89 @@ fetch('https://dog.ceo/api/breeds/list/all')

# Ejercicios



## Ejemplo async await

```javascript
function promiseGenerator() {
  const randomNumber = Math.round(Math.random() * 5000);
  console.log('Esperando ', randomNumber);
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log('Resolviendo ', randomNumber);
      resolve(randomNumber);
    }, randomNumber);
  });
}

async function myFunction(params) {
  const myPromise1 = promiseGenerator();
  const myPromise2 = promiseGenerator();
  const myPromise3 = promiseGenerator();
  const value1 = await myPromise1;
  console.log('value1', value1);
  const value2 = await myPromise2;
  console.log('value2', value2);
  const value3 = await myPromise3;
  console.log('value3', value3);
}

myFunction();
```



## Fetch

Recuperar los datos de la casa Targaryen de juego de la API de Game of Thrones, de esta casa deberemos averiguar cual es el Lord actual (almacenado en currentLord) y recuperar sus datos, los del lord.

Del lord actual debemos sacar por la consola el nombre (name), y los titulos (titles) cada uno en una linea por consola.

Enlace para hacer fetch https://www.anapioficeandfire.com/api/houses/378
\ No newline at end of file
Enlace para hacer fetch https://www.anapioficeandfire.com/api/houses/378

```javascript
fetch('https://www.anapioficeandfire.com/api/houses/378').then((response) => {
  response.json().then((houseData) => {
    fetch(houseData.currentLord).then((response) => {
      response.json().then((lordData) => {
        console.log('Name', lordData.name);
        console.log('Titles:');
        for (const title of lordData.titles) {
          console.log(`- ${title}`);
        }
      });
    });
  });
}); 

async function getHouse() {
  const requestHouse = await fetch('https://www.anapioficeandfire.com/api/houses/378');
  const houseData = await requestHouse.json();
  const requestLord = await fetch(houseData.currentLord);
  const lordData = await requestLord.json();
  console.log(lordData.name);
  for (const title of lordData.titles) {
    console.log(`- ${title}`);
  }
}

getHouse();
```

## Ejemplo JSON stringify y parse

```javascript
const user = { a: {} };

const userCopy = JSON.parse(JSON.stringify(user));
console.log(JSON.stringify(user));
console.log(user.a === userCopy.a);

const stringifiedObect = JSON.stringify(user);
console.log(stringifiedObect);

const parsedObject = JSON.parse(stringifiedObect);
console.log(parsedObject); 
```
