#Continuación de arrays
Si queremos crear un array de 5 posiciones podemos hacerlo como sigue
const myArray = new Array(5);
Para llenar un array de un valor podemos hacerlo con fill
myArray.fill('*');
Lo llena de asteriscos
myArray.fill([]);
¡¡¡CUIDADO!!! todas las posiciones de myArray tienen apuntan a la misma instacia del mismo array.

Callbacks
Un callback es una función que pasaremos a otra fución como parámetro para que sea ejecutada posteriormente.
function uppercaseEcho(message) {
  console.log(message.toUpperCase());
}

function lowercaseEcho(message) {
  console.log(message.toLowerCase());
}

function changeCase(caseFunction) {
  const message = 'aAaAbBbBB';
  caseFunction(message);
}

changeCase(uppercaseEcho);
changeCase(lowercaseEcho);
Las métodos de array que veremos a continuación necesitan callbacks, en los callbacks describiremos lo que queremos que hagan

Más métodos de Array

Map
map: Transforma el array como describamos en la función de callback y nos devuelve un array nuevo. Deberemos hacer return del dato transformado para cada una de las posiciones en el callback
let datos = [1, 2, 3, 4, 5];

let datosMas5 = datos.map((dato) => dato + 5);

console.log(datosMas5);

Ejercicio diagonales
Hacer un array bidimensional y poner simbolos distintos en las dos diagonales
function createMatrix(size) {
  let matrix = new Array(size);
  for (let i = 0; i < matrix.length; i++) {
    matrix[i] = new Array(size).fill(' ');
  }
  return matrix;
}
function createDiagonals(matrix) {
  for (let i = 0; i < matrix.length; i++) {
    matrix[i][i] = '💩';
    matrix[matrix.length - 1 - i][i] = '💩';
  }
  return matrix;
}

let myMatrix = createMatrix(10);
let diagonals = createDiagonals(myMatrix);

console.log(diagonals);

Chrome devtools
Introducción a las devtools por Google

Ejemplos

For...of
let myArray = [9, 8, 7, 6];

for (let i = 0; i < myArray.length; i++) {
  console.log(myArray[i]);
}

for (const item of myArray) {
  console.log(item);
}

Callbacks
function myCallbackES(name) {
  console.log(`Hola ${name}`);
}
function myCallbackEN(name) {
  console.log(`Hi ${name}`);
}

function greet(callback) {
  let name = 'Iván';
  callback(name);
}

greet(myCallbackES);
greet(myCallbackEN);

Multiplicar por dos un array con y sin map
 const myMap = [9, 8, 7, 6];

function double(data) {
  let result = [...data];
  for (let i = 0; i < data.length; i++) {
    result[i] = result[i] * 2;
  }
  return result;
}

const doubleValue = double(myMap);

function multiply2(value, index) {
  return value * index;
}
const doubleWithMap = myMap.map(multiply2);
console.log(myMap, doubleWithMap);

Map con users
let users = [
  { name: 'Ivan', age: 35 },
  { name: 'David', age: 29 }
];

function getAge(user, index, data) {
  console.log(data);
  return user.age;
}

let ages = users.map(getAge);
console.log(users, ages);
