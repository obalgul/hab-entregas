"use strict";

let edadUsuario = parseInt(prompt("Introduce tu edad aquí"));
const mayorEdad = 18;
if (edadUsuario >= mayorEdad) {
  console.log(
    `Es mayor de edad, tiene ${mayorEdad} años, o más. Puede conducir`
  );
} else if (edadUsuario < mayorEdad) {
  console.log(
    `No es mayor de edad, tiene menos de ${mayorEdad} años. No puede conducir`
  );
}
