"use strict";
/**
 * EJERCICIOS
 */

const countries = [
  {
    code: "CN",
    name: "China",
    population: 1439,
    infected: 81999,
  },
  {
    code: "US",
    name: "Estados Unidos",
    population: 331,
    infected: 112468,
  },
  {
    code: "DE",
    name: "Alemania",
    population: 83,
    infected: 56202,
  },
  {
    code: "ES",
    name: "España",
    population: 46,
    infected: 72248,
  },
  {
    code: "UK",
    name: "Reino Unido",
    population: 67,
    infected: 17301,
  },
];

console.log("Número total de infectados");
const totalInfected = countries.reduce((accumulator, countries) => {
  return accumulator + countries.infected;
}, 0);

console.log(totalInfected);

console.log("Número total de sanos");
let totalPoblationCountries =
  countries.reduce((acculator, countries) => {
    return acculator + countries.population;
  }, 0) * 1000000;

let totalpoblationHealthy = totalPoblationCountries - totalInfected;

console.log(totalpoblationHealthy);

console.log(`Numero total de infectados en los países (del array de países)`);
let infectedName = countries.map((countries) => {
  return countries.name;
});

let infectedCountries = countries.map((countries) => {
  return countries.infected;
});

let totalInfectedCountry = [];
totalInfectedCountry = countries.map((name, index) => {
  return {
    name: infectedName[index],
    infected: infectedCountries[index],
  };
});
console.log(totalInfectedCountry);

console.log(`País con más infectados (del array de países)`);
let maximusNumber = 0;

for (let i = 0; i < infectedCountries.length; i++) {
  if (infectedCountries[i] > maximusNumber) {
    maximusNumber = infectedCountries[i];
  }
}

let plusCounterInfected = countries.filter((countries) => {
  return countries.infected === maximusNumber;
});
console.log(plusCounterInfected);

const plusCounterInfected = countries.map((country) => country.infected);
console.log(Math.max(...plusCounterInfected));

console.log(`Población en millones`);

const totalPopulation = countries.reduce((accumulator, countries) => {
  return accumulator + countries.population;
}, 0);

totalPopulation * 1000000;
console.log(totalPopulation);
