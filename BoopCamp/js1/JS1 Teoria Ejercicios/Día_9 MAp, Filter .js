Filter
Filter en MDN
Filter es otro método de Array que nos permitirá obtener un nuevo array en el que los únicos miembros serán los que cuplan la condición establecida en el callback
function isBigEnough(value) {
  /* Si se cumple la siguiente condición de la que hacemos return entonces la posición actual del array se queda en el nuevo array filtrado */
  return value >= 10;
}

var filtered = [12, 5, 8, 130, 44].filter(isBigEnough); // Muestra [12, 130, 44]

Ejercicios

Implementación de una función equivalente a map
function myMap(values, callback) {
  const newValues = [];
  for (let i = 0; i < values.length; i++) {
    newValues.push(callback(values[i], i, values));
  }
  return newValues;
}

function double(age) {
  return age * 2;
}
function multiplyByIndex(age, index) {
  return age * index;
}
console.log(myMap(myValues, multiplyByIndex));
console.log(myValues.map(multiplyByIndex));

Convertir un array de usuarios con mascota en un array de mascotas con map de distintas maneras
const users = [
  { name: 'Ivan', pet: 'perro' },
  { name: 'Chindasvinto', pet: 'tarantula' }
];
console.log(users);

function getPet(user) {
  return user.pet;
}

// Las siguientes maneras de hacer map dan el mismo resultado
console.log(users.map(getPet));
console.log(
  users.map(function(user) {
    return user.pet;
  })
);
console.log(
  users.map((user) => {
    return user.pet;
  })
);
	console.log(
  users.map((user) => {
    return user.pet;
  })
);
console.log(users.map((user) => user.pet));

Repaso map sin arrow functions
const ages = [7, 3, 9, 18];

function triple(age) {
  return age;
}

function multiplyByIndex(age, index) {
  return age * index;
}

function double(age) {
  return age * 2;
} 

console.log(ages);
const doubleAges = ages.map(double);
console.log(doubleAges);
console.log(ages.map(triple));
console.log(ages.map(multiplyByIndex));

Filter
const users = [
  { name: 'Ivan', pet: 'perro' },
  { name: 'Chindasvinto', pet: 'tarantula' },
  { name: 'Laura', pet: 'perro' }
];

console.log(
  users.filter((user) => {
    return user.pet === 'perro';
  })
);
