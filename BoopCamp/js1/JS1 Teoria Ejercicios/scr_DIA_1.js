'use strict';
//DIA 1

//let miVariable = 14;

//let patasPerro = 4;
//let patasArana = 8;

//let paraMisilesNucleares = 42;

//let Ivan; //Es una variable distinta a
//let ivan;

//Const aEstoNoPuedoCambiarleElNombre = 'Ivan';
//const aEstoNoPuedoCambiarleElNombre = 'Clodoveo'; //esto falla

//const TELEFONO_DE_CASA = '981999999';

/*
Las variables deben de ser
faciles de entender para nuestros
compañeros
*/

//let a;
//let valor;
//let chirimbolo;

/*
 las anteriores variables
no son recomendables por ser ambiguas
*/

// Metodos de ver informacion

//console.log('muestra informacion');
//console.warn('muestra un aviso');
//console.error('muestra un error');
//console.log('varios', 'datos', 'separados', 'por', 'coma');

//Tipos de valores

// Booleanos

//let estoEsUnBoolean = true;
//let elOtroValorBooleanoEs = false;

//null simboliza una referencia a un objeto no definido

//let estoEsNull = null;

//undefined simboliza una variable que no se le ha asignado un valor

//Number representa tanto numeros enteros como con punto flotante.

// Valores de Number especiales.

//let notANAmber = ' not a number' / 2;
//console.log(notANumber); //muestra NaN
//console.log(notANumber + 2); // Aunque el NaN se vista de seda NaN se queda

// string cadenas de texto

//let podemosDeclararlo = 'Así';
//let otraManeraEquivalente = 'esta';
//let laMasModernaEsEsta = `que añade funcionalidades a mayores`;
// enlas anteriores puedes interpolar valores almacenados de una vaeiable

//const miNombre = 'Ivan';
//console.log(`Me llamo ${miNombre}`);

// Con typeOf podemos ver el tipo de las distintas variables

// CODIGO
/*
let trueBoolean = true;
let falseBoolean = false;
let undefinedVariable;
let nullVariable = null;
let someNumber = 4;
let nanVariable = NaN;
let plusInfinity = Infinity;
let minusInfinity = -Infinity;
let aString = 'Con comillas simples o dobles los strings pueden ser de una línea';
let backtickString = `Puede
ocupar
varias
líneas`;
let someValue = aString / someNumber;
someValue = 5;
someValue = true;
someValue = 'Javascript es dinamicamente tipado';
//console.log(someValue);
*/
