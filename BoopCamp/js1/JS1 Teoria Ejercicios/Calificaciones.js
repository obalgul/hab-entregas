"use strict";

let nota = parseInt(prompt("Introduce tu nota aquí"));

if (Number(nota) === nota) {
  if (nota <= 3) {
    console.log("Muy Deficiente.");
  } else if (nota < 5) {
    console.log("Insuficiente.");
  } else if (nota < 6) {
    console.log("Suficiente.");
  } else if (nota < 7) {
    console.log("Bien.");
  } else if (nota < 9) {
    console.log("Notable.");
  } else if (nota <= 10) {
    console.log("Sobresaliente.");
  } else if (nota > 10) {
    console.log("Nota incorrecta");
  }
}
