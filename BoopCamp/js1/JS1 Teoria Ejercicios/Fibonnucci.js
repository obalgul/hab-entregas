'use strict';

function fibonnacci(position) {
  if (position === 0) {
    return 0;
  }
  if (position === 1) {
    return 1;
  }
  return fibonnacci(position - 1) + fibonnacci(position - 2);
}
console.time();
console.log(fibonnacci(5));
console.timeEnd();
