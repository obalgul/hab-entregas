'use strict';

//buscar números primos

function isPrime(numberToCheck) {
  for (let i = 2; i < numberToCheck; i++) {
    if (numberToCheck % i === 0) {
      return false;
    }
  }
  return true;
}

function printPrimes(limit) {
  for (let i = 2; i <= limit; i++) {
    if (isPrime(i) === true) {
      console.log(i);
    }
  }
}

function printFirstPrimes(limit) {
  let primesFound = 0;
  for (let i = 2; primesFound <= limit; i++) {
    if (isPrime(i)) {
      console.log(i);
      primesFound++;
    }
  }
}

printPrimes(100);
printFirstPrimes(100);
