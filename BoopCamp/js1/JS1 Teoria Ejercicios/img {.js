img {
  max-width: 100%;
}'use strict';
'use strict';
/**
 * EJERCICIOS
 */
const persons = [
  {
    name: 'Pedro',
    age: 35,
    country: 'ES',
    infected: true,
    pet: 'Troski',
  },
  {
    name: 'Elisabeth',
    age: 14,
    country: 'UK',
    infected: true,
    pet: 'Firulais',
  },
  {
    name: 'Pablo',
    age: 25,
    country: 'ES',
    infected: false,
    pet: 'Berritxu',
  },
  {
    name: 'Angela',
    age: 18,
    country: 'DE',
    infected: false,
    pet: 'Noodle',
  },
  {
    name: 'Boris',
    age: 50,
    country: 'UK',
    infected: true,
    pet: 'Leon',
  },
  {
    name: 'Donald',
    age: 69,
    country: 'US',
    infected: false,
    pet: 'Pence',
  },
];

const pets = [
  {
    name: 'Troski',
    animal: 'perro',
  },
  {
    name: 'Firulais',
    animal: 'perro',
  },
  {
    name: 'Berritxu',
    animal: 'loro',
  },
  {
    name: 'Noodle',
    animal: 'araña',
  },
  {
    name: 'Leon',
    animal: 'gato',
  },
  {
    name: 'Pence',
    animal: 'perro',
  },
];

const animals = [
  {
    name: 'perro',
    legs: 4,
  },
  {
    name: 'araña',
    legs: 8,
  },
  {
    name: 'gato',
    legs: 4,
  },
  {
    name: 'loro',
    legs: 2,
  },
  {
    name: 'gallina',
    legs: 2,
  },
];

console.log('Número total de infectados del array de personas');

const peopleInfected = persons.filter((persons) => persons.infected).length;
console.log(peopleInfected);

console.log('Array con nombre de todas las mascotas');

const petsName = persons.map((persons) => persons.pet);
console.log(petsName);

console.log('Array con las personas infectadas del array de personas');
const personsInfected = persons.filter((persons) => persons.infected).map((persons) => persons.name);
console.log(personsInfected);

console.log('Array de españoles con perro');

const countryPersons = persons.map((persons) => persons.country);
const personsName = persons.map((persons) => persons.name);
const petsAnimal = pets.map((pets) => pets.animal);
const personsWithDog = personsName
  .map((name, index) => {
    return { name: personsName[index], country: countryPersons[index], animal: petsAnimal[index] };
  })
  .filter((personsWithDog) => personsWithDog.country === 'ES')
  .filter((personsWithDog) => personsWithDog.animal === 'perro');
console.log(personsWithDog);

console.log('Número de personas sanas del array de personas');
const healtlyPersons = persons.filter((persons) => !persons.infected).length;
console.log(healtlyPersons);

console.log('Array con las personas y el objeto de la persona tiene a mayores todos los datos de su mascota');
const personsWithPet = persons.map((person) => {
  person.petData = pets.filter((pet) => {
    return pet.name === person.pet;
  });
  person.petData.animal = pets.filter((animal) => {
    return person.animal === animal.name;
  });
  return person;
});
console.log(personsWithPet);

console.log('A partir de las personas sacar el animal que tienen más personas como mascota');
const plusAnimal = persons
  .map((person) => {
    person.petData = pets.find((pet) => {
      return pet.name === person.pet;
    });
    person.petData.animalData = animals.find((animal) => {
      return person.petData.animal === animal.name;
    });
    return person;
  })
  .filter((plusAnimal) => {
    return plusAnimal.petData.animalData.legs === 4;
  })
  .map((plusAnimal) => {
    return plusAnimal.petData.animal;
  })
  .filter((plusAnimal) => {
    return (plusAnimal === 'perro') > (plusAnimal === 'gato');
  });
console.log(plusAnimal);

console.log('Número total de patas de las mascotas de las personas');
const totalPetsLengs = plusAnimal.reduce((acumulator, legs) => {
  return petData.animalData.legs;
}, 0);
console.log(totalPetsLengs);

console.log('Array con las personas que tienen animales de 4 patas');

//console.log(personsPetsFourLegs);

console.log(`A partir del string 'España' obtener un array de personas no infectadas de ese país`);

//console.log(spanishPersons);

console.log('Array de paises que tienen personas con loros como mascota');

//console.log(parrotCountry);

console.log('Numero de infectados totales (los del objeto del país) de los paises con mascotas de ocho patas');

//console.log(petSpiderInfected);
