Classes
Classes en MDN
Podemos crear nuevas clases de maneras distintas al igual que las funciones.
Class declaration
class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
}
Class expression
// Sin nombre
let Rectangle = class {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
};
console.log(Rectangle.name);
// salida: "Rectangle"

// con nombre
let Rectangle = class Rectangle2 {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
};
console.log(Rectangle.name);
// salida: "Rectangle2"
El método constructor se ejecuta cuando instanciamos la clase, si no declaramos constructor, el constructor por defecto será
constructor() {}
La palabra static declara métodos estáticos de una clase. Los métodos estáticos son llamados sin instanciar la clase y no pueden llamarse desde una instancia de la clase.
Atributos públicos de la clase
class Rectangle {
  height = 0;
  width;
  constructor(height, width) {    
    this.height = height;
    this.width = width;
  }
}
Atributos privados
class Rectangle {
  #height = 0;
  #width;
  constructor(height, width) {    
    this.#height = height;
    this.#width = width;
  }
}
En el ejercicio de classes de más abajo podemos ver como se extiende una clase. Al hacer esto podemos crear una nueva clase que aumenta o modifica la funcionalidad de su padre. Para ello nos podemos valer de la función super que hace referencia a la función con el mismo nombre del padre.
class Animal {

  constructor(name) {
    this.name = name;
  }
}

class Dog extends Animal {
  constructor(name) {
    super(name);
    this.name += ' the dog';
  }

Ejemplo atributo privado y método para obtenerlo
class User {
  #creditCard;

  constructor(creditCard) {
    this.#creditCard = creditCard;
  }

  getCreditCard() {
    return this.#creditCard;
  }
}

const myUser = new User(11111111);
console.log(myUser);
console.log(User);
console.log(myUser.getCreditCard()); 

Ejercicio zorros y gallinas con classes
class Animal {
  breed() {
    return new Animal();
  }
}

class Gallina extends Animal {
  breed() {
    return new Gallina();
  }
}

class Zorro extends Animal {
  breed() {
    return new Zorro();
  }

  eat(establo) {
    return establo.filter((animal) => {
      return animal instanceof Zorro;
    });
  }
}

class Granja {
  establo;

  constructor() {
    this.establo = Granja.buildEstablo();
  }

  closeDoor() {
    if (
      this.establo[0] instanceof Zorro === this.establo[1] instanceof Zorro &&
      this.establo[0] instanceof Gallina === this.establo[1] instanceof Gallina
    ) {
      this.establo.push(this.establo[0].breed());
      return this.establo;
    } else {
      const myFox = this.establo.find((animal) => {
        return animal instanceof Zorro;
      });
      this.establo = myFox.eat(this.establo);
      console.log('Animales Distintos');
      return this.establo;
    }
  }

  static buildEstablo() {
    return [new Zorro(), new Gallina()];
  }
}

Granja.buildEstablo();

const myFarm = new Granja();

console.log(myFarm);
console.log(myFarm.closeDoor());

Datos ejercicio sospechosos
En el ejercicio tendremos un array de sospechosos, cada uno con un nombre unos atributos físicos:

Altura
Color de ojos
Tatuajes
...

Cada sospechoso además tiene una pista de quien ha sido el culpable de un crimen, por ejemplo. Uno puede saber que tiene los ojos de un cierto color. Cada sospechoso sabe sólo un dato del culpable y el propio culpable dará una pista verdadera, no mentirá. El dato de la pista es privado.
Tendremos un detective que se encargará de interrogar a los sospechosos y con los datos recopilados buscará al culpable.
const names = ['Willy', 'Ivan', 'Ramiro'];
const eyeColor = ['azul', 'marron', 'azul'];
const height = ['bajo', 'alto', 'alto'];
const tattooed = [true, false, false];
const tip = [
  {
    height: 'alto'
  },
  {
    eyeColor: 'marron'
  },
  {
    tattooed: false
  }
]
