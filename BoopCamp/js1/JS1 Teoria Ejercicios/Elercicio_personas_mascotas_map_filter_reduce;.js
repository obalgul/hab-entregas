 'use strict';
/**
 * EJERCICIOS
 */
const persons = [
  {
    name: 'Pedro',
    age: 35,
    country: 'ES',
    infected: true,
    pet: 'Troski',
  },
  {
    name: 'Elisabeth',
    age: 14,
    country: 'UK',
    infected: true,
    pet: 'Firulais',
  },
  {
    name: 'Pablo',
    age: 25,
    country: 'ES',
    infected: false,
    pet: 'Berritxu',
  },
  {
    name: 'Angela',
    age: 18,
    country: 'DE',
    infected: false,
    pet: 'Noodle',
  },
  {
    name: 'Boris',
    age: 50,
    country: 'UK',
    infected: true,
    pet: 'Leon',
  },
  {
    name: 'Donald',
    age: 69,
    country: 'US',
    infected: false,
    pet: 'Pence',
  },
];

const pets = [
  {
    name: 'Troski',
    animal: 'perro',
  },
  {
    name: 'Firulais',
    animal: 'perro',
  },
  {
    name: 'Berritxu',
    animal: 'loro',
  },
  {
    name: 'Noodle',
    animal: 'araña',
  },
  {
    name: 'Leon',
    animal: 'gato',
  },
  {
    name: 'Pence',
    animal: 'perro',
  },
];

const animals = [
  {
    name: 'perro',
    legs: 4,
  },
  {
    name: 'araña',
    legs: 8,
  },
  {
    name: 'gato',
    legs: 4,
  },
  {
    name: 'loro',
    legs: 2,
  },
  {
    name: 'gallina',
    legs: 2,
  },
];

console.log('Número de total de infectados del array de personas');
const patientsInfected = persons.filter((persons) => {
  return persons.infected;
}).length;
console.log(patientsInfected);

console.log('Array con nombre de todas las mascotas');
const petsName = pets.map((pets) => {
  return pets.name;
});
console.log(petsName);

console.log('Array con las personas infectadas del array de personas');
const personsInfected = persons.filter((persons) => {
  return persons.infected;
});
console.log(personsInfected);

console.log('Array de españoles con perro');
const animalPets = pets.map((pets) => {
  return pets.animal;
});

const personsNames = persons.map((person) => {
  return persons.name;
});

const personsCountry = persons.map((persons) => {
  return persons.country;
});

const personsPets = persons.map((name, index) => {
  return {
    name: personsNames[index],
    country: personsCountry[index],
    pet: petsName[index],
    animal: animalPets[index],
  };
});

const pets.filter((person) => person.pet)
const nameOfDog = pets.filter((pet) => {
  cosnt
  return animalName.filter((animal) => {
    return animal === pet.name;
  })
})

const nameOfDog = pets.filter((pet) => pet.animal === 'perro').map ((pet)=>pet.name);
const spanishPersons = persons.filter((person) => person.country === 'ES');
const spanishWichDog1 = spanishPersons.filter((person) => {
  for (const dog of nameOfDogs) {
    return person.pet === dog;
  }
})

console.log(spanishWichDog1())

console.log(spanishWichDog);

console.log('Número de personas infectadas del array de personas');
const infectedNumber = persons.filter((persons) => {
  return persons.infected;
}).length;

console.log(infectedNumber);

console.log('Array con las personas y el objeto de la persona tiene a mayores todos los datos de su mascota');
const personsWithPet = persons.map((person) => {
  person.petData = pets.find((pet) => {
    return pet.name === person.pet;
  });
  person.petData.animalData = animals.find((animal) => {
    return person.petData.animal === animal.name;
  });
  return person;
});
console.log(personsWithPet);

const personsTotal = persons.map((person) => {
  return {
    ...person,
    pet: pets.find(pet => pet.name === person.pet)
  }
});
console.log(personsTotal());

console.log('A partir de las personas sacar el animal que tienen más personas como mascota');
const plusAnimal = persons
  .map((person) => {
    person.petData = pets.find((pet) => {
      return pet.name === person.pet;
    });
    person.petData.animalData = animals.find((animal) => {
      return person.petData.animal === animal.name;
    });
    return person;
  })
  .filter((plusAnimal) => {
    return plusAnimal.petData.animalData.legs === 4;
  })
  .map((plusAnimal) => {
    return plusAnimal.petData.animal;
  })
  .filter((plusAnimal) => {
    return (plusAnimal === 'perro') > (plusAnimal === 'gato');
  });

console.log(plusAnimal);

console.log('Número total de patas de las mascotas de las personas');
const totalPets = persons
  .map((person) => {
    person.petData = pets.find((pet) => {
      return pet.name === person.pet;
    });
    person.petData.animalData = animals.find((animal) => {
      return person.petData.animal === animal.name;
    });
    return person;
  })
  .reduce((accumulator, totalPets) => {
    return accumulator + totalPets.petData.animalData.legs;
  }, 0);
console.log(totalPets);

console.log('Array con las personas que tienen animales de 4 patas');
const personsPetsFourLegs = persons
  .map((person) => {
    person.petData = pets.find((pet) => {
      return pet.name === person.pet;
    });
    person.petData.animalData = animals.find((animal) => {
      return person.petData.animal === animal.name;
    });
    return person;
  })
  .filter((personsPetsFourLegs) => {
    return personsPetsFourLegs.petData.animalData.legs === 4;
  });
console.log(personsPetsFourLegs);

console.log(`A partir del string 'España' obtener un array de personas no infectadas de ese país`);
const spanishPersons = persons
  .filter((persons) => {
    return !persons.infected;
  })
  .filter((spanishPersons) => {
    return spanishPersons.country === 'ES';
  });

console.log(spanishPersons);

console.log('Array de paises que tienen personas con loros como mascota');
const peopleWorl = persons.map((persons) => {
  return persons.name;
});
const countryWorl = persons.map((persons) => {
  return persons.country;
});
const typePets = pets.map((pets) => {
  return pets.animal;
});

const parrotCountry = persons
  .map((name, index) => {
    return {
      country: countryWorl[index],
      name: peopleWorl[index],
      typePet: typePets[index],
    };
  })
  .filter((parrotCountry) => {
    return parrotCountry.typePet === 'loro';
  });

console.log(parrotCountry);

console.log('Numero de infectados totales (los del objeto del país) de los paises con mascotas de ocho patas');
const petSpiderInfected = persons
  .map((person) => {
    person.petData = pets.find((pet) => {
      return pet.name === person.pet;
    });
    person.petData.animalData = animals.find((animal) => {
      return person.petData.animal === animal.name;
    });
    return person;
  })
  .filter((petSpiderInfected) => {
    return petSpiderInfected.petData.animalData.name === 'araña';
  })
  .filter((petSpiderInfected) => {
    return petSpiderInfected.infected;
  });
console.log(petSpiderInfected);
