Ejercicio e-commerce
const itemNames = ['Camisa', 'Pantalon', 'Calcetines'];
const itemPrices = [13, 27, 100];

class Item {
  name;
  price;
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }
}

class CartItem extends Item {
  ammount = 1;

  constructor(item) {
    super(item.name, item.price);
  }

  increaseAmmount() {
    this.ammount++;
  }

  get total() {
    return this.ammount * this.price;
  }
}

class User {
  #shoppingCart = [];

  addToCart(item) {
    const itemFound = this.#shoppingCart.find((cartItem) => {
      return item.name === cartItem.name;
    });
    if (itemFound) {
      itemFound.increaseAmmount();
    } else {
      this.#shoppingCart.push(new CartItem(item));
    }
  }

  get cart() {
    return this.#shoppingCart;
  }

  fillCart(inventory, ammount) {
    for (let i = 0; i < ammount; i++) {
      const inventoryIndex = Math.floor(Math.random() * inventory.length);
      this.addToCart(inventory[inventoryIndex]);
    }
  }

  buy() {
    Shop.checkout(this.#shoppingCart);
  }
}

class Shop {
  static checkout(shoppingCart) {
    console.log(`Hack a Shop`);

    const initialTotals = { totalPrice: 0, totalUnits: 0 };
    const totals = shoppingCart.reduce((accumulator, currentItem) => {
      console.log(
        `${currentItem.name} ${currentItem.price}€ ${currentItem.ammount}U Total de item: ${currentItem.total}`
      );
      accumulator.totalPrice += currentItem.total;
      accumulator.totalUnits += currentItem.ammount;
      return accumulator;
    }, initialTotals);

    console.log(`Unidades totales ${totals.totalUnits}, Suelta ${totals.totalPrice}€`);
  }
}

const inventory = itemNames.map((name, index) => {
  return new Item(name, itemPrices[index]);
});

const myUser = new User();
myUser.fillCart(inventory, 50);
myUser.buy();


Ejercicio Fetch
Hacer una petición a la siguiente url que nos devuelve los datos de la casa Targaryen de Juego de Tronos:

https://www.anapioficeandfire.com/api/houses/378

Tendremos que hacer dos cosas independientes:

Recuperar los datos de tres personajes al azar del array de "swornMembers" de la primera petición. Estos datos se tienen que mostrar todos a la vez.
Recuperar los datos de otros tres personajes al azar, pero como Daenerys se ha levantado con mal pie, el primero en llegar del servidor lo mandaremos para el otro barrio diciendo su nombre y DRACARYS por la consola.

function getRandomPosition(data) {
  return Math.round(Math.random() * data.length);
}

async function getMembers() {
  const houseRequest = await fetch('https://www.anapioficeandfire.com/api/houses/378');
  const houseData = await houseRequest.json();
  const allMembers = [];
  for (let i = 0; i < 3; i++) {
    allMembers.push(fetch(houseData.swornMembers[getRandomPosition(houseData.swornMembers)]));
  }
  const allRequest = await Promise.all(allMembers);
  const allData = allRequest.map(async (request) => {
    return await request.json();
  });
  for (const member of allData) {
    const memberData = await member;
    console.log(memberData.name);
  }
}

getMembers();
