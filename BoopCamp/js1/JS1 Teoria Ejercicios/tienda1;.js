'use strict';

const itemNames = ['Camisa', 'Pantalon', 'Calcetines'];
const itemPrices = [13, 27, 100];

class Item {
  itemType;
  prize;

  constructor(itemType, prize) {
    this.itemType = itemType;
    this.prize = prize;
  }
}

const inventary = itemNames.map((name, index) => {
  return new Item(name, itemPrices[index]);
});

class User {
  #carrito = [];
  addItems(item) {
    let foundItem = this.#carrito.find((element) => {
      element, item;
      return element.itemData.itemType === item.itemType;
    });
    if (foundItem) {
      foundItem.ammount++;
    } else {
      this.#carrito.push({ itemData: item, ammount: 1 });
    }
  }
  leerCarrito() {
    return this.#carrito;
  }
}

class Shop extends User {
  leerCarrito() {
    this.#carrito;
  }
}

const myUser = new User();
myUser.addItems(inventary[0]);
myUser.addItems(inventary[0]);
myUser.addItems(inventary[0]);
myUser.addItems(inventary[1]);
myUser.addItems(inventary[2]);
myUser.addItems(inventary[2]);
myUser.addItems(inventary[2]);
myUser.addItems(inventary[2]);
myUser.addItems(inventary[2]);
const myShop = new Shop();
console.log(myShop);
