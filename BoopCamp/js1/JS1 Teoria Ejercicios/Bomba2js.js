'use strict';

// Generamos una contraseña entre 1 y 5.
const bombPassword = Math.round(Math.random() * 4 + 1);

// Función para desactivar la bomba.
function deactivatedBomb(password) {
  // password = bombPassword;
  // Le damos al usuario 5 intentos.
  for (let i = 0; i < 5; i++) {
    const userPassword = parseInt(prompt('Dime un número: '));
    console.log(`Intento nº${i + 1}`);
    // Si acierta se desactiva la bomba.
    if (userPassword === password) {
      return true;
    }
  }
  // Si tras 5 intentos fracasa BOOOM!
  return false;
}

// Llamamos a la función
if (deactivatedBomb(bombPassword) === true) {
  console.log('Has desactivado la bomba!');
} else {
  console.log('BOOOOOOMMMMMM!');
}
