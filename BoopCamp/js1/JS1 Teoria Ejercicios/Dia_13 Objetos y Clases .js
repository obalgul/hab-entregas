POO
Programación orientada a objetos en 
Artículo sobre principios de programación orientada a objetos

Classes
Classes en MDN
Podemos crear nuevas clases de maneras distintas al igual que las funciones.
Class declaration
class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
}
Class expression
// Sin nombre
let Rectangle = class {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
};
console.log(Rectangle.name);
// salida: "Rectangle"

// con nombre
let Rectangle = class Rectangle2 {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
};
console.log(Rectangle.name);
// salida: "Rectangle2"
El método constructor se ejecuta cuando instanciamos la clase, si no declaramos constructor, el constructor por defecto será
constructor() {}
La palabra static declara métodos estáticos de una clase. Los métodos estáticos son llamados sin instanciar la clase y no pueden llamarse desde una instancia de la clase.
Atributos públicos de la clase
class Rectangle {
  height = 0;
  width;
  constructor(height, width) {    
    this.height = height;
    this.width = width;
  }
}
Atributos privados
class Rectangle {
  #height = 0;
  #width;
  constructor(height, width) {    
    this.#height = height;
    this.#width = width;
  }
}
En el ejercicio de classes de más abajo podemos ver como se extiende una clase. Al hacer esto podemos crear una nueva clase que aumenta o modifica la funcionalidad de su padre. Para ello nos podemos valer de la función super que hace referencia a la función con el mismo nombre del padre.
class Animal {

  constructor(name) {
    this.name = name;
  }
}

class Dog extends Animal {
  constructor(name) {
    super(name);
    this.name += ' the dog';
  }

Ejercicio zorros y gallinas con funciones
//paso 1 los zorros se comen las gallinas
const establo = [{ animal: "zorro" }, { animal: "gallina" }];

function granja(establo) {
  if (establo[0].animal !== establo[1].animal) {
    return establo.filter((item) => {
      return item.animal === "zorro";
    });
  } else {
    console.log("Animales iguales");
  }
}

console.log(establo, granja(establo));
// paso 2 every para comprobar si tenemos que criar

const establo = [{ animal: "gallina" }, { animal: "zorro" }];

function granja(establo) {
  const sameAnimal = establo.every((item) => {
    return item.animal === establo[0].animal;
  });
  if (sameAnimal) {
    console.log("Animales iguales");
  } else {
    return establo.filter((item) => {
      return item.animal === "zorro";
    });
  }
}

console.log(establo, granja(establo));

// paso 3 con if else para criar

const establo = [{ animal: "gallina" }, { animal: "gallina" }];

function granja(establo) {
  const sameAnimal = establo.every((item) => {
    return item.animal === establo[0].animal;
  });

  if (sameAnimal) {
    let establoCopy = [...establo];
    if (establo[0].animal === "zorro") {
      establoCopy.push({ animal: "zorro" });
    } else {
      establoCopy.push({ animal: "gallina" });
    }
    return establoCopy;
  } else {
    return establo.filter((item) => {
      return item.animal === "zorro";
    });
  }
}

console.log(establo, granja(establo));

//paso 4 con literal de la cria
const establo = [{ animal: "zorro" }, { animal: "zorro" }];

function granja(establo) {
  const sameAnimal = establo.every((item) => {
    return item.animal === establo[0].animal;
  });

  if (sameAnimal) {
    let establoCopy = [...establo];
    establoCopy.push({ animal: establoCopy[0].animal });

    return establoCopy;
  } else {
    return establo.filter((item) => {
      return item.animal === "zorro";
    });
  }
}

console.log(establo, granja(establo));

//paso 5 con push de la cria
const establo = [{ animal: "zorro" }, { animal: "zorro" }];

function granja(establo) {
  const sameAnimal = establo.every((item) => {
    return item.animal === establo[0].animal;
  });

  if (sameAnimal) {
    let establoCopy = [...establo];
    establoCopy.push({ ...establoCopy[0] });

    return establoCopy;
  } else {
    return establo.filter((item) => {
      return item.animal === "zorro";
    });
  }
}

console.log(establo, granja(establo));

// Paso final

const establo = [{ animal: 'zorro' }, { animal: 'zorro' }];

function granja(establo) {
  const sameAnimal = establo.every((item) => {
    return item.animal === establo[0].animal;
  });

  if (sameAnimal) {
    let establoCopy = [...establo, { ...establo[0] }];
    return establoCopy;
  } else {
    return establo.filter((item) => {
      return item.animal === 'zorro';
    });
  }
}

console.log(establo, granja(establo));

Ejercicio zorros y gallinas con Class
class Animal {}

class Gallina extends Animal {
  breed() {
    return new Gallina();
  }
}

class Zorro extends Animal {
  breed() {
    return new Zorro();
  }
  hunt(establo) {
    return establo.filter((animal) => {
      return animal.constructor.name === 'Zorro';
    });
  }
}

class Granja {
  establo;
  constructor(establo) {
    this.establo = establo;
  }
  closeDoors() {
    if (this.establo[0].constructor.name === this.establo[1].constructor.name) {
      this.establo.push(this.establo[0].breed());
    } else {
      const myFox = this.establo.find((animal) => {
        return animal.constructor.name === 'Zorro';
      });
      this.establo = myFox.hunt(this.establo);
    }
  }
}

const establo = [new Zorro(), new Zorro()];

const myFarm = new Granja(establo);
myFarm.closeDoors();
console.log(establo, myFarm);

Ejemplo de animales con Class
class Animal {
  constructor(patas, ojos) {
    this.patas = patas;
    this.ojos = ojos;
  }
  tellLegs() {
    console.log(this.patas);
  }
}

class Perro extends Animal {
  constructor(name, patas) {
    super(patas, 2);
    this.name = name;
  }

  speak() {
    console.log('GUAU', this.name);
  }
}

class Gato extends Animal {
  constructor(name) {
    super(4, 2);
    this.name = name;
  }

  speak() {
    console.log('MIAU', this.name);
  }
}

class Arana extends Animal {
  constructor() {
    super(8, 8);
  }

  tellLegs() {
    console.log('Tengo', this.patas);
  }
}

const myDog = new Perro('Troski', 4);
const myOtherDog = new Perro('Firulais', 3);

const myCat = new Gato('Friskis');

const mySpyder = new Arana();

console.log(myDog, myOtherDog, myCat, mySpyder);

myOtherDog.speak();
myCat.speak();

myOtherDog.tellLegs();

mySpyder.tellLegs();

Equivalente a class con funciones
function perro(name) {
  this.name = name;
  this.speak = function () {
    console.log(this.name);
  };
}
const troski = new perro('Troski');
const firulais = new perro('Firulais');

troski.speak();
