"use strict";

let names = ["Adane", "Arantxa", "Azucena", "Xabi", "Jon"];
let ages = [21, 22, 33, 18, 37];
let infected = [false, true, false, true, true];

console.log(names, ages, infected);

const patients = names.map((name, index) => {
  return { name: name, age: ages[index], infected: infected[index] };
});

console.log(patients);
const everyInfected = patients.filter(patients => {
  return patients.infected;
});

console.log(everyInfected);
const infectedAges = everyInfected.reduce((accumuleitor, everyInfected) => {
  return accumuleitor + everyInfected.age;
}, 0);
console.log(infectedAges);
