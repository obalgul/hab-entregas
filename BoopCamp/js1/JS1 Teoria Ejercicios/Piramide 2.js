'use strict';

// Piramide 2
function buildFloor(size, brick) {
  let floor = '';
  for (let i = 0; i < size; i++) {
    floor = floor + brick;
  }
  return floor;
}

function printPyramid(height) {
  for (let i = 0; i < height; i++) {
    console.log(buildFloor(height - (i + 1), ' ') + buildFloor(i + 1, '*'));
  }
}

printPyramid(5);
