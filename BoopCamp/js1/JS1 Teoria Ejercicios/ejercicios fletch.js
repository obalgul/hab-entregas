'use strict';
/*## Fetch

Recuperar los datos de la casa Targaryen de juego de la API de Game of Thrones, de esta casa deberemos averiguar cual es el Lord actual(almacenado en currentLord) y recuperar sus datos.
Recuperar los datos de la casa Targaryen de juego de la API de Game of Thrones, de esta casa deberemos averiguar cual es el Lord actual(almacenado en currentLord) y recuperar sus datos, los del lord.

Del lord actual debemos sacar por la consola el nombre(name), y los titulos(titles) cada uno en una linea por consola.

Enlace para hacer fetch https://anapioficeandfire.com/api/characters/1303
Enlace para hacer fetch https://www.anapioficeandfire.com/api/houses/378
*/

async function getHouse() {
  const requestHouse = await fetch('https://www.anapioficeandfire.com/api/houses/378');
  const houseData = await requestHouse.json();
  const requestLord = await fetch(houseData.currentLord);
  const lordData = await requestLord.json();
  console.log(lordData.name);
  for (const title of lordData.titles) {
    console.log(`- ${title}`);
  }
}
getHouse();

/*
- Recuperar los datos de tres personajes al azar del array de "swornMembers" de la primera petición.Estos datos se tienen que mostrar todos a la vez.
- Recuperar los datos de otros tres personajes al azar, pero como Daenerys se ha levantado con mal pie, el primero en llegar del servidor lo mandaremos para el otro barrio diciendo su nombre y DRACARYS por la consola.
- Recuperar los datos de otros tres personajes al azar, pero como Daenerys se ha levantado con mal pie, el primero en llegar del servidor lo mandaremos para el otro barrio diciendo su nombre y DRACARYS por la consola.
*/

function getRandomPosition(data) {
  return Math.round(Math.random() * data.length);
}

async function getMembers() {
  const houseRequest = await fetch('https://www.anapioficeandfire.com/api/houses/378');
  const houseData = await houseRequest.json();
  const allMembers = [];
  for (let i = 0; i < 3; i++) {
    allMembers.push(fetch(houseData.swornMembers[getRandomPosition(houseData.swornMembers)]));
  }
  const allRequest = await Promise.all(allMembers);
  const allData = allRequest.map(async (request) => {
    return await request.json();
  });
  for (const member of allData) {
    const memberData = await member;
    console.log(memberData.name);
  }
}

getMembers();
