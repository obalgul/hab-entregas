Repaso
Este es el repaso de lo que vimos ayer por videoconferencia. Tened en cuenta que al ir explicando he ido editando el código sobre la marcha, así que si algo no os funciona avisadme y lo corrijo lo más rápido que pueda 😊
'use strict';

// Comentarios

// Un comentario de una linea

/*
  Estos comentarios
  pueden ocupar 
  varias lineas
*/

/**
 * Variables
 */
let myVariable;
const myConstant = 5;
myVariable = true;

console.log('Esto saca un mensaje por consola', myConstant, myVariable);

/**
 * Tipos de datos
 */

let myBoolean = false;
let myNull = null;
let myUndefined = undefined;
let myNumber = 5;
let otherNumericValues = NaN;
let myString = 'Esto es un String';
let myStringWithBackticks = `myNumber contiene ${myNumber}`;
console.log(typeof myBoolean);

/**
 * Operadores
 */

const numericOperators = 1 + 2 - (((3 * 4) / 5) % 8) ** 2;
const convertirANumero = +'5';

let counter = 0;
counter++;
counter--;
console.log(counter);

let asignacion = 'algo';
counter = counter + 2;
counter += 2; // Es equivalente a la anterior linea
counter -= 3;

const concatenacionDeStrings = 'Un string ' + 'Otro string';
console.log(concatenacionDeStrings);

console.log(5 !== 4);

'2' + 2 === '22';
2 + 2 + '2' === '42';

// Comparación: menor, mayor...
4 < 5 === true;
5 <= 5 === true;
5 > 4 === true;
5 >= 5 === true;
4 > 5 === false;

// AND
true && true === true;
true && false === false;
false && true === false;
false && false === false;

// OR
true || true === true;
true || false === true;
false || true === true;
false || false === false;

// NOT
!true === false;
!false === true;

console.log(!!'false');

/**
 * if...else
 */
const myName = 'Iván';

if (myName === 'Iván') {
  console.log('Se cumple la primera condicion');
} else if (myName === 'Lucía') {
  console.log('Se cumple la segunda condicion');
} else {
  console.log('No se cumplen las anteriores');
}

/**
 * Operador ternario
 */
const isIvan = myName === 'Iván' ? true : false;

/**
 * switch
 */

switch (myName) {
  case 'Iván':
    console.log('Se llama Iván');
    break;

  case 'Lucía':
    console.log('Se llama Lucía');
    break;

  default:
    console.log('No se como se llama');
    break;
}

/**
 * funciones
 */
function greet(name, age) {
  return `Hola ${name}, tienes ${age} años`;
}

let userName = 'David';
let userAge = 30;

greet(userName, userAge);
let saludo = greet('Ivan', 34);
console.log(saludo);
console.log(greet('Ana', 20));
console.log(Math.random());

/**
 * Bucles
 */
let contador = 0;
while (contador < 5) {
  console.log('contador', contador);
  contador++;
}

let contador = 0;
do {
  console.log('contador', contador);
  contador++;
} while (contador < 5);

for (let contador = 0; contador < 5; contador++) {
  console.log('contador', contador);
}

/**
 * Objetos
 */
let user = {
  name: 'Antón',
  age: 17,
  address: {
    line1: 'C/ Colombia',
    line2: 'numero 14'
  }
};

user.name = 'María';
user['age'] = 23;

console.log(user, user.name, user['age']);
console.log(Object.keys(user));
console.log(Object.values(user));

let userCopy = { ...user, test: false };
console.log(userCopy);

/**
 * Arrays
 */
let myArray = [9, 8, 7, 6];
let otherArray = ['a', 'b', 'c'];
myArray.push(5);
let myArrayCopy = [...myArray, ...otherArray];
let emptyArray = [];
console.log(myArray, myArrayCopy);

console.log(myArray, myArray.length, emptyArray.length);

if (emptyArray) {
  console.log('es truthy');
} else {
  console.log('es falsy');
}
for (let position of myArrayCopy) {
  console.log(position);
}

/**
 * Arrow functions
 */

// Las dos siguientes funciones son equivalentes
let myArrow = (operando1, operando2) => {
  return operando1 + operando2;
};
let myArrowImplicitReturn = (operando1, operando2) => operando1 + operando2;
console.log(myArrow(2, 3));
console.log(myArrowImplicitReturn(2, 3));

/**
 * Callbacks
 */
function withCallback(callbackFunction) {
  callbackFunction();
}

withCallback(() => {
  console.log('HEY');
});

/**
 * Map
 */
const numbers = [9, 8, 7, 6];
const double = numbers.map((value) => {
  return value * 2;
});
const multipliedByIndex = numbers.map((value, index) => value * index);

console.log(numbers, double, multipliedByIndex);

const users = [
  { name: 'Ivan', age: 35 },
  { name: 'Ana', age: 15 }
];
const ages = users.map((user) => {
  return user.age;
});

console.log(users, ages);

/**
 * Filter
 */
let adultUsers = users.filter((user) => {
  return user.age >= 18;
});
console.log(users, adultUsers);

// Ejemplo Filter
const persons = [
  { name: 'Ivan', pet: { animal: 'dog', petName: 'Vega' } },
  { name: 'David', pet: { animal: 'cat', petName: 'Friskis' } },
  { name: 'Ana', pet: { animal: 'dog', petName: 'Firulais' } }
];

const dogPersons = persons.filter((person) => {
  return person.pet.animal === 'dog';
});

const dogs = persons
  .filter((person) => {
    return person.pet.animal === 'dog';
  })
  .map((person) => {
    return person.pet;
  });

console.log(persons, dogPersons, dogs);

// Ejemplo objeto
const commerceUser = {
  name: 'Ivan',
  email: 'fake@fake.com',
  address: {
    line1: 'C/ Peru',
    line2: 'n 15',
    CP: 99999
  }
};
