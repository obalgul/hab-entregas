'use strict';
/*## Fetch

Recuperar los datos de la casa Targaryen de juego de la API de Game of Thrones, de esta casa deberemos averiguar cual es el Lord actual(almacenado en currentLord) y recuperar sus datos.
Recuperar los datos de la casa Targaryen de juego de la API de Game of Thrones, de esta casa deberemos averiguar cual es el Lord actual(almacenado en currentLord) y recuperar sus datos, los del lord.

Del lord actual debemos sacar por la consola el nombre(name), y los titulos(titles) cada uno en una linea por consola.

Enlace para hacer fetch https://anapioficeandfire.com/api/characters/1303
Enlace para hacer fetch https://www.anapioficeandfire.com/api/houses/378
*/

async function getHouse() {
  const requestHouse = await fetch('https://www.anapioficeandfire.com/api/houses/378');
  const houseData = await requestHouse.json();
  const requestLord = await fetch(houseData.currentLord);
  const lordData = await requestLord.json();
  console.log(lordData.name);
  for (const title of lordData.title) {
    console.log(`- ${title}`);
  }
}
getHouse();
