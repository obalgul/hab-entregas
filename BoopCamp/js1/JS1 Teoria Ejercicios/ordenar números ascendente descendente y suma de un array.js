'use strict';

const ages = [22, 37, 21, 32, 63, 50];

const ordesAgesAsc = ages.sort((a, b) => {
  return a - b;
});

console.log(ordesAgesAsc);

const ordesAgesDes = ages.sort((a, b) => {
  return b - a;
});
console.log(ordesAgesDes);

const totalAges = ages.reduce((accumulator, number) => {
  return accumulator + number;
});

console.log(totalAges);
