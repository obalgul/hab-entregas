'use strict';

'use strict';
/**
 * EJERCICIOS
 */

const countries = [
  {
    code: 'CN',
    name: 'China',
    population: 1439,
    infected: 81999,
  },
  {
    code: 'US',
    name: 'Estados Unidos',
    population: 331,
    infected: 112468,
  },
  {
    code: 'DE',
    name: 'Alemania',
    population: 83,
    infected: 56202,
  },
  {
    code: 'ES',
    name: 'España',
    population: 46,
    infected: 72248,
  },
  {
    code: 'UK',
    name: 'Reino Unido',
    population: 67,
    infected: 17301,
  },
];

console.log('Número total de infectados');
const totalInfected = countries.reduce((accumulator, countries) => {
  return accumulator + countries.infected;
}, 0);

console.log(totalInfected);

console.log('Número total de sanos');
let totalPoblationCountries = countries.reduce((acculator, countries) => {
  return acculator + countries.population;
}, 0);

totalPoblationCountries = totalPoblationCountries * 1000000;

let totalpoblationHealthy = totalPoblationCountries - totalInfected;

console.log(totalpoblationHealthy);

console.log(`Numero total de infectados en los países (del array de países)`);
let infectedName = countries.map((countries) => {
  return countries.name;
});

let infectedCountries = countries.map((countries) => {
  return countries.infected;
});

let totalInfectedCountry = [];
totalInfectedCountry = countries.map((name, index) => {
  return {
    name: infectedName[index],
    infected: infectedCountries[index],
  };
});
console.log(totalInfectedCountry);

console.log(`País con más infectados (del array de países)`);
let maximusNumber = 0;

for (let i = 0; i < infectedCountries.length; i++) {
  if (infectedCountries[i] > maximusNumber) {
    maximusNumber = infectedCountries[i];
  }
}

let plusCounterInfected = countries.filter((countries) => {
  return countries.infected === maximusNumber;
});
console.log(plusCounterInfected);

console.log(`Población en millones`);

const totalPopulation = countries.reduce((accumulator, countries) => {
  return accumulator + countries.population;
}, 0);

totalPopulation * 1000000;
console.log(totalPopulation);

console.log(`Actualizar los infectedos de esta lista a los ya existentes `);

/*China = 12576; EEUU =157939;
 Alemania = 64056; España 24865;
  Reino Unido = 93584 */

const addInfected = [12576, 157939, 64056, 24865, 93538];

const totalNewInfected = infectedCountries.map((infected, index) => {
  return infectedCountries[index] + addInfected[index];
});
let totalNewInfectedCountry = [];
totalNewInfectedCountry = countries.map((name, index) => {
  return {
    name: infectedName[index],
    infected: totalNewInfected[index],
  };
});

console.log(totalNewInfectedCountry);

console.log(`Sacar por orden de mayor a menor los infectados por paises`);

const sortedInfected = [...totalNewInfectedCountry];
sortedInfected.sort((a, b) => b.infected - a.infected);
console.log(sortedInfected, totalNewInfectedCountry);
