'use strict';

function chronos(time) {
  let passTime = 1;
  const intervalId = setInterval(() => {
    if (passTime < time) {
      console.log(`RING RING Campana y se acabo.`);
      clearInterval(intervalId);
    } else {
      console.log(`Ha pasado el ${passTime} segundos.`);
      passTime++;
    }
  }, 1000);
}

console.log(chronos(7));
