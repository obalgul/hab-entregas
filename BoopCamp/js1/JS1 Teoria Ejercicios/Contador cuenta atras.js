'use strict';

function chrono(time) {
  let passtime = time;
  let finishTime = 0;
  const setIntervalId = setInterval(() => {
    if (passtime < finishTime) {
      console.log('Se acabo el tiempo');
      clearInterval(setIntervalId);
    } else {
      console.log(`Han pasado ${passtime}`);
      --passtime;
    }
  }, 1000);
}

chrono(10);
