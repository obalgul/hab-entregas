'use strict';
function buscarprimo(posiblePrimo) {
  for (let i = 2; i < posiblePrimo; i++) {
    if (posiblePrimo % i === 0) {
      return false;
    }
  }
  return true;
}

console.log(buscarprimo(13));

function imprimirPrimo() {
  for (let i = 2; i < 50; i++) {
    if (buscarprimo(i)) {
      console.log(i);
    }
  }
}
console.log(imprimirPrimo());
