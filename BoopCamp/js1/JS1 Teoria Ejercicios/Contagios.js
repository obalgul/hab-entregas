'use strict';

const pacientes = [true, false, true, false, false, false, true, true];

function infectados(pacientes) {
  let infectados = [...pacientes];
  for (let i = 0; i < pacientes.length; i++) {
    if (pacientes[i]) {
      if (i - 1 > 0) {
        infectados[i - 1] = true;
      }
      if (i + 1 < infectados.length) {
        infectados[i + 1] = true;
      }
    }
  }

  return infectados;
}

console.log(pacientes, infectados(pacientes));
