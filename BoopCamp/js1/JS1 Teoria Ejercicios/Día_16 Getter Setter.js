Date
Date en MDN
Con Date podremos realizar operaciones con fechas.
let today = new Date()
let birthday = new Date('December 17, 1995 03:24:00')
let birthday = new Date('1995-12-17T03:24:00')
let birthday = new Date(1995, 11, 17)           
let birthday = new Date(1995, 11, 17, 3, 24, 0)


// Si queremos medir el tiempo que lleva hacer algo podemos ver la diferencia entre dos fechas
let start = new Date()

// The event to time goes here:
funcionQueTarda()
let end = Date.now()
let elapsed = end - start // En milisegundos

Destructuring
Destructuring en MDN
Destructuring en javascript.info
La asignación por destructuring nos permite asignación de valores de una forma más compacta
let userData = ['Perico', 'Palotes']

let [firstName, surname] = userData;

console.log(firstName);
console.log(surname);
Podemos ignorar elementos dejando huecos
let userData = ['Perico', 'de los', 'Palotes',]

let [firstName, , surname] = userData
// No asignamos el valor de 'de los'
Con la sintaxis de rest podemos guardar el resto de valores que no hayamos asignado con destructuring. Estos quedarán en un array.
let [name1, name2, ...rest] = ['Ivan', 'Roberto', 'David', 'Rosa', 'Gonzalo'];

console.log(name1);
console.log(name2);

console.log(rest); // ['David', 'Rosa', 'Gonzalo']
También podemos asignar valores por defecto en caso de que no se asigne ningún valor a la variable con destructuring
// default values
let [name = 'Sin nombre', surname = 'sin apellido'] = ['Ivan'];

console.log(name); // Ivan
console.log(surname); // sin apellido
Tambien podemos hacer asignaciones por destructuring de objetos, en este caso el destructuring asignará a la variable el atributo del objeto con el mismo nombre.
let user = {
	name: 'Ivan',
	pet: 'dog',
	city: 'A Coruña'
};

let {name, pet, city} = user;

console.log(name); // Ivan
console.log(pet); // dog
console.log(city); // A Coruña
Al igual que con los arrays podemos usar el rest para almacenar los valores que no hemos asignado. Los guardará como un objeto.
let user = {
	name: 'Ivan',
	pet: 'dog',
	city: 'A Coruña'
};

let {name, ...rest} = user;

try...catch
try...catch en MDN
try...catch especifica un bloque de código que se intentará ejecutar y que se deberá hacer en caso de que salte una excepción.
try {
  nonExistentFunction();
}
catch(error) {
  console.error(error);
}
Con throw podemos lanzar nuestra propia excepción.
try {
  throw 'myException'; // genera una excepción
}
catch (e) {
  console.log(e);
}
Finally se ejecuta despues de que try y catch terminen de ejecutarse
try {
  doThis;
}
finally {
  doOtherThing(); // Siempre se ejecuta después del try
}

Ejercicios

Ejercicio métodos de Array
// A partir de las personas sacar el animal que tienen más personas como mascota
// Mapeamos las personas para quedarnos con el animal que es su mascota
const myPets = persons.map((person) => {
  // Buscamos su mascota en el array de mascotas
  return pets.find((pet) => {
    return person.pet === pet.name;
  }).animal;
  // De la mascota nos quedamos solo con el animal que es, el string
});

/**
 * En este reduce vamos a hacer un array, que recontará cuantos animales de cada tipo hay
 * Ejemplo de estructura de datos:
 * [{name:'tigre',ammount:2},.......]
 */
const animalRanking = myPets.reduce((accumulator, pet) => {
  // Miramos si nuestra estructura de datos ya tiene insertado este animal
  // Buscamos su posicion
  const petIndex = accumulator.findIndex((item) => {
    return item.name === pet;
  });
  // Si este animal ya estaba insertado
  if (petIndex > -1) {
    // E incrementamos su cantidad
    accumulator[petIndex].ammount++;
  } else {
    // Sino creamos esta entrada en el array
    accumulator.push({ name: pet, ammount: 1 });
  }
  return accumulator;
}, []);

// Vamos a hacer un reduce en el que devolveremos solo el animal mas repetido
const mostPopularAnimal = animalRanking.reduce((biggest, item) => {
  // Comparamos si el acumulador tiene mas cantidad que el elemento que queremos comparar
  return biggest.ammount > item.ammount ? biggest : item;
});

console.log(mostPopularAnimal);

Generar array de sospechosos
const names = ['Willy', 'Ivan', 'Ramiro'];
const eyeColor = ['azul', 'marron', 'azul'];
const height = ['bajo', 'alto', 'alto'];
const tattooed = [true, false, false];
const tip = [
  {
    height: 'alto',
  },
  {
    eyeColor: 'marron',
  },
  {
    tattooed: false,
  },
];

class Suspect {
  name;
  eyeColor;
  height;
  tattooed;
  #tip;
  constructor(suspectData) {
    this.name = suspectData.name;
    this.eyeColor = suspectData.eyeColor;
    this.height = suspectData.height;
    this.tattooed = suspectData.tattooed;
    this.#tip = suspectData.tip;
  }
}

const mySuspect = new Suspect({
  name: names[0],
  eyeColor: eyeColor[0],
  height: height[0],
  tattoed: tattooed[0],
  tip: tip[0],
});

const suspects = names.map((name, index) => {
  return new Suspect({
    name: name,
    eyeColor: eyeColor[index],
    height: height[index],
    tattoed: tattooed[index],
    tip: tip[index],
  });
});

console.log(suspects);

Ejemplo try...catch
const user = { name: 'Ivan', pet: 'perro', city: 'La Coru' };

const { name, lastName, ...rest } = user;
try {
  console.log(name, lastName.length, rest);
} catch (e) {
  console.log(e);
}

console.log('Esto se ejecuta');

# Enunciado e-commerce


Crear un falso e-commerce. Por un lado tenemos todos los artículos que forman el stock de la tienda. Tendremos también un usuario que añade cosas a su carrito, que es privado. Los artículos cuando los metemos al carrito los convertimos un tipo de dato que guarda las unidades que tiene el usuario de dicho artículo. La tienda es la encargada de imprimir un ticket por la consola.

	const itemNames = ['Camisa', 'Pantalon', 'Calcetines'];
	const itemPrices = [13, 27, 100];
	const items = Item.buildItems(itemNames, itemPrices);
	const myUser = new User();

