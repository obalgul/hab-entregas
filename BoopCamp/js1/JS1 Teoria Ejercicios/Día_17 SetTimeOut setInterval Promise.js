setTimeout
setTimeout en MDN
Con setTimeout podemos aplazar la ejecución de un fragmento de código. Para cancelar un setTimeout debemos de guardar lo que nos devuelve cuando lo llamamos y posteriormente hacer un clearTimeout
// Así empezamos un timeout
idTimeout = setTimeout(() => console.log('Esto tarda 2s'),2000);
// Esto cancela el timeout
clearTimeout(idTimeout)

setInterval
setInterval en MDN
Con setInterval podemos repetir periodicamente la ejecución de un fragmento de código. Para cancelar un setInterval debemos de guardar lo que nos devuelve cuando lo llamamos y posteriormente hacer un clearInterval
// Así empezamos un timeout
idInterval = setInterval(() => console.log('Esto se repite cada 2s'),2000);
// Esto cancela el timeout
clearInterval(idInterval)

Promise
Promise en MDN
Una promise representa un valor que puede estar disponible ahora, en el futuro, o nunca. Then devuelve una nueva promesa. Para resolver una promesa debemos usar el primer parámetro del callback, para rechazar la promesa usaremos el segundo parámetro
const myPromise = new Promise(function(resolve, reject) {
  setTimeout(function() {
    // En este caso solo resolvemos, no rechazamos
    resolve('El valor');
  }, 300);
});

myPromise.then(function(value) {
  console.log(value);
});

Promise all
Promise all en MDN
Espera a que todas las promesas de un array de promesas terminen, entonces en el callback del then tendremos disponible un array de valores, los valores estarán en el mismo orden que las promesas iniciales.
function promiseGenerator() {
  const randomNumber = Math.random() * 10000;
  return new Promise((resolve) => {
    console.log('Espero ->', randomNumber);
    setTimeout(() => {
      console.log('Resolviendo ->', randomNumber);
      resolve(randomNumber);
    }, randomNumber);
  });
}

const allPromises = [];
allPromises.push(promiseGenerator());
allPromises.push(promiseGenerator());
allPromises.push(promiseGenerator());

Promise.all(allPromises).then((allData) => {
  console.log(allData);
});

Promise race
Promise race en MDN
En cuanto termina la primera promesa ejecuta el callback y tendremos el valor de esa promesa en el parámetro del callback. El callback no se vuelve a ejecutar aunque terminen el resto de promesas.
function promiseGenerator() {
  const randomNumber = Math.random() * 10000;
  return new Promise((resolve) => {
    console.log('Espero ->', randomNumber);
    setTimeout(() => {
      console.log('Resolviendo ->', randomNumber);
      resolve(randomNumber);
    }, randomNumber);
  });
}

const allPromises = [];
allPromises.push(promiseGenerator());
allPromises.push(promiseGenerator());
allPromises.push(promiseGenerator());

Promise.race(allPromises).then((raceData) => {
  console.log(raceData);
});

Ejemplo cronometro
 function chrono(ammount) {
  let elapsedTime = 0;
  const intervalId = setInterval(() => {
    if (elapsedTime > ammount) {
      console.log('RING RING RING ');
      clearInterval(intervalId);
    } else {
      console.log(`Seconds elapsed ${elapsedTime}`);
      elapsedTime++;
    }
  }, 1000);
}
chrono(5); 

Ejemplos promises
let ammount = null;
let carPrice = null;

const accountRequest = new Promise((resolve, reject) => {
  resolve(10000);
});
const carRequest = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(11000);
  }, Math.random() * 1000);
});
// ESTO FATAL!!!
let test;
accountRequest.then((ammount) => {
  test = ammount;
});
console.log(test);
// DESDE AQUI NO FATAL

const promiseArray = [accountRequest, carRequest];
const allDataPromise = Promise.all(promiseArray);
const promiseRace = Promise.race(promiseArray);

allDataPromise.then((allData) => {
  if (allData[0] >= allData[1]) {
    console.log('COMPRA!');
  } else {
    console.log('ESPERA!');
  }
});

promiseRace.then((value) => {
  console.log('RACE', value);
});

Ejercicio Sospechosos
const names = ['Willy', 'Ivan', 'Ramiro'];
const eyeColor = ['azul', 'marron', 'azul'];
const height = ['bajo', 'alto', 'alto'];
const tattooed = [true, false, false];
const tip = [
  {
    height: 'alto',
  },
  {
    eyeColor: 'marron',
  },
  {
    tattooed: false,
  },
];

class Suspect {
  name;
  eyeColor;
  height;
  tattooed;
  #tip;
  constructor(name, eyeColor, height, tattooed, tip) {
    this.name = name;
    this.eyeColor = eyeColor;
    this.height = height;
    this.tattooed = tattooed;
    this.#tip = tip;
  }

  confess() {
    return this.#tip;
  }
}

class Detective {
  suspects;
  summary;
  constructor(suspects) {
    this.suspects = suspects;
  }
  investigate() {
    this.summary = this.suspects.map((suspect) => {
      return suspect.confess();
    });
    console.log(this.summary);
    const guilty = this.suspects.filter((suspect) => {
      return this.summary.every((tip) => {
        const tipKey = Object.keys(tip);
        return tip[tipKey] === suspect[tipKey];
      });
    });
    return guilty;
  }
}

const suspects = names.map((name, index) => {
  return new Suspect(name, eyeColor[index], height[index], tattooed[index], tip[index]);
});
const myDetective = new Detective(suspects);
console.log(myDetective.investigate());
console.log(suspects, myDetective);
