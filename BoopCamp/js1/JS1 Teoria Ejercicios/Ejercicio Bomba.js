'use strict';

//Ejercicio bomba

//Generamos código bomba.
function generateBombCode(range) {
  return Math.round(Math.random() * range);
}

// Generamos la introducción del código del usuario.
function askUserForCode() {
  return +prompt('Inserta aquí tu código. Un números de 1 al 10');
}

// Creamos la desactivación de la bomba y generar el número de intentos.
function deactivateBomb(numberOfAttempts) {
  const bombCode = generateBombCode(10);

  console.log(bombCode); //comprobación del código de la bomba.
  for (let i = 0; i < numberOfAttempts; i++) {
    const userGuess = askUserForCode();
    if (userGuess === bombCode) {
      return { defused: true, attempts: i + 1 };
    }
  }
  return { defused: false, attempts: numberOfAttempts };
}

// Se crea la función que ejecuta el juego.
function playGame(numberOfAttempts) {
  let result = deactivateBomb(numberOfAttempts);
  if (result.defused) {
    console.log(`Has Desactivado la bomba en ${result.attempts} intentos. !!!!Enhorabueno¡¡¡¡`);
  } else {
    console.log(`La bomba a estallado en ${result.attempts} intentos. Has quedado todo desparramado.`);
  }
}

// Sentencia para ejecutar el juego y los intentos a realizar.
console.log(playGame(5));
