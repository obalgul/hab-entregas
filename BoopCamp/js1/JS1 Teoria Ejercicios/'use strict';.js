'use strict';

/*Crear un falso e-commerce. Por un lado tenemos todos los artículos que forman
 el stock de la tienda. Tendremos también un usuario que añade cosas a su carrito,
 que es privado. Los artículos cuando los metemos al carrito los convertimos un tipo de
 dato que guarda las unidades del usuario de dicho artículo. La tienda es la
 encargada de imprimir un ticket por la consola.*/

const itemNames = ['Camisa', 'Pantalon', 'Calcetines'];
const itemPrices = [13, 27, 100];
//const items = Item.buildItems(itemNames, itemPrices);
//const myUser = new User();

class Item {
  itemType;
  prize;

  constructor(itemType, prize) {
    this.itemType = itemType;
    this.prize = prize;
  }
}

// elaboramos un array con todos los items a la venta y sus precios:

const inventary = itemNames.map((name, index) => {
  return new Item(name, itemPrices[index]);
});

class User {
  #carrito = [];

  addItems(item) {
    console.l;
    let foundItem = this.#carrito.find((element) => element.itemData.name === item.name);
    if (foundItem) {
      foundItem.ammount++;
    } else {
      this.#carrito.push({ itemData: item, ammount: 1 });
    }
    // Primero busco si está en el carrito

    // Si no habia, lo meto
    //else {s
    //Si existia le incremento las unidades
    //}
  }
}

const myUser = new User();
myUser.addItems(inventary[0]);
myUser.addItems(inventary[1]);
myUser.addItems(inventary[2]);
console.log(myUser);
