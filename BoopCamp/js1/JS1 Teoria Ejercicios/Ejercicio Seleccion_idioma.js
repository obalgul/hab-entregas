'use strict';

const idiomas = ['ES', 'EN', 'FR'];
let idiomaPorDefecto = 'EN';
let idiomaAMostrar;
let idiomaDelUsuario = 'ES';
console.log(idiomas.indexOf(idiomaDelUsuario));
if (idiomas.indexOf(idiomaDelUsuario) === -1) {
  idiomaAMostrar = idiomaPorDefecto;
} else {
  idiomaAMostrar = idiomaDelUsuario;
}

console.log(idiomaAMostrar);
