# Curso de CSS

- Breve historia del CSS
- Relación de HTML y CSS
- Síntaxis del CSS
- Selectores
- Modelo de cascada y herencia
- Modularización

### Breve historia del CSS

- El CSS fue propuesto en el año 1994 como un lenguaje para especificar como los documentos HTML son presentados a los usuarios.
- La w3c escogió entre múltiples propuestas de hojas de estilo, el CSS fue la escogida.
- En el año 1996 aparece la primera especificación de la w3c: CSS1.
- No es una idea nueva, desde los años 80 habia diferentes sistemas de especificar la presentación de documentos XML y similares.
- En el año 1997 se especificó el CSS2 y en el 1998 el CSS3. Esta versión es una especificación aún no acabada y es la que usamos ahora.
- El soporte en los navegadores en los primeros años fue muy limitado y no fue hasta mitad de los años 2000 cuando comenzó a usarse seriamente.

### Relación de HTML y CSS

- El CSS se aplica al HTML mediante una serie de reglas que definen como determinados tags se muestran.

- Por ejemplo: quiero que todos mis encabezados de primer nivel tengan el texto color amarillo y fondo azul.

- Lo primero que hace el navegador es cargar el HTML.

- En el HTML hay referencias a  reglas CSS, veremos que estas referencias pueden ser de diferentes tipos. 

- Cuando todo está cargado el navegador convenirte el HTML a lo que se llama  DOM que es una representación en memoria de la  estructura del HTML. 

- El navegador procesa el  CSS y comprueba se hay alguna  regla aplicable al  DOM y la aplica. 

- Finalmente el navegador  renderiza el  DOM en el  viewport (parte del navegador donde se ve el contenido del  body)

- Ejemplo:

  ```html
  <!DOCTYPE html>
  <html lang="gl">
    <head>
      <meta charset="utf-8" />
      <title>CSS!</title>

      <style>
        h1 {
          color: yellow;
          background: blue;
        }
      </style>
    </head>
    <body>
      <h1>Ola amigas!</h1>
    </body>
  </html>
  ```

#### Formas de aplicar o CSS

- Hai tres formas de cargar CSS en nuestro HTML:

  - aplicar directamente las declaraciones a las etiquetas (no recomendado):

    ```html
    <h1 style="color:yellow;background:blue">Esto no mola!</h1>
    ```

  - usando la etiqueta *style* en _head_ (ver ejemplo anterior). Tampoco es recomendado.

  - La forma recomendada es usar un archivo CSS externo, para eso tenemos que indicar en el *head* la referencia a ese archivo, así sería el HTML:

    ```html
    <!DOCTYPE html>
    <html lang="gl">
      <head>
        <meta charset="utf-8" />
        <title>CSS!</title>

        <link rel="stylesheet" href="style.css" />
      </head>
      <body>
        <h1>Ola amigas!</h1>
      </body>
    </html>
    ```

    y este es el archivo CSS referenciado en el _head_:

    ```
    h1 {
        color: yellow;
        background: pink;
    }
    ```

### Síntaxis el CSS

- El  CSS se construye mediante declaraciones agrupadas en bloques, y esos bloques están asociados a  selectores que definen la que elementos HTML se aplican. El conjunto de  selector y bloque de declaraciones es una  regla de  CSS.

- Las declaraciones de  CSS están compuestos por propiedades y valores separadas internamente por dos puntos (:) y con la siguiente declaración con ";":

  ```css
  background: blue;
  ```

  - Las propiedades indican el aspecto estilístico que queremos modificar: fuente, color, fondo, posición...
  - Los valores indican como queremos cambiar el aspecto estilístico anterior.

- Los bloques son grupos de declaraciones agrupadas entre llaves ({}):

  ```css
   {
    background: blue;
    color: yellow;
  }
  ```

- Es obligatorio que todas las declaraciones estén agrupadas en bloques, no deben aparecer fuera de los bloques.

- Los bloques tienen que estar precedidos por selectores. Los  selectores definen la que elementos se aplican las declaraciones agrupadas en el bloque. Estas declaraciones pueden hacer referencia directa a  tags, clases,  ids:

  ```css
  h1 {
    background: blue;
    color: yellow;
  }
  ```

- Fuera de los bloques pueden aparecer otros *CSS  Statements*, que son reglas especiales de CSS que definen tipos de letra, codificación de caracteres, *media queries* o módulos que veremos en las siguientes secciones:

  ```css
  @import 'fichero.css';
  ```

- Al igual que en HTML el espacio en blanco en los ficheros CSS es ignorado.

### Selectores

- Los selectores de CSS permiten asociar bloques de declaraciones con elementos de nuestras páginas.
- Tipos de selectores:
  - **Selectores simples**: permiten seleccionar por tipo de elemento (nombre del tag), class o id.
  - **Selectores por atributos**: permiten seleccionar elementos basándose en sus atributos o valores de atributos.
  - **Pseudo-classes**: permiten seleccionar elementos basándose en su estado actual.
  - **Pseudo-elements**: permite seleccionar contenidos posicionado respecto a un elemento o generar contenido asociado a un elemento.

#### Selectores simples

##### Selectores por tipo de elemento

- Para seleccionar un elemento por su nombre de tag simplemente indicamos el nombre del tag en el selector:

  ```html
  <p>lorem ipsum</p>
  ```

  ```css
  p {
    color: rebeccapurple;
  }
  ```

- Los selectores por nombre de tag son muy genéricos y afectan a todos los tags, hay que usarlos con cuidado y normalmente combinados con otros selectores. Veremos más adelante como hacerlo.

- Hay un selector especial que es el asterisco (\*) que selecciona todos os elementos, usar con cuidado ou combinado con otros selectores:

  ```css
  * {
    background: rgba(0, 255, 0, 0.2);
  }
  ```

##### Selectores por class

- El class é un atributo que puede tener cualquier elemento HTML. Se puede especificar una única class ou varias para cada elemento y puede haber múltiples elementos en la misma página con el mismo class. No pueden tener espacios.

  ```html
  <p class="defecto cabecera parrafo-especial">lorem ipsum</p>
  ```

- Para seleccionar por class  simplemente indicamos el nombre de la class con un punto (.) antes:

  ```css
  .cabecera {
    background: lightgray;
  }
  ```

##### Selectores por id

- El id es un atributo que puede tener cualquier elemento HTML, el igual que *class* no puede tener espacios. Los ids tienen que ser únicos dentro del mismo documento HTML pero pueden repetirse en outros documentos del sitio web.

  ```html
  <h1 id="principal">Lorem ipsum</h1>
  ```

- Para seleccionar por id en CSS simplemente indicamos el nombre del id cun con un (#) antes:

  ```css
  #principal {
    text-decoration: underline;
  }
  ```

#### Selectores por atributos

- Los elementos HTML pueden tener atributos variados:

  ```html
  <p lang="en">Texto en inglés</p>
  <a href="http://google.com">Google</a>
  ```

- Aparte de los atributos propios de cada elemento (ver la documentación de la MDN de cada elemento en particular) estos son los atributos globales que soportan todos los elementos: https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes

- El atributo especial *data* nos permite crear atributos personalizados, simplemente tenemos que crear un atributo que se chame "data-ALGO":

  ```html
  <p data-autor="Berto">Texto por Berto</p>
  <article data-etiquetas="producto destacado oferta">...contido</article>
  <button data-numero="3">Enviar</button>
  ```

- Para seleccionar por nombre de atributo usamos el selector _[atributo]_:

  ```css
  [lang] {
    background: blue;
  }

  [data-autor] {
    color: red;
  }
  ```

- Para seleccionar por combinación de atributo y valor usamos _[atributo=valor]_:

  ```css
  [data-autor='Berto'] {
    color: green;
  }
  ```

  Esto seleccionaría todos los elementos que tengan el atributo _data-autor_ con valor Berto.

- Para selectionar por contenido de atributo usamos:

  - `[atributo^="val"]`: selecciona todos os elementos cuyo atributo comience por "val"
  - `[atributo$="val"]`: selecciona todos os elementos cuyo atributo acabe por "val"
  - `[atributo*="val"]`: selecciona todos os elementos cuyo atributo contenga la cadena de texto "val", p.ej.: seleccionaria los atributos con valor "o**val**ado".
  
- Más detalles y ejemplos aquí: https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Attribute_selectors

#### Pseudo-classes

- Las pseudo-classes permiten seleccionar elementos **en base a su estado, posicion, hijos, etc.**

- Se escriben con dos puntos (:) antes y normalmente complementan a otros selectores.

  ```html
  <a href="http://google.com">Google</a>
  ```

  ```css
  a:visited {
    color: gray;
  }
  ```

- Hay muchas pseudo-classes que se pueden consultar aquí: https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes#Index_of_standard_pseudo-classes

- Algunas de las pseudo-classes más usadas:

  - :checked - selecciona inputs de tipo checkbox que estén marcados.
  - :first-child - selecciona los elementos que sean primeros hijos de un grupo de hijos.
  - :last-child - similar al anterior pero aplicado al último hijo.
  - :not(selector) - selecciona elementos no afectados por "selector"
  - :nth-child(valor) - selecciona hijos basados en su posición.

#### Pseudo-elements

- Los *pseudo-elements* son selectores que nos permiten referirnos a **partes de los elementos**.

- Son similares a las *pseudo-classes* pero en lugar de dos puntos (:) antes se escriben con dos puntos dobles (::).

- Los *pseudo-elements* más usados son:

  - ::first-letter - permite darle estilo a la primera letra del texto del elemento.
  - ::first-line - lo mismo que lo anterior pero selecciona la primeira línea del texto.
  - ::selection - permite darle estilo al texto que tengamos seleccionado con el ratón o teclado en ese momento.

- Hay dos pseudo-elements especiales que seleccionan partes de los elementos **que non existen** y permiten "crearlas" asociándoles un contenido usando la propiedad de CSS "*content*". Son ::before e ::after.

  ```css
  p.especial::after {
    content: 'Ver máis';
    color: red;
  }
  ```

  Esto crearía el contenido textual "Ver más" después de los  parrafos con la  class "especial". Es importante tener en cuenta que esto no crea una etiqueta nueva si no que solo crea el contenido en una especie de elemento sin nombre y sin valor  estructural pero que puede ser  estilado. En el ejemplo anterior aparecería de color rojo.

#### Combinación de selectores

- Hai varias formas de combinar selectores para optimizar nuestro CSS:
  - **Lista de selectores:** `A, B` cualquier elemento que seleccione A o B.
  - **Descendientes:** `A B`: selecciona los elementos B que sean hijos (directos o no) de A.
  - **Descendientes directos:** `A > B` selecciona los elementos B que tengan de padre a A.
  - **Adyacentes:** `A + B` selecciona los elementos B que aparezcan justo después de A siendo los dos hijos directos del mismo padre.
  - **Hermanos no consecutivos:** `A ~ B` selecciona los elementos B que aparezcan después de A aún que non sean los seguintes y que compartan el mismo padre.

### Modelo de cascada e herdanza

#### A cascada

- Cuando escribimos  selectores llegará un momento que múltiples  reglas de  CSS afectan a un mismo elemento.

- El modelo de  cascada define cual de las  reglas gana y es aplicada. El modelo de  cascada es complejo y requiere experiencia a parte de teoría a la hora de usarlo bien.

- Hay tres factores que afectan al modelo de  cascada: importancia, especificidad y orden

- La importancia de una propiedad de  CSS podemos aplicarla usando una declaración especial: _! important_:

  ```html
  <p class="especial">lorem ipsum</p>
  <p class="especial" id="unico">hello kitty</p>
  ```

  ```css
  #unico {
    border: 1px solid red;
    color: red;
  }

  .especial {
    color: green;
    border: none !important;
  }
  ```

- So estamos usando muchos !important en nuestro  CSS **estamos haciendo algo mal**. La mejor estrategia es evitarlos completamente únicamente que sea la última opción y sepamos el que estamos haciendo.

- La especificidad determina cuanto de específico es un  selector.

- Como  regla general los  selectores de elemento tienen poca especificidad, los de clase más y los de  id aun más. La única manera de ganar a un  selector de  ID es usar alguna propiedad con el  keyword !important.

- El  CSS que se establece mediante el atributo  style directamente en los  tags (**no recomendado!**) gana siempre a cualquier otro  CSS excepto a las declaraciones con !important.

- Quitando estas excepciones las reglas son así:

  - Cada selector tendrá un valor de especificidad numérico.

  - Cada elemento que contenga el selector aumenta la especificidad en 1.

  - Cada clase o *pseudo-class* que contenga el selector aumenta la especificidad en 10.

  - Cada id que contenga el selector aumenta la especificidad en 100.

  - Los pseudo-elements no afectan al valor de especificidad.

    ```css
    /* especificidad: 101 */
    #outer a {
      background-color: red;
    }

    /* especificidad: 201 */
    #outer #inner a {
      background-color: blue;
    }

    /* especificidad: 104 */
    #outer div ul li a {
      color: yellow;
    }

    /* especificidad: 113 */
    #outer div ul .nav a {
      color: white;
    }

    /* especificidad: 24 */
    div div li:nth-child(2) a:hover {
      border: 10px solid black;
    }

    /* especificidad: 23 */
    div li:nth-child(2) a:hover {
      border: 10px dashed black;
    }

    /* especificidad: 33 */
    div div .nav:nth-child(2) a:hover {
      border: 10px double black;
    }
    ```

- Por último a orden en la que está especificado el  CSS también afecta a la  Cascada, por lo tanto la dos  reglas con la misma especificidad exacta afecta siempre la última que aparece en el código.

#### La herencia

- La herencia en  CSS determina que propiedades van a afectar a los hijos de un elemento cuando le aplicamos  CSS.
- Algunas propiedades se heredan y otras no. No hay una  regla general para establecer cuáles son unas y otras. Hay que aplicar el sentido común = mirar la documentación.
- Por ejemplo, el tipo de letra, color sí que se hereda pero el margen no.
- Para ver cuáles son heredadas y cuales no se puede consultar la documentación,  p.ex: se vamos a la documentación de la propiedad  _font-style_: https://developer.mozilla.org/en-US/docs/Web/CSS/font-style#Specifications vemos en la sección de "Specifications" una tabla de fondo azul que indica si esta propiedad es "Inherited" o no.
- Hay dos valores especiales que son universales la cualquier propiedad de  CSS que permite controlar la herencia: _inherit_ y _initial_. Las propiedades que tengan valor _inherit_ siempre heredarán el valor de su elemento padre y las que tengan el valor _initial_ cancelarán cualquier herencia y cogerán el valor de la hoja de estilos por defecto del navegador.

### Modularización

- Los ficheros CSS permiten cargar otros ficheros CSS mediante el _statement_ @import.

- El @import vai acompañado de la URL que queremos cargar, esta URL puede ser absoluta o relativa al igual que en el _href_ de los links:

  ```css
  @import '/css/outro-estilo.css';
  ```

- Ls @import **siempre** deben aparecer al principio de la hoja de estilos antes que cualquier otra regla de CSS.
