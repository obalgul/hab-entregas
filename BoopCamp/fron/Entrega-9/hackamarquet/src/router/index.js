import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/HomeView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",

    component: () => import("../views/About.vue"),
  },
  {
    path: "*",
    name: "Error!!!",
    component: () => import("../views/Error.vue"),
  },
  {
    path: "/add",
    name: "NewUser",
    component: () => import("../views/NewUser.vue"),
  },
  {
    path: "/addArticle",
    name: "NewArticle",
    component: () => import("../views/NewArticle.vue"),
  },
  {
    path: "/articles",
    name: "ListArticles",
    component: () => import("../views/ListArticles.vue"),
  },
  {
    path: "/editarticle",
    name: "EditArticle",
    component: () => import("../views/EditArticle.vue"),
  },
  {
    path: "/listUsers",
    name: "ListUsers",
    component: () => import("../views/ListUsers.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
