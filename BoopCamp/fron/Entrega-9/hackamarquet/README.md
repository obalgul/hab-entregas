# hackamarquet

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

#### Descripción

Realización de una web de mercado. Con varias funcionalidades, para desarrollar los conocimientos adquiridos en Vue, JS, CSS, NODE entre otros.
