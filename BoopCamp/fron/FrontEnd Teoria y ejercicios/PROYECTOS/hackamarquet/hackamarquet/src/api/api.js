//exportamos los módulos de nmp

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

//creamos una conexión a la base de datos con los datas de esta

const connection = mysql.createConnection({
  host: "localhost",
  user: "oscar",
  password: "oscar",
  database: "hackamarquet",
});

//conectamos la base de datos
connection.connect((error) => {
  if (error) throw error;
  console.log("Dadabase is UP");
});

const PORT = 3050;

//arrancamos la conexión a la api
app.listen(PORT, () => console.log("API is UP"));

//****************************ARTICULOS*******************************

//creamos conexión para creación de articulos
//es un POST

app.post("/addArticle", (req, res) => {
  //secuencia sql
  const sql = "INSERT INTO listaproductos SET ?";

  //objeto de datos del nuebo cliente
  const nuevoCliente = {
    nombre: req.body.nombre,
    stock: req.body.stock,
    disponibilidad: req.body.disponibilidad,
    imagen: req.body.imagen,
  };
  //conexion bbdd
  connection.query(sql, nuevoCliente, (error) => {
    if (error) throw error;
    console.log("Creado artículo correctamente");
  });
});

//editar o actualizar articulo
app.put("/editArticle/:id", (req, res) => {
  //datos recogidos de la vista
  const id = req.params.id;
  const nombre = req.body.nombre;
  const stock = req.body.stock;
  const disponibilidad = req.body.disponibilidad;
  const imagen = req.body.imagen;

  //consulta sql
  const sql = `UPDATE listaproductos SET nombre='${nombre}', stock='${stock}', disponibilidad='${disponibilidad}', imagen='${imagen}'
              WHERE id=${id}`;

  //conexión a la bbdd
  connection.query(sql, (error) => {
    if (error) throw error;
    console.log("Artículo actualizado correctamente");
  });
});

//Borrar un articulo con DELETE
app.delete("/delete/:id", (req, res) => {
  //recogemos los datos enviados por la vista
  const id = req.params.id;

  //consulta sql
  const sql = `DELETE FROM listaproductos WHERE id=${id}`;

  //conexión bbdd
  connection.query(sql, (error) => {
    if (error) throw error;
    console.log("Articulo borrado correctamente");
  });
});

//creamos conexión para listar productos
app.get("/articles", (req, res) => {
  //consulta sql
  const sql = "SELECT * FROM listaproductos";

  //conexión a la bbdd
  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results);
    } else {
      console.log("La lista esta vácia.");
    }
  });
});

//****************************USUARIOS*********************************

//creamos un GET conexión para listar usuarios
app.get("/users", (req, res) => {
  //consulta sql
  const sql = "SELECT * FROM listaclientes";

  //conexión a la bbdd
  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results);
    } else {
      console.log("La lista esta vácia.");
    }
  });
});

//creamos conexión para creación de usuarios registrado
//es un POST

app.post("/add", (req, res) => {
  //secuencia sql
  const sql = "INSERT INTO listaclientes SET ?";

  //objeto de datos del nuebo cliente
  const nuevoCliente = {
    nombre: req.body.nombre,
    usuario: req.body.usuario,
    password: req.body.password,
    email: req.body.email,
    foto: req.body.foto,
  };
  //conexion bbdd
  connection.query(sql, nuevoCliente, (error) => {
    if (error) throw error;
    console.log("Creado cliente correctamente");
  });
});

//editar o actualizar usuario
app.put("/listUsers/:id", (req, res) => {
  //datos recogidos de la vista
  const id = req.params.id;
  const nombre = req.body.nombre;
  const usuario = req.body.usuario;
  const password = req.body.password;
  const email = req.body.email;
  const foto = req.body.foto;

  //consulta sql
  const sql = `UPDATE listaclientes SET nombre='${nombre}', usuario='${usuario}', password='${password}', email='${email}', foto='${foto}'
              WHERE id=${id}`;

  //conexión a la bbdd
  connection.query(sql, (error) => {
    if (error) throw error;
    console.log("Cliente actualizado correctamente");
  });
});

//Borrar un usuario con DELETE
app.delete("/deleteUser/:id", (req, res) => {
  //recogemos los datos enviados por la vista
  const id = req.params.id;

  //consulta sql
  const sql = `DELETE FROM listaclientes WHERE id=${id}`;

  //conexión bbdd
  connection.query(sql, (error) => {
    if (error) throw error;
    console.log("Usuario borrado correctamente");
  });
});
