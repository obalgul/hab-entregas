//requerimos axios
const axios = require("axios").default;
//url dela api
const APIURL = "https://rickandmortyapi.com/api/";

//funcion para coger los personajes
function getAllCharacters() {
  return axios.get(`${APIURL}/character`);
}
export default {
  getAllCharacters,
};
