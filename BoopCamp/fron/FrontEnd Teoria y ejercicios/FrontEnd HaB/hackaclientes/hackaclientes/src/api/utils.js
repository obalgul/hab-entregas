import jwt from "jwt-decode";
import axios from "axios";

const ENDPOINT = "http://localhost:3050";

export function login(user, password) {
  try {
    axios
      .post(`${ENDPOINT}/auth`, {
        user: user,
        password: password,
      })
      .then(function(response) {
        console.log(response);
        //Guardo el Token
        setAuthToken(response.data.token);
        //guardo el role
        setIsAdmin(response.data.admin);
        //guardo nombre usuario
        setName(response.data.user);
      });
  } catch (error) {
    console.log(error);
  }
}

//Funcion para guardar el localstroage el jsonwebtoken
export function setAuthToken(token) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  localStorage.setItem("AUTH_TOKEN_KEY", token);
}

//Funcion para drecuperar el token
export function getAuthToken() {
  return localStorage.getItem("AUTH_TOKEN_KEY");
}

//Funcion para conseguir fecha caducidad del token
export function tokenExpiration(encodedToken) {
  let token = jwt(encodedToken);
  if (!token.exp) {
    return null;
  }
  let date = new Date(0);
  date.setUTCSeconds(token.exp);
  return date;
}

//Funcion que comprueba si el token esta pocho
export function isExpired(token) {
  let expirationDate = tokenExpiration(token);
  return expirationDate < new Date();
}

//Fucion que comprueba si etta logeada y su token es correcto
export function isLoggedIn() {
  let authToken = getAuthToken();
  return !!authToken && !isExpired(authToken);
}

//funcion guardar admin en el localstorage
export function setIsAdmin(admin) {
  localStorage.setItem("ROLE", admin);
}

//recuperar admn  del localstorage
export function getIsAdmin() {
  return localStorage.getItem("ROLE");
}

//Comprobar que es admin true
export function checkIsAdmin() {
  let role = null;
  let admin = getIsAdmin();

  if (admin === "true") {
    role = true;
  } else {
    role = false;
  }
  return role;
}
//funcion de guardar nombre de usurio en le localstorage
export function setName(user) {
  localStorage.setItem("NAME", user);
}

//recuperamos el nombre de usuario de localstorage
export function getName() {
  return localStorage.getItem("NAME");
}

//Funcion de logout
export function logout() {
  axios.defaults.headers.common["Authorization"] = "";
  localStorage.removeItem("AUTH_TOKEN_KEY");
  localStorage.removeItem("ROLE");
  localStorage.removeItem("NAME");
}
