//Import cosas token
const config = require("./config");

const jwt = require("jsonwebtoken");

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.set("llave", config.llave);

const connection = mysql.createConnection({
  host: "localhost",
  user: "oscar",
  password: "oscar",
  database: "hackaclientes",
});

connection.connect((error) => {
  if (error) throw error;
  console.log("DAtabase UP");
});

const PORT = 3050;

app.listen(PORT, () => console.log("API UP"));

//recoger todos los clientes de la ddbb

app.get("/clientes", (req, res) => {
  //secuencia sql
  const sql = "SELECT * FROM listaclientes";

  //conexion a la bbdd
  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results);
    } else {
      console.log("No hay clientes");
    }
  });
});

//añadir clientes a la bbdd
app.post("/add", (req, res) => {
  //secuencia sql
  const sql = "INSERT INTO listaclientes SET ?";

  //objeto de datos del nuebo cliente
  const nuevoCliente = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    ciudad: req.body.ciudad,
    empresa: req.body.empresa,
  };
  //conexion bbdd
  connection.query(sql, nuevoCliente, (error) => {
    if (error) throw error;
    console.log("Creado cliente correctamente");
  });
});

//actualizar cliente bbdd
app.put("/update/:id", (req, res) => {
  //Datos recibidos de la vista
  const id = req.params.id;
  const nombre = req.body.nombre;
  const apellido = req.body.apellido;
  const ciudad = req.body.ciudad;
  const empresa = req.body.empresa;

  //secuencia sql
  const sql = `UPDATE listaclientes SET nombre='${nombre}', apellido='${apellido}', ciudad='${ciudad}', empresa='${empresa}' 
              WHERE id=${id}`;

  //conexinbase datos
  connection.query(sql, (error) => {
    if (error) throw error;
    console.log("Cliente actulizado con éxito.");
  });
});

//Borrar clientes

app.delete("/delete/:id", (req, res) => {
  //datos que llegan de la vista
  const id = req.params.id;

  //secuencia sql
  const sql = `DELETE FROM listaclientes WHERE id=${id}`;

  //conexion bbdd
  connection.query(sql, (error) => {
    if (error) throw error;
    console.log("Cliente borrado satisfactoriamente");
  });
});

//Autorizacion dela API
app.post("/auth", (req, res) => {
  //Datos que se reciben
  const user = req.body.user;
  const password = req.body.password;

  //secuencia sql
  const sql = `SELECT * FROM usuarios WHERE user='${user}' AND password='${password}'`;

  //conexión bbdd
  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      const payload = {
        check: true,
      };
      //Guardando si es admin
      let admin = null;
      if (results[0].isAdmin === 1) {
        admin = true;
      } else {
        admin = false;
      }
      //guardando el nimbre de usuario
      let user = "";
      user = results[0].user;

      //Token
      const token = jwt.sign(payload, app.get("llave"), {
        expiresIn: "2days",
      });
      res.json({
        mensaje: "Autenticación completada con éxito",
        token: token,
        admin: admin,
        user: user,
      });
    } else {
      console.log("Datos incorrectos");
    }
  });
});
