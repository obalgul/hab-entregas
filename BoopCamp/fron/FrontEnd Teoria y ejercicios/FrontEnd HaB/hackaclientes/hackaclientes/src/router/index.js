import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

//Importamos la función para poder entrar o no
import { isLoggedIn } from "../api/utils.js";

import { checkIsAdmin } from "../api/utils.js";

Vue.use(VueRouter);

const routes = [
  {
    path: "/home",
    name: "Home",
    component: Home,
    meta: {
      allowAnon: false,
    },
  },
  {
    path: "/about",
    name: "About",

    component: () => import("../views/About.vue"),
    meta: {
      allowAnon: true,
    },
  },
  {
    path: "/add-clients",
    name: "AddClients",

    component: () => import("../views/AddClients.vue"),
    meta: {
      allowAnon: false,
      onlyAdmin: true,
    },
    beforeEnter: (to, fron, next) => {
      if (to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: "/home",
          query: { redirect: to.fulPath },
        });
      } else {
        next();
      }
    },
  },
  {
    path: "*",
    name: "Horror!!!",

    component: () => import("../views/Error.vue"),
    meta: {
      allowAnon: true,
    },
  },
  {
    path: "/",
    name: "Login",

    component: () => import("../views/LoginPage.vue"),
    meta: {
      allowAnon: true,
    },
  },
  {
    path: "/registred",
    name: "Registred",

    component: () => import("../views/RegistredPage.vue"),
    meta: {
      allowAnon: true,
    },
  },
];

const router = new VueRouter({
  routes,
});

//comprobación si es valido el ususario para ver la pagina
router.beforeEach((to, from, next) => {
  if (!to.meta.allowAnon && !isLoggedIn()) {
    next({
      path: "/",
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
});

export default router;
