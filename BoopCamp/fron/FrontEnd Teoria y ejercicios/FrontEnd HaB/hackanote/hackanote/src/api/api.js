const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const app = express();

//cosas que usa app
app.use(cors());
app.use(bodyParser.urlencoded({ extendde: true }));
app.use(bodyParser.json());

//conexión a la BBDD
const connection = mysql.createConnection({
  host: "localhost",
  user: "oscar",
  password: "oscar",
  database: "bbdd_notas",
});

//realizar conexion
connection.connect((error) => {
  if (error) throw error;
  console.log("Database UP ");
});

//conexiona a la api
const PORT = 3050;

app.listen(PORT, () => console.log("API UP"));

//mensaje de bienvenida
app.get("/", (req, res) => {
  res.send("Hola");
});

//recogiendo nota específivca
app.get("/notas", (req, res) => {
  //secuencia sql
  const sql = "SELECT * FROM notas";

  //conexión bbdd
  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results);
    } else {
      console.log("No hay resultdos");
    }
  });
});

//actualizando nota
app.put("/notas/update/:id", (req, res) => {
  //datos que envio
  const texto = req.body.texto;
  const id = req.body.id;

  //secuenci sql
  const sql = `UPDATE notas SET texto= '${texto}' WHERE id='${id}' `;

  //conexion bbdd
  connection.query(sql, (error) => {
    if (error) throw error;
    console.log("Nota actualizada");
  });
});

//añadir una nota
app.post("/notas/add", (req, res) => {
  //secuencia sql
  const sql = "INSERT INTO notas SET ?";

  //objeto qu recibe la base de datos
  const nuevaNota = {
    texto: req.body.texto,
    titulo: req.body.titulo,
    autor: req.body.autor,
  };

  //conexión y ejecución de sql
  connection.query(sql, nuevaNota, (error) => {
    if (error) throw error;
    console.log("Nota creada con éxito");
  });
});

//borra nota
app.delete("/notas/delete/:id", (req, res) => {
  //datos que envio
  const id = req.params.id;

  //secuenci sql
  const sql = `DELETE FROM notas WHERE id='${id}' `;

  //conexion bbdd
  connection.query(sql, (error) => {
    if (error) throw error;
    console.log("Nota borrada");
  });
});
