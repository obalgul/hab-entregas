import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router";
import routes from '@/router/index';
import moment from "moment";

Vue.config.productionTip = false

const router = new VueRouter({
    routes
});

// import store from "@/store/store";
// router.beforeEach((to, from, next) => {
//     if (to.name !== 'login' && to.name === 'users' && !store.isStaff()) {
//         next({name: 'products'})
//     } else {
//         next();
//     }
// })

Vue.filter('yesno', function (value) {
    if (value) {return "Sí"}
    return "No";
})

Vue.filter('myDate', function (value) {
    if (value === null) { return ""; }
    let myDate = moment(value);
    return myDate.format('DD/MM/YYYY kk:mm');
})


new Vue({
    render: h => h(App),
    router,
}).$mount('#app')
