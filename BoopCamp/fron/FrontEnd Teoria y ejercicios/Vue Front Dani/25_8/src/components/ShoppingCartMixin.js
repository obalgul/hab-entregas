export default {
    computed: {
        cartPrice() {
            let totalPrice = 0;
            for (let i = 0; i < this.cart.length; i++) {
                totalPrice += this.cart[i].price * this.cart[i].quantity;
            }
            return totalPrice;
        },
    },
}