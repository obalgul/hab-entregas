import axios from 'axios';

const instance = axios.create({
    baseURL: process.env.VUE_APP_SERVER_URL,
});


export default {
    login: function (username, password) {
        return instance.post('token/', {username, password})
            .then(response => {
                this.setToken(response.data.access);
                return response.data;
            });
    },

    setToken: (token) => {
        instance.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    },

    logout: () => {
        delete instance.defaults.headers.common['Authorization'];
    },

    // products
    getProducts: () => {
        return instance.get('products/')
            .then(response => {
                return response.data;
            });
    },
    getProduct: (id) => {
        return instance.get('products/' + id + '/')
            .then(response => {
                return response.data
            });
    },
    addProduct: (product) => {
        return instance.post('products/', product)
    },
    deleteProduct: (product) => {
        return instance.delete('products/' + product.id + '/', product)
    },
    updatePrice: (product, newPrice) => {
        return instance.patch('products/' + product.id + '/', {
            price: newPrice
        });
    },
    updateQuantity: (product, newQuantity) => {
        return instance.patch('products/' + product.id + '/', {
            quantity: newQuantity
        });
    },

    // users
    getOwnUser: () => {
        return instance.get('users/me/')
            .then(response => {
                return response.data;
            })
            .catch(error => {
                console.log(error);
            });
    },

    asyncGetOwnUser: async () => {
        try {
            return await instance.get('users/me/');
        } catch (error) {
            console.log(error);
        }
    },



    getUsers: function () {
        return instance.get('user-extensions/')
            .then(response => {
                let plainUsers = [];
                for (let i = 0; i < response.data.length; i++) {
                    plainUsers.push(this.transformUserFromBackend(response.data[i]))
                }
                return plainUsers;
            });
    },
    createUser: (newUser) => {
        return instance.post('users/', newUser)
            .then(response => response.data);
    },
    updateUser: function (newUser) {
        return instance.patch('users/' + newUser.id + '/', newUser)
            .then(response => this.transformUserFromBackend(response.data));
    },
    uploadProfileImage: function (user, formData) {
        return instance.patch('user-extensions/' + user.user_extension_id + '/',
            formData,
            {headers: {'Content-Type': 'multipart/form-data'}})
            .then(response => this.transformUserFromBackend(response.data));
    },
    isUsernameAvailable: function (username) {
        return instance.get('users/check-username/' + username)
            .then(response => response.data.is_available);
    },

    transformUserFromBackend: (backendUser) => {
        return {
            profile_image: backendUser.profile_image,
            user_extension_id: backendUser.id,
            ...backendUser.user,
        }
    },

    // purchase history
    getPurchaseHistory: (user) => {
        return instance.get('purchase-history/', {params: {userid: user.id}})
            .then(response => {
                return response.data;
            })
    }
}