import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue"),
  },
  {
    path: "/topartist",
    name: "TopArtist",
    component: () => import("../views/TopArtist.vue"),
  },
  {
    path: "/toptrack",
    name: "TopTrack",
    component: () => import("../views/TopTrack.vue"),
  },
  {
    path: "*",
    name: "Horror!!!",
    component: () => import("../views/Error.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
