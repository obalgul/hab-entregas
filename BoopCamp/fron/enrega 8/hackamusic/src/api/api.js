import config from "./config.js";
const axios = require("axios").default;

const apiKey = config.apiKey;

const BASE_URL = "https://ws.audioscrobbler.com/";
const URL_GEO = `2.0/?method=geo.gettopartists&country=spain&api_key=${apiKey}&format=json`;
const TopTracks = `2.0/?method=geo.gettoptracks&country=spain&api_key=${apiKey}&format=json`;
const TopTags = `2.0/?method=chart.gettoptags&api_key=${apiKey}&format=json`;

async function getArtists() {
  try {
    const response = await axios.get(`${BASE_URL}${URL_GEO}`);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

async function getTracks() {
  try {
    const response = await axios.get(`${BASE_URL}${TopTracks}`);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}
async function getTags() {
  try {
    const response = await axios.get(`${BASE_URL}${TopTags}`);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export default {
  getArtists,
  getTracks,
  getTags,
};
