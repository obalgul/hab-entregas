USE oscar;

CREATE TABLE clientes
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
número_cuenta BIT
);
CREATE TABLE sucursales
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
número_cuenta BIT,
id_clientes INT UNSIGNED REFERENCES clientes
);
CREATE TABLE trasacciones
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
número_trasacción BIT UNIQUE,
fecha TIMESTAMP,
importe BIT,
número_cuenta BIT,
id_sucursales INT UNSIGNED REFERENCES sucursales
);
CREATE TABLE clientes_sucursales
(
id_clientes INT UNSIGNED REFERENCES clientes,
id_sucursales INT UNSIGNED REFERENCES sucursales,
PRIMARY KEY (id_clientes, id_sucursales)
);
CREATE TABLE sucursales_trasacciones
(
id_sucursales INT UNSIGNED REFERENCES sucursales,
id_trasacciones INT UNSIGNED REFERENCES trasacciones,
PRIMARY KEY (id_sucusales, id_trasacciones)
);