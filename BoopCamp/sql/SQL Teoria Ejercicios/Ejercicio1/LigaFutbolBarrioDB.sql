use oscar;

CREATE TABLE IF NOT EXISTS equipos
( 
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
categoria VARCHAR (50) NOT NULL,
nombre_equipo VARCHAR (50) NOT NULL UNIQUE,
patrocinador VARCHAR(50),
color_camiseta_1 VARCHAR (50) NOT NULL,
color_camiseta_2 VARCHAR (50) NOT NULL
);
CREATE TABLE IF NOT EXISTS jugadores
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (50) NOT NULL,
apellidos VARCHAR (50) NOT NULL,
fecha_nacimiento DATE NOT NULL,
telefono VARCHAR (9) NOT NULL,
direccion VARCHAR (100) NOT NULL,
id_equipo INT UNSIGNED,
FOREIGN KEY (id_equipo) REFERENCES equipos (id)
);
CREATE TABLE IF NOT EXISTS partidos
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
fecha DATETIME,
campo VARCHAR (50) NOT NULL,
arbitro VARCHAR (50) NOT NULL,
incidencias TINYTEXT
);
CREATE TABLE IF NOT EXISTS equipo_partido
(
id_equipo INT UNSIGNED,
id_partido INT UNSIGNED,
goles INT UNSIGNED DEFAULT 0,
FOREIGN KEY (id_equipo) REFERENCES equipos (id),
FOREIGN KEY (id_partido) REFERENCES partidos (id),
PRIMARY KEY (id_equipo, id_partido)
);
