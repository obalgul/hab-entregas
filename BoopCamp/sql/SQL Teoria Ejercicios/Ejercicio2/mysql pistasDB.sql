USE pistasDB;

CREATE TABLE IF NOT EXISTS usuarios
( 
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (50) NOT NULL,
apellidos VARCHAR (50) NOT NULL,
email VARCHAR(75) UNIQUE,
telefono VARCHAR (13) NOT NULL,
dni VARCHAR (9) UNIQUE,
fecha_nacimiento DATE
);
CREATE TABLE IF NOT EXISTS polideportivos
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre_polideportivo VARCHAR (50),
direccion VARCHAR (50),
extensión_m2 INT CHECK (extension > 0)
);
CREATE TABLE IF NOT EXISTS pistas
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
codigo VARCHAR (15) UNIQUE,
tipo VARCHAR (50),
estado_pista BOOLEAN DEFAULT TRUE,
ultimo_precio DECIMAL (4,2),
ultima_reserva DATETIME,
id_polideportivo INT UNSIGNED,
FOREIGN KEY (id_polideportivo) REFERENCES polideportivos (id)
);
CREATE TABLE IF NOT EXISTS reservas
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
fecha_en_que_reserva DATETIME,
fecha_de_uso DATETIME,
precio DECIMAL (4,2),
id_pista INT UNSIGNED,
FOREIGN KEY (id_pista) REFERENCES pistas (id)
);
CREATE TABLE IF NOT EXISTS usuarios_reservas
(
id_usuario INT UNSIGNED,
id_reserva INT UNSIGNED,
FOREIGN KEY (id_usuario) REFERENCES usuarios (id),
FOREIGN KEY (id_reserva) REFERENCES reservas (id),
PRIMARY KEY (id_usuario, id_reserva) 
);