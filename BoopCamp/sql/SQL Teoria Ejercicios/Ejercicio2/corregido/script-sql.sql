USE polideportivos;

CREATE TABLE usuarios (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    email VARCHAR(50),
    telefono VARCHAR(10),
    dni VARCHAR(9),
    fecha_nacimiento DATE
);

CREATE TABLE polideportivos (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50),
    direccion VARCHAR(50),
    extension INT CHECK (extension > 0)
);

CREATE TABLE pistas (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    codigo VARCHAR(10) UNIQUE,
    tipo ENUM('tenis', 'fútbol', 'baloncesto', 'natación'),
    operativa BOOLEAN DEFAULT TRUE,
    precio DECIMAL(4, 2),
    fecha_ultima_reserva DATETIME,
    id_polideportivo INT UNSIGNED,
    FOREIGN KEY (id_polideportivo) REFERENCES polideportivos (id)
);


CREATE TABLE reservas (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    fecha_reserva DATETIME,
    fecha_uso DATETIME,
    precio DECIMAL(4, 2),
    id_pista INT UNSIGNED,
    FOREIGN KEY (id_pista) REFERENCES pistas (id)
);


CREATE TABLE usuario_reserva (
	id_usuario INT UNSIGNED,
    id_reserva INT UNSIGNED,
    FOREIGN KEY (id_usuario) REFERENCES usuarios (id),
    FOREIGN KEY (id_reserva) REFERENCES reservas (id),
    PRIMARY KEY(id_usuario, id_reserva)
);