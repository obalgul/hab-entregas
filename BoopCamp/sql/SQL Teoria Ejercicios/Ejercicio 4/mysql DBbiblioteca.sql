USE DBbiblioteca;

CREATE TABLE IF NOT EXISTS usuarios
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (50) NOT NULL,
apellidos VARCHAR (50) NOT NULL,
email VARCHAR(75) UNIQUE,
telefono VARCHAR (13) NOT NULL,
fecha_nacimiento DATE
);
CREATE TABLE IF NOT EXISTS libros
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
titulo VARCHAR (50) NOT NULL,
editorial VARCHAR (50) NOT NULL,
numero_paginas DECIMAL (4,0),
resumen VARCHAR (1300),
fecha_inicio_prestamo DATETIME,
fecha_fin_prestamo DATETIME,
id_usuario INT UNSIGNED,
FOREIGN KEY (id_usuario) REFERENCES usuarios (id),
id_autor INT UNSIGNED,
FOREIGN KEY (id_autor) REFERENCES autores (id)
);
CREATE TABLE IF NOT EXISTS autores
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (50) NOT NULL,
apellidos VARCHAR (50) NOT NULL,
fecha_nacimiento DATE,
fecha_fallecimieno DATE,
nacionalidad VARCHAR (25),
resumen_vida VARCHAR (500)
);
CREATE TABLE IF NOT EXISTS usuarios_libros
(
id_usuario INT UNSIGNED,
FOREIGN KEY (id_usuario) REFERENCES usuarios (id),
id_libro INT UNSIGNED,
FOREIGN KEY (id_libro) REFERENCES libros (id),
PRIMARY KEY (id_usuario, id_libro)
);
CREATE TABLE IF NOT EXISTS libros_autores
(
id_libro INT UNSIGNED,
FOREIGN KEY (id_libro) REFERENCES libros (id),
id_autor INT UNSIGNED,
FOREIGN KEY (id_autor) REFERENCES autores (id),
PRIMARY KEY (id_libro, id_autor)
);