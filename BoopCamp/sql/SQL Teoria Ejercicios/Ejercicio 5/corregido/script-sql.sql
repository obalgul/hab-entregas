-- CREATE DATABASE colegio;
USE colegio;

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE alumnos (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    calle VARCHAR(50),
    localidad VARCHAR(50),
    codigo_postal VARCHAR(5),
    nota_media DECIMAL(4, 2),
    id_alumno_trabajos INT UNSIGNED,
    FOREIGN KEY (id_alumno_trabajos) REFERENCES alumnos (id)
);

CREATE TABLE asignaturas (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50),
    descripcion VARCHAR(50),
    cantidad_horas INT,
    id_profesor INT UNSIGNED,
    FOREIGN KEY (id_profesor) REFERENCES profesores (id),
    id_curso INT UNSIGNED,
    FOREIGN KEY (id_curso) REFERENCES cursos (id)
);

CREATE TABLE profesores (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    rama ENUM('letras', 'ciencias'),
    usuario VARCHAR(50),
    contrasena VARCHAR(128),
    ultimo_cambio_contrasena DATE,
    carga_lectiva INT
);

CREATE TABLE cursos (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50),
    aula VARCHAR(50),
    id_profesor_tutor INT UNSIGNED,
    FOREIGN KEY (id_profesor_tutor) REFERENCES profesores (id)
);

CREATE TABLE calificaciones (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nota DECIMAL(4, 2),
    observaciones VARCHAR(500),
    evaluacion VARCHAR(50),
    id_alumno INT UNSIGNED,
    FOREIGN KEY (id_alumno) REFERENCES alumnos (id),
    id_asignatura INT UNSIGNED,
    FOREIGN KEY (id_asignatura) REFERENCES asignaturas (id)
);

CREATE TABLE telefonos (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    id_profesor INT UNSIGNED,
    FOREIGN KEY (id_profesor) REFERENCES profesores (id),
    telefono VARCHAR(10)
);

-- Relación estudia
CREATE TABLE alumno_asignatura (
	id_alumno INT UNSIGNED,
    FOREIGN KEY (id_alumno) REFERENCES alumnos (id),
    id_asignatura INT UNSIGNED,
    FOREIGN KEY (id_asignatura) REFERENCES asignaturas (id),
    PRIMARY KEY (id_alumno, id_asignatura)
);

SET FOREIGN_KEY_CHECKS = 1;
