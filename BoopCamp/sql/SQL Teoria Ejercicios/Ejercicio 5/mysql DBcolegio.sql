USE DBcolegio;

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE IF NOT EXISTS studens
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
username VARCHAR (25) NOT NULL,
last_name VARCHAR (50) NOT NULL,
adress VARCHAR (75),
phone VARCHAR (13),
dni VARCHAR (9) UNIQUE NOT NULL,
email VARCHAR (50),
expedient_number VARCHAR (15),
id_partner_student INT UNSIGNED,
FOREIGN KEY (id_partner_student) REFERENCES studens (id)
);
CREATE TABLE IF NOT EXISTS subjets
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
week_hour_number DECIMAL (4, 2),
subjet_name VARCHAR (50),
id_course INT UNSIGNED,
id_theacher INT UNSIGNED,
FOREIGN KEY (id_course) REFERENCES courses (id),
FOREIGN KEY (id_theacher) REFERENCES theachers (id)
);
CREATE TABLE IF NOT EXISTS courses
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
classsroom VARCHAR (50),
schedude DATETIME,
course_code VARCHAR (15),
course_name VARCHAR (50),
id_theacher_tutor INT UNSIGNED,
FOREIGN KEY (id_theacher_tutor) REFERENCES theachers (id)
);
CREATE TABLE IF NOT EXISTS teachers
(
id INT UNSIGNED PRIMARY KEY,
username VARCHAR (25),
last_name VARCHAR (50),
adress VARCHAR (75),
email VARCHAR (75)
);
CREATE TABLE IF NOT EXISTS ratings
(
id INT UNSIGNED PRIMARY KEY,
note DECIMAL (4, 2),
evaluation DECIMAL (4, 2),
observations VARCHAR (300),
id_student INT UNSIGNED, 
id_subjet INT UNSIGNED,
FOREIGN KEY (id_student) REFERENCES studens (id),
FOREIGN KEY (id_subjet) REFERENCES subjets (id)
);
CREATE TABLE IF NOT EXISTS student_subjet
(
id INT UNSIGNED PRIMARY KEY,
id_student INT UNSIGNED,
id_subjet INT UNSIGNED,
FOREIGN KEY (id_student) REFERENCES students (id),
FOREIGN KEY (id_subjet) REFERENCES sujets (id)
);
SET FOREIGN_KEY_CHECKS = 1;