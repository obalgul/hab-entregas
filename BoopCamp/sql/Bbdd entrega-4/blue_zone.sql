use blue_zone;

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE IF NOT EXISTS users
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
user_name VARCHAR (25) NOT NULL,
last_name VARCHAR (50) NOT NULL,
dni VARCHAR (9) UNIQUE NOT NULL,
movil_phone VARCHAR (13) NOT NULL,
email VARCHAR (75) UNIQUE NOT NULL,
credircart_number VARCHAR (20) UNIQUE NOT NULL,
date_creation_claim DATETIME NOT NULL,
claim_text VARCHAR(250) NOT NULL,
claim_state BOOLEAN DEFAULT false,
);
CREATE TABLE IF NOT EXISTS car_users_registred
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
enrollment VARCHAR (20) NOT NULL,
brand VARCHAR (20),
model VARCHAR (20),
type_vehicle  ENUM ('car', 'van', 'motocycle'),
id_user INT UNSIGNED,
FOREIGN KEY (id_user) REFERENCES users (id)
); 
CREATE TABLE IF NOT EXISTS parking_control_zones
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
date_time_start DATETIME NOT NULL,
date_time_finish DATETIME NOT NULL,
parking_control_limit_time NOT NULL,
id_car_user_registred INT UNSIGNED,
FOREIGN KEY (id_car_user_registred) REFERENCES id_car_user_registreds (id), 
);
CREATE TABLE IF NOT EXISTS penalty
(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
date_time_penalty DATATIME NOT NULL,
penalty_fee_price DECIMAL (2, 2),
id_parking_control_zone INT UNSIGNED,
FOREIGN KEY (id_parking_control_zone) REFERENCES id_parking_control_zones (id)
id_user INT UNSIGNED,
FOREIGN KEY (id_user) REFERENCES users (id)


SET FOREIGN_KEY_CHECKS=1;
