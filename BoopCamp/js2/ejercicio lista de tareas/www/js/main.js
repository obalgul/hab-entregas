class TodoList {
  constructor(form, list, keyName) {
    this.keyName = keyName;
    this.todos = [];

    const storedTodos = JSON.parse(window.localStorage.getItem(this.keyName));

    if (storedTodos) {
      this.todos = storedTodos;
    }

    this.form = form;
    this.list = list;
    this.cleanButton = this.form.querySelector("button.clean");
    this.deleteAllButton = this.form.querySelector("button.delete");

    if (
      !this.form ||
      !this.list ||
      !this.cleanButton ||
      !this.deleteAllButton
    ) {
      throw new Error("Hai elementos que faltan, comprueba tu HTML");
    }

    this.render();
  }

  attachEvents() {
    this.form.addEventListener("submit", (e) => {
      e.preventDefault();

      const input = this.form.querySelector("input#todo");

      if (input.value.length > 0 && input.value.length < 200) {
        this.addTodo(input.value);

        input.value = "";
      }
    });

    this.cleanButton.addEventListener("click", (e) => {
      this.cleanTodoList();
    });

    this.deleteAllButton.addEventListener("click", (e) => {
      if (
        window.prompt("Escribe BORRAR para borrar todos los todos") === "BORRAR"
      ) {
        this.deleteAllTodos();
      }
    });

    this.list.addEventListener("click", (event) => {
      const target = event.target;

      if (target.matches('input[type="checkbox"]')) {
        this.toggleTodoStatus(target.getAttribute("data-index"));
      }
    });
  }

  addTodo(text) {
    if (text.length === 0) throw new Error("El texto del todo está vacío");

    const newTodo = {
      text: text,
      done: false,
    };

    this.todos.unshift(newTodo);

    this.sync();
  }

  deleteAllTodos() {
    this.todos = [];

    this.sync();
  }

  toggleTodoStatus(index) {
    const todo = this.todos[index];

    todo.done = !todo.done;

    this.sync();
  }

  cleanTodoList() {
    const cleanList = this.todos.filter((todo) => !todo.done);

    this.todos = cleanList;

    this.sync();
  }

  sync() {
    window.localStorage.setItem(this.keyName, JSON.stringify(this.todos));

    this.render();
  }

  render() {
    this.list.innerHTML = "";

    const fragment = document.createDocumentFragment();

    let i = 0;

    for (const todo of this.todos) {
      const li = document.createElement("li");
      const input = document.createElement("input");
      input.setAttribute("type", "checkbox");
      input.setAttribute("data-index", i);

      if (todo.done) {
        li.classList.add("done");
        input.setAttribute("checked", true);
      }

      const p = document.createElement("p");
      p.textContent = todo.text;

      li.append(input);
      li.append(p);

      fragment.append(li);

      i++;
    }

    this.list.append(fragment);
  }
}

const form = document.querySelector("form#new-todo");
const list = document.querySelector("ul#todos");
const myTodos = new TodoList(form, list, "stored-todos");
myTodos.attachEvents();
