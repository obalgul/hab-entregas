const clock = document.querySelector("header h1");
const greeting = document.querySelector("header h2");
const time = document.body;

// añade un 0 inicial al número si es menor que 10
function format(number) {
  if (number >= 10) return number;
  return "0" + number;
}

function writeClock() {
  const header = clock.parentElement;
  const alarm = header.dataset.alarm;

  let alarmHour;
  let alarmMinute;
  let alarmSecond;

  [alarmHour, alarmMinute, alarmSecond] = alarm.split(":");

  const now = new Date();

  const hour = format(now.getHours());
  const minute = format(now.getMinutes());
  const second = format(now.getSeconds());

  clock.textContent = `
    ${hour}:${minute}:${second}
  `;

  if (
    hour === Number(alarmHour) &&
    minute === Number(alarmMinute) &&
    second === Number(alarmSecond)
  ) {
    clearInterval(interval);
    greeting.textContent = "¡¡¡¡Alarma!!!";
    return;
  }

  greeting.textContent = getGreeting(now.getHours());
}

function getGreeting(hour) {
  if (hour <= 6 || hour >= 22)
    return (
      time.classList.add("noche"),
      (time.style.backgroundImage = "url(/img/noche.jpeg) "),
      "Buenas noches"
    );
  if (hour <= 12)
    return (
      time.classList.add("dia"),
      (time.style.backgroundImage = "url(/img/dia.jpg) "),
      "Buenas días"
    );
  return (
    time.classList.add("tarde"),
    (time.style.backgroundImage = "url(/img/tarde.jpeg) "),
    "Buenos tardes"
  );
}

writeClock();
const interval = setInterval(writeClock, 1000);
