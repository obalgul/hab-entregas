const clock = document.querySelector("header h1");
const greeting = document.querySelector("header h2");
const time = document.body;

function format(number) {
  if (number >= 10) return number;
  return "0" + number;
}
function writeClock() {
  const now = new Date();

  const hour = format(now.getHours());
  const minute = format(now.getMinutes());
  const second = format(now.getSeconds());

  clock.textContent = `
    ${hour}:${minute}:${second}
  `;

  greeting.textContent = getGreeting(now.getHours());
}

function getGreeting(hour) {
  if (hour <= 6 || hour >= 22)
    return (
      time.classList.add("noche"),
      (time.style.backgroundImage = "url(/img/noche.jpeg) "),
      "Buenas noches"
    );
  if (hour <= 12)
    return (
      time.classList.add("dia"),
      (time.style.backgroundImage = "url(/img/dia.jpg) "),
      "Buenas días"
    );
  return (
    time.classList.add("tarde"),
    (time.style.backgroundImage = "url(/img/tarde.jpeg) "),
    "Buenos tardes"
  );
}

writeClock();
const interval = setInterval(writeClock, 1000);

//------------------------------------------------------------------//

async function dataTemp() {
  const coruna = await fetch(
    `https://www.el-tiempo.net/api/json/v2/provincias/15/municipios/15030`
  );
  const dataCoruna = await coruna.json();
  const corunaCity = dataCoruna.municipio.NOMBRE_CAPITAL;
  const corunaTemp = dataCoruna.temperaturas;
  const corunaSky = dataCoruna.stateSky.description;
  const corunaTact = dataCoruna.temperatura_actual;

  const corunaData = {
    city: corunaCity,
    min: corunaTemp.min,
    max: corunaTemp.max,
    act: corunaTact,
    sky: corunaSky,
  };

  const santiago = await fetch(
    `https://www.el-tiempo.net/api/json/v2/provincias/15/municipios/15078`
  );
  const dataSantiago = await santiago.json();
  const santiagoCity = dataSantiago.municipio.NOMBRE_CAPITAL;
  const santiagoTemp = dataSantiago.temperaturas;
  const santiagoSky = dataSantiago.stateSky.description;
  const santiagoTact = dataSantiago.temperatura_actual;

  const santiagoData = {
    city: santiagoCity,
    min: santiagoTemp.min,
    max: santiagoTemp.max,
    act: santiagoTact,
    sky: santiagoSky,
  };

  const lugo = await fetch(
    `https://www.el-tiempo.net/api/json/v2/provincias/27/municipios/27028`
  );
  const dataLugo = await lugo.json();
  const lugoCity = dataLugo.municipio.NOMBRE_CAPITAL;
  const lugoTemp = dataLugo.temperaturas;
  const lugoSky = dataLugo.stateSky.description;
  const lugoTact = dataLugo.temperatura_actual;

  const lugoData = {
    city: lugoCity,
    min: lugoTemp.min,
    max: lugoTemp.max,
    act: lugoTact,
    sky: lugoSky,
  };

  const ourense = await fetch(
    `https://www.el-tiempo.net/api/json/v2/provincias/32/municipios/32054`
  );
  const dataOurense = await ourense.json();
  const ourenseCity = dataOurense.municipio.NOMBRE_CAPITAL;
  const ourenseTemp = dataOurense.temperaturas;
  const ourenseSky = dataOurense.stateSky.description;
  const ourenseTact = dataOurense.temperatura_actual;

  const ourenseData = {
    city: ourenseCity,
    min: ourenseTemp.min,
    max: ourenseTemp.max,
    act: ourenseTact,
    sky: ourenseSky,
  };

  const pontevedra = await fetch(
    `https://www.el-tiempo.net/api/json/v2/provincias/36/municipios/36038`
  );
  const dataPontevedra = await pontevedra.json();
  const pontevedraCity = dataPontevedra.municipio.NOMBRE_CAPITAL;
  const pontevedraTemp = dataPontevedra.temperaturas;
  const pontevedraSky = dataPontevedra.stateSky.description;
  const pontevedraTact = dataPontevedra.temperatura_actual;

  const pontevedraData = {
    city: pontevedraCity,
    min: pontevedraTemp.min,
    max: pontevedraTemp.max,
    act: pontevedraTact,
    sky: pontevedraSky,
  };

  const vigo = await fetch(
    `https://www.el-tiempo.net/api/json/v2/provincias/36/municipios/36057`
  );
  const dataVigo = await vigo.json();
  const vigoCity = dataVigo.municipio.NOMBRE_CAPITAL;
  const vigoTemp = dataVigo.temperaturas;
  const vigoSky = dataVigo.stateSky.description;
  const vigoTact = dataVigo.temperatura_actual;

  const vigoData = {
    city: vigoCity,
    min: vigoTemp.min,
    max: vigoTemp.max,
    act: vigoTact,
    sky: vigoSky,
  };

  const temperaturas = [
    santiagoData,
    corunaData,
    lugoData,
    ourenseData,
    pontevedraData,
    vigoData,
  ];

  const section = document.querySelector("section.temp");
  const table = document.createElement("table");
  const caption = document.createElement("caption");
  caption.textContent = "Temperaturas";

  table.append(caption);

  const thead = document.createElement("thead");

  thead.innerHTML = `
  <th>Ciudad</th>
  <th>Mínima</th>
  <th>Máxima</th>
  <th>Ahora</th>
  <th>Estado del cielo</th>
`;

  table.append(thead);

  for (const temp of temperaturas) {
    const tr = document.createElement("tr");

    const cityCol = document.createElement("td");
    cityCol.textContent = temp.city;

    const minCol = document.createElement("td");
    minCol.textContent = temp.min;
    minCol.classList.add(getClassName(temp.min));

    const maxCol = document.createElement("td");
    maxCol.textContent = temp.max;
    maxCol.classList.add(getClassName(temp.max));

    const actCol = document.createElement("td");
    actCol.textContent = temp.act;
    actCol.classList.add(getClassName(temp.act));

    const skyCol = document.createElement("td");
    skyCol.textContent = temp.sky;

    tr.append(cityCol);
    tr.append(minCol);
    tr.append(maxCol);
    tr.append(actCol);
    tr.append(skyCol);

    table.append(tr);
  }

  section.append(table);

  function getClassName(temp) {
    if (temp < 16) return "lower";
    if (temp < 26) return "low";
    if (temp < 27) return "medium";
    return "high";
  }
}

console.log(dataTemp());
