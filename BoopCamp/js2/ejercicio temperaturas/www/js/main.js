// Temp. menor que 20: fondo verde
// Temp. entre 20 y 30: fondo naranja
// Temp. mayor de 30: fondo rojo

const temperaturas = [
  {
    city: "A coruña",
    min: 17,
    max: 23,
  },
  {
    city: "Ferrol",
    min: 15,
    max: 32,
  },
  {
    city: "Lugo",
    min: 4,
    max: 27,
  },
  {
    city: "Ourense",
    min: 9,
    max: 35,
  },
  {
    city: "Pontevedra",
    min: 18,
    max: 27,
  },
];

const section = document.querySelector("section.temp");
const table = document.createElement("table");
const caption = document.createElement("caption");
caption.textContent = "Temperaturas";

table.append(caption);

const thead = document.createElement("thead");

thead.innerHTML = `
  <th>Ciudad</th>
  <th>Mínima</th>
  <th>Máxima</th>
`;

table.append(thead);

for (const temp of temperaturas) {
  const tr = document.createElement("tr");

  const cityCol = document.createElement("td");
  cityCol.textContent = temp.city;

  const minCol = document.createElement("td");
  minCol.textContent = temp.min;
  minCol.classList.add(getClassName(temp.min));

  const maxCol = document.createElement("td");
  maxCol.textContent = temp.max;
  maxCol.classList.add(getClassName(temp.max));

  tr.append(cityCol);
  tr.append(minCol);
  tr.append(maxCol);

  table.append(tr);
}

section.append(table);

function getClassName(temp) {
  if (temp < 10) return "lower";
  if (temp < 25) return "low";
  if (temp < 30) return "medium";
  return "high";
}
