const form = document.querySelector("form");
const input = form.querySelector('input[name="tweet"]');
const list = document.querySelector("ul#tweets");
console.log("caca");

let tweets = [];
//guardamos la info en memoria navegados de forma local.
const loalTweets = JSON.parse(window.localStorage.getItem("tweets"));

if (loalTweets) {
  tweets = loalTweets;
}

function updateTweetList() {
  list.innerHTML = "";
  // guardamos en un fragmento para insertarlo al DOM
  const fragment = document.createDocumentFragment();

  let index = 0;
  //creamos los li que se guardaran en el array
  for (const tweet of tweets) {
    //creamos los contenedores del li
    const li = document.createElement("li");

    const p = document.createElement("p");
    p.textContent = tweet.text;

    const footer = document.createElement("footer");

    const time = document.createElement("time");
    time.textContent = tweet.date;
    // creamos el botton con atributos para tener un index del botón
    const button = document.createElement("button");
    button.setAttribute("data-index", index);
    button.classList.add("action");
    button.textContent = "Borrar";
    //añadimos los contenedores del li a la lista
    footer.append(time);
    footer.append(button);

    li.append(p);
    li.append(footer);

    fragment.append(li);
    // mantenemos la cuenta de los li
    index++;
  }
  // metemos el fragmento dentro de la lista.
  list.append(fragment);
}

//creamos un evento pra guardar el tweet
form.addEventListener("submit", (event) => {
  event.preventDefault();

  const tweetText = input.value;
  //creamos un limite de escritura para lso tweets
  if (tweetText.length === 0 || tweetText.length === 250) {
    alert("Debes escribir entre al menos un caracter hasta 250.");
  } else {
    // guardamos la fecha
    const now = new Date();
    //añadimos al tweet el text y la fecha en un objeto
    tweets.unshift({
      text: tweetText,
      // al mes le sumamos 1 porque js emppieza a contar en 0
      date: `${now.getDate()}/${now.getMonth() + 1}/${now.getFullYear()}`,
    });
    //actualizamos en el bbdd del navegador
    window.localStorage.setItem("tweets", JSON.stringify(tweets));
    //reiniciamos el input de entrada de mensaje para que aparezca vacia
    input.value = "";
    updateTweetList();
  }
});

//creamos un evento que borra el tweet
list.addEventListener("click", (event) => {
  const target = event.target;

  if (target.matches("button.action")) {
    const index = target.getAttribute("data-index");
    tweets.splice(index, 1);
    // se actualiza en la bbdd dle navegador en local
    window.localStorage.setItem("tweets", JSON.stringify(tweets));

    updateTweetList();
  }
});

updateTweetList();
