const express = require("express");

const app = express();

app.get("/", (request, response) => {
  const now = new Date();
  if (now.getMinutes() % 2 === 0) {
    response.send(`Son las ${now.getHours()}:${now.getMinutes()}`);
  } else {
    response.status(500).send("Estoy durmiendo vuelve en un minuto 😴");
  }
});

app.listen(3000, () => {
  console.log("🥱");
});
