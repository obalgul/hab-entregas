const express = require("express");

const app = express();

app.use((request, response, next) => {
  const now = new Date();
  if (now.getMinutes() % 2 === 0) {
    next();
  } else {
    next(new Error("Estoy durmiendo vuelve en un minuto 😴"));
  }
});

app.get("/", (request, response) => {
  const now = new Date();
  response.send(`Son las ${now.getHours()}:${now.getMinutes()}`);
});

// Error middleware
app.use((error, request, response, next) => {
  response.status(500).send(error.message);
});

// Not found
app.use((request, response) => response.status(404).send("Not found"));

app.listen(3000, () => {
  console.log("🥱");
});
