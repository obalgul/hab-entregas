const fs = require("fs").promises;
const path = require("path");
const os = require("os");
const chalk = require("chalk");

const { formatDistanceToNow } = require("date-fns");
const { es } = require("date-fns/locale");

const todoFile = path.join(os.homedir(), ".tasks.json");

// Lee la lista de todos y devuelve el contenido
async function readTodoList() {
  try {
    let todos;

    try {
      const todoListContent = await fs.readFile(todoFile);
      todos = JSON.parse(todoListContent.toString());
    } catch (error) {
      todos = { todos: [] };
      await fs.writeFile(todoFile, JSON.stringify(todos));
    }

    return todos;
  } catch (error) {
    console.error(error);
  }
}

// Guarda la lista de todos
async function saveTodosList(todoList) {
  try {
    await fs.writeFile(todoFile, JSON.stringify(todoList, null, 5));
  } catch (error) {
    console.error(error.message);
  }
}
//Añadir todo a  la lista
async function addTodo({ text, priority }) {
  try {
    const currentList = await readTodoList();
    const newTodo = {
      text,
      priority,
      done: false,
      createDate: new Date(),
    };

    currentList.todos.unshift(newTodo);
    console.log(
      chalk.green(
        `Se ha creado una nueva tarea "${text} a la lista. Ejecuta "node.js --list" para comprobar el listado de tareas guardadas.`
      )
    );

    await saveTodosList(currentList);
  } catch (error) {
    console.error("Ooops!! Algo ha sucedido. No se pudo guardar la tarea");
  }
}

// Marcar como echo o no una tarea
async function setTodoState(index, state) {
  const currentList = await readTodoList();
  const todo = currentList.todos[index - 1];
  if (todo) {
    todo.done = state;
    console.log(
      chalk.blue(
        `La tarea "${todo.text}" se ha marcado como "${
          state ? "realizada" : "pendiente"
        }`
      )
    );
    await saveTodosList(currentList);
  } else {
    console.log("Va a ser que ese número de tarea no existe.");
  }
}

async function listTodos() {
  const { todos } = await readTodoList();

  console.log();
  console.log(chalk.magenta("---------------------------"));
  console.log(chalk.magenta("Lista de tareas pendientes:"));
  console.log(chalk.magenta("---------------------------"));
  console.log();

  for (const [index, todo] of todos.entries()) {
    const todoStatus = todo.done ? "✔" : "x";
    const todoPriority = todo.priority ? "Urgente" : "Normal";
    const todoDistance = formatDistanceToNow(new Date(todo.createDate), {
      locale: es,
    });

    const output = `${index + 1} | ${todoStatus} | ${
      todo.text
    } | ${todoPriority} | Añadida hace ${todoDistance}`;

    if (todo.done) {
      console.log(chalk.strikethrough.cyan(output));
    } else {
      if (todo.priority) {
        console.log(chalk.red(output));
      } else {
        console.log(chalk.greenBright(output));
      }
    }
  }

  console.log();
  console.log(chalk.magenta("---------------------------"));
  console.log();
  console.log(
    chalk.yellow(
      `Use "node todo.js --done=<número>" para marcar una tarea como realizada.`
    )
  );
  console.log();
}

//Eliminamos las tareas hechas.
async function cleanTodos() {
  const currentList = await readTodoList();

  currentList.todos = currentList.todos.filter((todo) => !todo.done);

  console.log(
    chalk.blueBright(
      `Se eliminaton de la lista las tareas marcadas como hechas. Puedes comprobarlo escribiendo "node todo.js --list"`
    )
  );
  await saveTodosList(currentList);
}

module.exports = {
  addTodo,
  setTodoState,
  listTodos,
  cleanTodos,
};
