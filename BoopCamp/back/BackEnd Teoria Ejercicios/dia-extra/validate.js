const Joi = require("@hapi/joi");

// Definimos valor
const value = {
  email: "berto@ber.to", //requerido
  // userName: "lo",
  // age: 43,
  password: "123449", //requerido
};

// Crear un esquema
const schema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required()
    .error(new Error("El email debe de ser válido")),
  userName: Joi.string().min(3).max(100),
  age: Joi.number().min(18),
  password: Joi.string()
    .min(8)
    .required()
    .error(new Error("La password debe de tener un mínimo de 8 caracteres")),
});

// Pasarle datos al esquema
const validationResult = schema.validate(value, { convert: false });

// Ver los resultados de la validación
if (validationResult.error) {
  console.log("No es válido:");
  console.log(validationResult.error.message);
} else {
  console.log("Ok, seguimos");
}
