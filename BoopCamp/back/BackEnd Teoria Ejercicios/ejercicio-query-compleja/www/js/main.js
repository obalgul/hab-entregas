const form = document.querySelector("form");
const output = document.querySelector("#output");
const parametres = document.querySelector("#parametres");

form.addEventListener("submit", (event) => {
  event.preventDefault();
  const nombre = form.elements.nombre.value;
  const provincia = form.elements.provincia.value;
  const envio = form.elements.envio.checked;
  const tipo = form.elements.tipo.value;

  generateQuery({ body: { nombre, provincia, envio, tipo } }, "OR");
});

function generateQuery(req, union = "AND") {
  // Imaginaos que esto es un controlador de express
  const { nombre, provincia, envio, tipo } = req.body;

  let query = `SELECT * from clients`;
  const params = [];

  if (nombre || provincia || envio || tipo) {
    const conditions = [];

    if (nombre) {
      conditions.push("nombre LIKE ?");
      params.push(`'%${nombre}%'`);
    }

    if (provincia) {
      conditions.push("provincia=?");
      params.push(`'${provincia}'`);
    }

    if (envio) {
      conditions.push("envio=?");
      params.push(`true`);
    }

    if (tipo) {
      conditions.push("tipo=?");
      params.push(`'${tipo}'`);
    }

    query = `${query} WHERE ${conditions.join(` ${union} `)}`;
  }

  output.innerText = query;
  parametres.innerText = `[${params.join(",")}]`;
}
