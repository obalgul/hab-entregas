const cowsay = require("cowsay");
const args = process.argv.slice(2);

//cowsay.say es la llamada al saludo. Viene de fuera.
const message = cowsay.say({
  text: args.join(" "),
});

console.log(message);
