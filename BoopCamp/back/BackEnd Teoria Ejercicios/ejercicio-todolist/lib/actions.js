const fs = require("fs").promises;
const path = require("path");
const os = require("os");
const chalk = require("chalk");

const { formatDistanceToNow } = require("date-fns");
const { es } = require("date-fns/locale");

const todoFile = path.join(os.homedir(), ".tasks.json");

// Estructura de la lista de todos en disco
/*
{
  "todos": [
    {
      "text": "Ir al dentista",
      "done": false,
      "priority": true,
      "createdAt": <date-iso>
    },
    {
      "text": "Ir al dentista",
      "done": false,
      "priority": true,
      "createdAt": <date-iso>
    },
  ]
}

*/

// Lee la lista de todos y devuelve el contenido
async function readTodoList() {
  try {
    let todos;

    try {
      const todoListContent = await fs.readFile(todoFile);
      todos = JSON.parse(todoListContent.toString());
    } catch (error) {
      todos = { todos: [] };
    }

    return todos;
  } catch (error) {
    console.error(error);
  }
}

// Guarda la lista de todos
async function saveTodoList(todoList) {
  try {
    await fs.writeFile(todoFile, JSON.stringify(todoList, null, 4));
  } catch (error) {
    console.error(error.message);
  }
}

// Esta función añade un TODO a la lista
async function addTodo({ text, priority }) {
  // Abrir la lista actual de todos
  const currentList = await readTodoList();

  // Creamos un objeto que representa el nuevo todo
  const now = new Date();
  const newTodo = {
    text,
    priority,
    done: false,
    createAt: now.toISOString(),
  };

  // Añadir el todo que recibe al principio de la lista
  currentList.todos.unshift(newTodo);

  console.log(
    chalk.green(
      `Se añadió el todo "${text}" a la lista. Usa node todo.js --list para ver los todos guardados.`
    )
  );

  // Guardar la lista actualizada
  await saveTodoList(currentList);
}

// Esta función marca un TODO como hecho
async function setTodoState(index, state) {
  // Abrir la lista actual de todos
  const currentList = await readTodoList();

  // Buscar el todo con el index que recibe
  const todo = currentList.todos[index - 1];

  if (todo) {
    // Modificar el objecto del todo como done: true
    todo.done = state;

    console.log(
      chalk.blue(
        `El todo "${todo.text}" se marcó como ${state ? "hecho" : "pendiente"}`
      )
    );

    // Guardar la lista actualizada
    await saveTodoList(currentList);
  } else {
    console.error("El todo no existe");
  }
}

// Esta función imprime en el terminal la lista de TODOs
async function listTodos() {
  const { todos } = await readTodoList();

  console.log();
  console.log(chalk.blue("Lista de todos pendientes:"));
  console.log(chalk.blue("--------------------------"));

  for (const [index, todo] of todos.entries()) {
    const todoStatus = todo.done ? "✔" : "x";
    const todoPriority = todo.priority ? "Alta prioridad" : "Normal";
    const todoDistance = formatDistanceToNow(new Date(todo.createAt), {
      locale: es,
    });

    const output = `${index + 1} | ${todoStatus} | ${
      todo.text
    } | ${todoPriority} | hace ${todoDistance}`;

    if (todo.done) {
      console.log(chalk.strikethrough.cyan(output));
    } else {
      if (todo.priority) {
        console.log(chalk.red(output));
      } else {
        console.log(chalk.green(output));
      }
    }
  }

  console.log();
  console.log(
    chalk.blue(
      "Usa node todo.js --done=<número> para marcar un todo como hecho"
    )
  );
  console.log();

  // Abrir la lista actual de todos
  // Imprimir la lista en la consola
}

// Esta función elimina de la lista de TODOs los marcados como hecho
async function cleanTodos() {
  // Abrir la lista actual de todos
  const currentList = await readTodoList();
  // Filtrar los que están marcados como done: true

  currentList.todos = currentList.todos.filter((todo) => !todo.done);

  console.log(chalk.blue("Los todos hechos fueron eliminados de la lista."));

  // Guardar la lista resultante
  await saveTodoList(currentList);
}

module.exports = {
  addTodo,
  setTodoState,
  listTodos,
  cleanTodos,
};
